# How to add middleware to an app
```go
err := srv.Srv{
    Middleware: []srv.Middleware{{
        // All the methods are optional
        OnReq: func(srv.IMiddlewareReqArg) error {...},
        OnResp: func(srv.IMiddlewareRespArg) error {...},
        OnErr: func(srv.IMiddlewareErrArg) error {...},
    }},
}.Run()
```
# Context
# Middleware application order
In this schema
```go
srv.Srv{..., Middleware: []srv.Middleware{A, B, C}}
```
- OnReq order is: A -> B -> C
- OnResp order is: C -> B -> A
- OnErr order is: C -> B -> A
# Example
Middleware for logging request time:
```go
import (
    "log"
    "time"
)

func log_time(start time.Time) {
    log.Printf("request time: %v", time.Since(start))
}

srv.Srv{
    ...,
    Middleware: []srv.Middleware{{
        OnReq: func(arg srv.IMiddlewareReqArg) error {
            arg.Context()["start"] = time.Now()
            return nil
        },
        OnResp: func(srv.IMiddlewareRespArg) error {
            log_time(arg.Context()["start"])
            return nil
        },
        OnErr: func(srv.IMiddlewareErrArg) error {
            log_time(arg.Context()["start"])
            return nil
        },
    }}
}
```