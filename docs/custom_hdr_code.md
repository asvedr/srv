# How to create 

Use `srv.Response[T]` to modify headers and response code.
Default response code is `200`. Default headers are inherited from request and `srv.Srv.DefaultHeaders`.

Response methods:
```go
// set response status code
SetStatus(status int) {...}
// replace all headers with arguments
SetHeaders(headers map[string]string) {...}
// add or replace header
AddHeader(key string, val string) {...}
// remove header(you can remove request or default header)
DelHeader(key string) {...}
```

Example
```go
type handler struct {...}

type Request struct {...}

func (handler) Handle(Request) (srv.Response[string], error) {
	var resp srv.Response[string]{Data: "hi"}
	resp.SetStatus(203)
	resp.AddHeader("X-Auth", "token:"+req.Login)
	return resp, nil
}
```
