# How to accept form

```go
type Request struct {
    File_Field srv.FormFile `srv:"form"`
    Opt_File_Field *srv.FormFile `srv:"form"`
    Value_Field int `srv:"form"`
    Opt_Value_Field *int `srv:"form"`
}
```
Type `FormFile` declared as
```go
type FormFile struct {
	File   multipart.File
	Header *multipart.FileHeader
}
```
File data can be read as `io.ReadAll(FormFile.File)`. File name can be got as `FormFile.Header.Filename`.

## Constraints

One request couldn't not contain `srv:"form"` with `srv:"query"` or `srv:"body"` together.
