# How to reuse request parser
```go
type ParentRequest struct {
    A `srv:"query"`
    B `srv:"query"`
}

type ChildRequest struct {
    ParentRequest `srv:"include"`
    C `srv:"query"`
}

func (handler) Handle(ChildRequest) (Response, error) {
    ...
}

...

handle("/path?a=1&b=2&c=3") ==> ChildRequest{{A: 1, B: 2}, C: 3}
```

Use label `srv:"include"` to reuse parsers.
