# How to declare header parser

When you declare handler method `Handle`
```go
type HeaderType struct {}

func (*HeaderType) ParseHeader(src string) (any, error) {
    return HeaderType{}, nil
}

func (*HeaderType) GenHeaderSample() string {
    return "JWT <token>"
}

type Request struct {
    Param HeaderType `srv:"header" name:"X-My-Header"`
}

func (handler) Handle(Request) (Response, error)
```
Request fields that marked with `srv:"header"` are used for header parsing. Header name **must** be declared as `name:"X-My-Header"` label. Header type must implement `srv.IHeaderParam` interface.
