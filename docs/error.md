# How errors are processed

If error implements interface IToApiError:
```go
type IToApiError interface {
	error
	ApiError() (status int, code string)
}
```
then error will be composed as
```
STATUS_CODE = err.ApiError().status
BODY = {"code": err.ApiError().code, "detail": err.Error()}
```
otherwise error will be composed as
```
STATUS_CODE = 500
BODY = {"code": "INTERNAL": "detail": err.Error()}
```

# Generic Api error:
```go
var err srv.IToApiError = srv.NewApiErr(
	400,
	"BAD_FILE", 
	"description description description...",
)
```

# Help
If you declared method `Error()` [for your handler](/docs/handler.md) then [help handler](/docs/help.md) will full info about errors to description