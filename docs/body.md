# How to declare body parser

When you declare handler method `Handle`
```go
type Request struct {
    Field string `srv:"body"`
}

func (handler) Handle(Request) (Response, error)
```
A request field that marked with `srv:"body"` will be used to store body data. Body can be:
- `[]byte`: stored as is
- `string`: bytes will be converted to string
- `struct{...}`: will be parsed as json

Body field is optional but you can not declare more then one body field.