# Form params

You can declare optional method in the handler
```go
func (handler) FormParams() any {
    return ...
}
```
If you do this, **srv** will automaticly create a handler with same path and method `PARAMS` which will return the value you declared in method `FormParams`

## Warning
`FormParams` must return a constant value