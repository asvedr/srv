# How to use routers

```go
r := srv.Router(
    "/api/", handler1, handler2, router2,
)
srv.Srv{handlers: []srv.IRoutable{r}}
```

After that trick `handler1` and `handler2` will got path prefix `/api/`