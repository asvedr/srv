# How to declare handler

Declare a type that implement interface `srv.IApiHandler`

```go
type IApiHandler[Request, Response any] interface {
	FormParams() any
	Errors() []error
	Method() string
	Path() string
	Description() string
	Handle(Request) (Response, error)
}
```

You can use types
`BaseGetHandler`,`BasePostHandler`,`BasePutHandler`,`BaseHandler`,`BasePatchHandler`,`BaseDeleteHandler` as parents.

__Path() and Handle() are only required methods__

- `FormParams()`: used to created `PARAMS` method if declared
- `Errors()`: used to add _Errors_ section in [autogenerated help](/docs/help.md)
- `Method()`: GET, POST, PUT, etc
- `Path()`: path/to/your/handler
- `Description()` description for [autogenerated help](/docs/help.md)
- `Handle()` handler function
