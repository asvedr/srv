# How to declare query parser

When you declare handler method `Handle`
```go
type Request struct {
    Param int `srv:"query" name:"p"`
}

func (handler) Handle(Request) (Response, error)
```
Request fields that marked with `srv:"query"` are used as query params. If the field is a pointer then the query param will be optional.
Optional marks for query param
- `name:"..."` - query param name
- `default:"..."` - default value. It makes param optional

## Slice value
If a field is declared as slice then query parser will store all specific keys in the field. Example:
```go
type Request struct {
    Param []int `srv:"query" name:"p"`
}

...

parse("/path?p=1&p=2&p=2") ==> Request{Param: []int{1,2,3}}
parse("/path?p[]=1&p[]=2") ==> Request{Param: []int{1,2}}
```

## Custom parser
You can declare custom parsing rules for a type if you make an implementation for interface `srv.IQueryParam`. Example
```go
type Custom struct {
    Val int
}

type Request struct {
    Param Custom `srv:"query" name:"p"`
}

func(*Custom) ParseQuery(src string) (any, error) {
    switch src {
    case "a": return Custom{Val: 1}, nil
    case "b": return Custom{Val: 1}, nil
    default: return nil, errors.New("custom")
    }
}

func(*Custom) GenQuerySample() string {
    return "a"
}

parse("/path?p=a") ==> Request{Param: Custom{Val: 1}}
```

# Path variable
You can declare query params as path items. `:var` - declare one var. `::var` - declare var that will collect all items after `/`.

Example
```go
type handler struct {
    Val int
}

type Request struct {
    Param Custom `srv:"query" name:"p"`
}

func(handler) Path() (string, error) {
    return "/path/:var/path/::rest/"
}

func(handler) Handler(Request) (string, error) {
    ...
}

// "/path/123/path/a/b/c" ==> {"var": 123, "rest": "a/b/c"}
```
