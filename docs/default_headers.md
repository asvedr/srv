# How to set default headers

```go
err := srv.Srv{
    DefaultHeaders: map[string]string{
        "X-Server": "my value",
    },
    Addr: ...,
    HelpPath: ...,
    Handlers: ...,
}.Run()
```
