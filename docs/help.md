# Help handler
When you configure srv `srv.Srv{}` you can set param `HelpPath`. If HelpPath != "" then GET handler will be created on this path. Help handler returns list of handlers and documentation for each handler.
