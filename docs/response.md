# How to declare response serializer

When you declare handler method `Handle`
```go
func (handler[Q,B,R]) Handle(Request[Q, B]) (R, error)
```
R is response type. R can be `struct`, `[]byte` or `string`. If R is `struct` - the value will be processed with `json.Marshal`. If R is `string` - it will be converted to bytes. If R is `[]byte` - it will be used as is.
