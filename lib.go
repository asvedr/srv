package srv

import (
	"errors"
	"fmt"

	"gitlab.com/asvedr/srv/internal/dispatcher"
	"gitlab.com/asvedr/srv/internal/err_to_api"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/form_params"
	"gitlab.com/asvedr/srv/internal/generic_handler"
	"gitlab.com/asvedr/srv/internal/handler_contaier"
	"gitlab.com/asvedr/srv/internal/help_handler"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/reqparser"
	"gitlab.com/asvedr/srv/internal/router"
	"gitlab.com/asvedr/srv/internal/rserializer"
	"gitlab.com/asvedr/srv/internal/types"
	"gitlab.com/asvedr/srv/internal/wrapper"
)

type None = types.None

type Srv struct {
	Addr           string
	HelpPath       string
	Handlers       []IRoutable
	DefaultHeaders map[string]string
	Middleware     []IMiddleware
	TlsCertPath    string
	TlsKeyPath     string
	Engine         string
	// ServeFunc      func(string, http.Handler) error
}

func MakeHandler[
	Req, Resp any,
	Handler IApiHandler[Req, Resp],
](handler Handler) IRoutable {
	maker := func() (IRoutable, error) {
		parser, err := reqparser.New[Req]()
		if err != nil {
			return nil, err
		}
		var response_ser proto.ResSerializer
		if is_struct_resp[Resp]() {
			response_ser = rserializer.MakeStruct[Resp]()
		} else {
			response_ser = rserializer.MakeStd[Resp]()
		}
		return wrapper.New(parser, response_ser, handler)
	}
	wrapped, err := maker()
	if err != nil {
		msg := fmt.Sprintf(
			"Can not make handler from %T: %v",
			handler,
			err,
		)
		panic(msg)
	}
	return wrapped
}

func Router(path string, children ...IRoutable) IRoutable {
	return router.New(path, children)
}

func (self Srv) Run() error {
	handlers := make([]IRoutable, len(self.Handlers))
	copy(handlers, self.Handlers)
	if self.HelpPath != "" {
		hh, err := help_handler.New(
			self.HelpPath, self.Handlers,
		)
		if err != nil {
			return err
		}
		handlers = append(handlers, MakeHandler(hh))
	}
	fp_maker := func(
		path types.PathPttrn, param any,
	) (IRoutable, error) {
		h, err := form_params.New(path, param)
		var res IRoutable
		if err == nil {
			res = MakeHandler(h)
		}
		return res, err
	}
	container, err := handler_contaier.New(
		fp_maker, router.New, handlers,
	)
	if err != nil {
		return err
	}
	disp := dispatcher.New(
		container,
		err_to_api.Proc,
		self.DefaultHeaders,
		self.Middleware,
	)
	engine := get_engine(self.Engine)
	if engine == nil {
		return errors.New("Unknown engine: " + self.Engine)
	}
	if (self.TlsCertPath == "") != (self.TlsKeyPath == "") {
		return errs.TlsNotFull{}
	}
	return engine.Spawn(
		self.Addr,
		disp,
		self.TlsCertPath,
		self.TlsKeyPath,
	)
}

func GenericHandler[Req, Resp any](
	method string,
	path string,
	f func(Req) (Resp, error),
) IRoutable {
	return MakeHandler(generic_handler.New(method, path, f))
}

func is_struct_resp[T any]() bool {
	var obj T
	var as_any any = obj
	_, casted := as_any.(proto.IIntoResponse)
	return casted
}
