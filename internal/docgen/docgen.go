package docgen

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/asvedr/srv/internal/docgen/errlist"
	"gitlab.com/asvedr/srv/internal/docgen/filler"
	"gitlab.com/asvedr/srv/internal/docgen/jsgen"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/html"
	"gitlab.com/asvedr/srv/internal/proto"
)

func Gen(parser proto.IReqParser) string {
	var result []string
	headers, err := gen_headers(parser)
	if err != nil {
		return *err
	}
	if len(headers) > 0 {
		result = append(result, html.Div("qbrd_label", "Headers:"))
		result = append(result, headers...)
	}
	query, err := gen_query(parser)
	if err != nil {
		return *err
	}
	if len(query) > 0 {
		result = append(result, html.Div("qbrd_label", "Query:"))
		result = append(result, query...)
	}
	form, err := gen_form(parser)
	if err != nil {
		return *err
	}
	if len(form) > 0 {
		result = append(result, html.Div("qbrd_label", "Form:"))
		result = append(result, form...)
	}
	body, err := gen_body(parser)
	if err != nil {
		return *err
	}
	if body != "" {
		result = append(
			result,
			html.Div("qbrd_label", "Body:"),
			body,
		)
	}
	return strings.Join(result, "\n")
}

func GenErrList(errs []error) string {
	if len(errs) != 0 {
		return errlist.Gen(errs)
	}
	return ""
}

func gen_headers(parser proto.IReqParser) ([]string, *string) {
	result := []string{}
	for _, hparam := range parser.HParams() {
		key := hparam.Key
		val, err := gen_h_param(hparam.Type)
		if err != nil {
			return nil, h_wrap_err(hparam.Type, err)
		}
		children := []string{
			html.Div("header_param_key", key+":"),
			html.Div("value", val),
		}
		tag := html.Div("header_param", children...)
		result = append(result, tag)
	}
	return result, nil
}

func gen_query(parser proto.IReqParser) ([]string, *string) {
	result := []string{}
	for _, qparam := range parser.QParams() {
		key := qparam.Key
		val, err := gen_q_param(qparam.Type)
		if err != nil {
			return nil, q_wrap_err(qparam.Type, err)
		}
		children := []string{
			html.Div("query_param_key", key+":"),
			html.Div("value", val),
		}
		if len(qparam.Default) > 0 {
			val := qparam.Default[0]
			dtag := html.Div("query_param_default", "default="+val)
			children = append(children, dtag)
		} else if qparam.Optional {
			otag := html.Div("query_param_optional", "optional")
			children = append(children, otag)
		}
		tag := html.Div("query_param", children...)
		result = append(result, tag)
	}
	return result, nil
}

func gen_form(parser proto.IReqParser) ([]string, *string) {
	result := []string{}
	for _, fparam := range parser.FParams() {
		key := fparam.Key
		val, err := gen_f_param(fparam.Type, fparam.IsFile)
		if err != nil {
			return nil, f_wrap_err(fparam.Type, err)
		}
		children := []string{
			html.Div("form_param_key", key+":"),
			html.Div("value", val),
		}
		if fparam.Default != nil {
			val := *fparam.Default
			dtag := html.Div("form_param_default", "default="+val)
			children = append(children, dtag)
		} else if fparam.Optional {
			otag := html.Div("form_param_optional", "optional")
			children = append(children, otag)
		}
		tag := html.Div("form_param", children...)
		result = append(result, tag)
	}
	return result, nil
}

func gen_h_param(tp reflect.Type) (string, error) {
	ptr := reflect.New(tp).Interface()
	intrf, casted := ptr.(proto.IHeaderParam)
	if !casted {
		return "", errors.New("not implemented")
	}
	return intrf.GenHeaderSample(), nil
}

func gen_q_param(tp reflect.Type) (string, error) {
	ptr := reflect.New(tp).Interface()
	intrf, casted := ptr.(proto.IQueryParam)
	if casted {
		return intrf.GenQuerySample(), nil
	}
	val, err := filler.NonEmptyDyn(tp)
	return fmt.Sprint(val), err
}

func gen_f_param(tp reflect.Type, is_file bool) (string, error) {
	if is_file {
		return html.Div("value file", "FILE"), nil
	}
	ptr := reflect.New(tp).Interface()
	intrf, casted := ptr.(proto.IFormParam)
	if casted {
		return html.Div("value", intrf.GenFormSample()), nil
	}
	var dyn_val reflect.Value
	if tp.Kind() == reflect.Pointer {
		dyn_val = reflect.New(tp.Elem()).Elem()
	} else {
		dyn_val = reflect.New(tp).Elem()
	}
	val, err := jsgen.DumpDynFlat(dyn_val.Interface())
	if err != nil {
		return "", err
	}
	return html.Div("value json", val), nil
}

func gen_body(parser proto.IReqParser) (string, *string) {
	opt_tp := parser.BodyType()
	if opt_tp == nil {
		return "", nil
	}
	tp := *opt_tp
	val, err := jsgen.GenDocType(tp)
	if err != nil {
		msg := "can not gen body: " + err.Error()
		return "", &msg
	}
	return val, nil
}

func h_wrap_err(tp reflect.Type, err error) *string {
	txt := errs.ErrCanNotUseTypeInHeader{
		Type: tp, Cause: err,
	}.Error()
	return &txt
}

func q_wrap_err(tp reflect.Type, err error) *string {
	txt := errs.ErrCanNotUseTypeInQuery{
		Type: tp, Cause: err,
	}.Error()
	return &txt
}

func f_wrap_err(tp reflect.Type, err error) *string {
	txt := errs.ErrCanNotUseTypeInForm{
		Type: tp, Cause: err,
	}.Error()
	return &txt
}
