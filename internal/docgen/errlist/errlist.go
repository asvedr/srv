package errlist

import (
	"fmt"

	"gitlab.com/asvedr/srv/internal/consts"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/html"
)

func Gen(errs []error) string {
	res := html.Div("qbrd_label", "Errors:")
	for _, err := range errs {
		status, code, description := get_err_data(err)
		res += "\n"
		res += html.Div(
			"error",
			html.Div("error_status", html.Div("value", status)),
			html.Div("error_code", html.Div("value", code)),
			html.Div("error_description", html.Div("value", description)),
		)
	}
	return res
}

func get_err_data(err error) (string, string, string) {
	to_api_err, casted := err.(errs.IToApiError)
	status := consts.InternalErrorStatus
	code := consts.InternalErrorCode
	if casted {
		status, code = to_api_err.ApiError()
	}
	return fmt.Sprint(status), code, err.Error()
}
