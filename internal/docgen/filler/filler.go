package filler

import (
	"fmt"
	"reflect"
)

func NonEmpty[T any]() (T, error) {
	var t T
	val, err := non_empty_value(reflect.TypeOf(t))
	if err != nil {
		return t, err
	}
	return val.Interface().(T), nil
}

func NonEmptyDyn(tp reflect.Type) (any, error) {
	val, err := non_empty_value(tp)
	if err != nil {
		return nil, err
	}
	if tp.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	return val.Interface(), nil
}

var primitive = map[reflect.Kind]any{
	reflect.Bool:       true,
	reflect.Int:        int(-1),
	reflect.Int8:       int8(-1),
	reflect.Int16:      int16(-1),
	reflect.Int32:      int32(-1),
	reflect.Int64:      int64(-1),
	reflect.Uint:       uint(1),
	reflect.Uint8:      uint8(1),
	reflect.Uint16:     uint16(1),
	reflect.Uint32:     uint32(1),
	reflect.Uint64:     uint64(1),
	reflect.Float32:    float32(1.5),
	reflect.Float64:    float64(1.5),
	reflect.Complex64:  complex64(1 + 2i),
	reflect.Complex128: complex128(1 + 2i),
}

var bts_type = reflect.TypeOf([]byte{}).String()
var any_type = reflect.TypeOf([]any{}).Elem().String()

func non_empty_value(tp reflect.Type) (reflect.Value, error) {
	if tp.String() == bts_type {
		return reflect.ValueOf([]byte{1, 2, 3}), nil
	}
	if tp.String() == any_type {
		return reflect.ValueOf("any"), nil
	}
	prim, is_prim := primitive[tp.Kind()]
	if is_prim {
		return reflect.ValueOf(prim), nil
	}
	switch tp.Kind() {
	case reflect.Map:
		return non_empty_map(tp)
	case reflect.Slice:
		return non_empty_slice(tp)
	case reflect.String:
		return reflect.ValueOf("string"), nil
	case reflect.Pointer:
		return non_empty_pointer(tp)
	case reflect.Struct:
		return non_empty_struct(tp)
	}
	err := fmt.Errorf("non_empty_value: unsupported type: %v", tp)
	return reflect.Value{}, err
}

func non_empty_pointer(tp reflect.Type) (reflect.Value, error) {
	if tp.Elem().Kind() == reflect.Pointer {
		err := fmt.Errorf("non_empty_value: unsupported type: %v", tp)
		return reflect.Value{}, err
	}
	data, err := non_empty_value(tp.Elem())
	if err != nil {
		return reflect.Value{}, err
	}
	ptr := reflect.New(tp.Elem())
	ptr.Elem().Set(data)
	return ptr, nil
}

func non_empty_slice(tp reflect.Type) (reflect.Value, error) {
	slice := reflect.MakeSlice(tp, 0, 0)
	elem, err := non_empty_value(tp.Elem())
	if err != nil {
		return slice, err
	}
	return reflect.Append(slice, elem), nil
}

func non_empty_map(maptp reflect.Type) (reflect.Value, error) {
	mapobj := reflect.MakeMap(maptp)
	key, err := non_empty_value(maptp.Key())
	if err != nil {
		return mapobj, err
	}
	val, err := non_empty_value(maptp.Elem())
	if err != nil {
		return mapobj, err
	}
	mapobj.SetMapIndex(key, val)
	return mapobj, nil
}

func non_empty_struct(tp reflect.Type) (reflect.Value, error) {
	strct := reflect.New(tp)
	elt := strct.Elem()
	for i := range tp.NumField() {
		val, err := non_empty_value(tp.Field(i).Type)
		if err != nil {
			return elt, err
		}
		elt.Field(i).Set(val)
	}
	return elt, nil
}
