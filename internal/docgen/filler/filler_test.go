package filler_test

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/docgen/filler"
)

type Nums struct {
	Int     int
	Int8    int8
	Int16   int16
	Int32   int32
	Int64   int64
	Uint    uint
	Uint8   uint8
	Uint16  uint16
	Uint32  uint32
	Uint64  uint64
	Float32 float32
	Float64 float32
}

type Toplevel struct {
	Nums   Nums              `json:"nums"`
	Bools  []bool            `json:"bools"`
	Map    map[string][]byte `json:"map"`
	Anys   []any             `json:"anys"`
	PtrInt *int              `json:"ptr_int"`
}

type Unsup struct {
	Field chan int
}

func TestNonZeroStruct(t *testing.T) {
	var neg int = -1
	expected := Toplevel{
		Nums: Nums{
			Int:     -1,
			Int8:    -1,
			Int16:   -1,
			Int32:   -1,
			Int64:   -1,
			Uint:    1,
			Uint8:   1,
			Uint16:  1,
			Uint32:  1,
			Uint64:  1,
			Float32: 1.5,
			Float64: 1.5,
		},
		Bools:  []bool{true},
		Map:    map[string][]byte{"string": {1, 2, 3}},
		Anys:   []any{"any"},
		PtrInt: &neg,
	}

	non_empty, err := filler.NonEmpty[Toplevel]()
	assert.NoError(t, err)

	assert.Equal(t, expected, non_empty)

	tp := reflect.TypeOf(Toplevel{})
	non_empty_any, err := filler.NonEmptyDyn(tp)
	assert.NoError(t, err)
	assert.Equal(t, expected, non_empty_any)
}

func TestUnsupported(t *testing.T) {
	_, err := filler.NonEmpty[Unsup]()
	assert.Equal(
		t,
		"non_empty_value: unsupported type: chan int",
		err.Error(),
	)

	_, err = filler.NonEmptyDyn(reflect.TypeOf(Unsup{}))
	assert.Equal(
		t,
		"non_empty_value: unsupported type: chan int",
		err.Error(),
	)
}

func TestUnsupportedRec(t *testing.T) {
	type InSlice struct {
		Field []Unsup
	}
	type InMapKey struct {
		Field map[Unsup]string
	}
	type InMapVal struct {
		Field map[string]Unsup
	}
	type InPtr struct {
		Field *Unsup
	}

	var err error
	msg := "non_empty_value: unsupported type: chan int"

	_, err = filler.NonEmpty[InSlice]()
	assert.Equal(t, msg, err.Error())

	_, err = filler.NonEmpty[InMapKey]()
	assert.Equal(t, msg, err.Error())

	_, err = filler.NonEmpty[InMapVal]()
	assert.Equal(t, msg, err.Error())

	_, err = filler.NonEmpty[InPtr]()
	assert.Equal(t, msg, err.Error())
}

func TestPtrToPtrErr(t *testing.T) {
	type Tp struct {
		Field **int
	}
	_, err := filler.NonEmpty[Tp]()
	assert.Equal(t, "non_empty_value: unsupported type: **int", err.Error())
}

// NonEmptyDyn produces value instead of pointer
func TestPtr(t *testing.T) {
	var obj *int
	val, err := filler.NonEmptyDyn(reflect.TypeOf(obj))
	assert.NoError(t, err)
	assert.Equal(t, val, int(-1))
}
