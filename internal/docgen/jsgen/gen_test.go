package jsgen_test

import (
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/docgen/jsgen"
	"gitlab.com/asvedr/srv/internal/html"
)

type Nums struct {
	X int  `json:"x"`
	Y uint `json:"y"`
}

type Toplevel struct {
	Nums  Nums              `json:"nums"`
	Bools []bool            `json:"bools"`
	Map   map[string][]byte `json:"map"`
	Anys  []any             `json:"anys"`
}

type Unsup struct {
	Field chan int
}

const doc_value = "<div class=\"value json\"><pre>{\n \"nums\": {\n  \"x\": -1,\n  \"y\": 1\n },\n \"bools\": [\n  true\n ],\n \"map\": {\n  \"string\": \"AQID\"\n },\n \"anys\": [\n  \"any\"\n ]\n}</pre></div>"

var value_string = html.Div("value string", "string")

var value_bytes = html.Div("value bytes", "bytes")

func TestGenDocValue(t *testing.T) {
	schema, err := jsgen.GenDocType(reflect.TypeOf(Toplevel{}))
	assert.NoError(t, err)
	assert.Equal(t, doc_value, schema)

	schema, err = jsgen.GenDocType(reflect.TypeOf([]byte{}))
	assert.NoError(t, err)
	assert.Equal(t, value_bytes, schema)

	schema, err = jsgen.GenDocType(reflect.TypeOf(""))
	assert.NoError(t, err)
	assert.Equal(t, value_string, schema)
}

func TestGenDocDyn(t *testing.T) {
	schema, err := jsgen.GenDocDyn(Toplevel{})
	assert.NoError(t, err)
	assert.Equal(t, doc_value, schema)

	schema, err = jsgen.GenDocDyn([]byte{1, 2, 3})
	assert.NoError(t, err)
	assert.Equal(t, value_bytes, schema)

	schema, err = jsgen.GenDocDyn("")
	assert.NoError(t, err)
	assert.Equal(t, value_string, schema)
}

func TestDumpDyn(t *testing.T) {
	schema, err := jsgen.DumpDyn(map[string]any{
		"x": 1,
		"y": []string{"a", "b"},
	})
	assert.NoError(t, err)
	exp := []string{
		`<div class="value json"><pre>{`,
		` "x": 1,`,
		` "y": [`,
		`  "a",`,
		`  "b"`,
		` ]`,
		`}</pre></div>`,
	}
	assert.Equal(t, strings.Join(exp, "\n"), schema)
}

func TestUnsupportable(t *testing.T) {
	msg := "can not gen doc for type jsgen_test.Unsup: non_empty_value: unsupported type: chan int"

	_, err := jsgen.GenDocType(reflect.TypeOf(Unsup{}))
	assert.Equal(t, msg, err.Error())

	_, err = jsgen.GenDocDyn(Unsup{})
	assert.Equal(t, msg, err.Error())
}
