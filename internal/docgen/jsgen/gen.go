package jsgen

import (
	"encoding/json"
	"fmt"
	"reflect"

	"gitlab.com/asvedr/srv/internal/docgen/filler"
	"gitlab.com/asvedr/srv/internal/html"
)

const indent = " "

var string_tp = reflect.TypeOf("")
var bts_tp = reflect.TypeOf([]byte{})

func GenDocType(tp reflect.Type) (string, error) {
	if tp == string_tp {
		return string_value(), nil
	}
	if tp == bts_tp {
		return bytes_value(), nil
	}
	non_empty, err := filler.NonEmptyDyn(tp)
	if err != nil {
		return "", wrap_err(err, tp)
	}
	val, err := json_value(non_empty)
	return val, wrap_err(err, tp)
}

func GenDocDyn(t any) (string, error) {
	tp := reflect.TypeOf(t)
	if tp == string_tp {
		return string_value(), nil
	}
	if tp == bts_tp {
		return bytes_value(), nil
	}
	non_empty, err := filler.NonEmptyDyn(tp)
	if err != nil {
		return "", wrap_err(err, tp)
	}
	val, err := json_value(non_empty)
	return val, wrap_err(err, tp)
}

func DumpDyn(t any) (string, error) {
	val, err := json_value(t)
	return val, wrap_err(err, reflect.TypeOf(t))
}

func DumpDynFlat(t any) (string, error) {
	bts, err := json.Marshal(t)
	return html.Div("value json", string(bts)), err
}

func string_value() string {
	return html.Div("value string", "string")
}

func bytes_value() string {
	return html.Div("value bytes", "bytes")
}

func json_value(src any) (string, error) {
	bts, err := json.MarshalIndent(src, "", indent)
	return html.Div("value json", "<pre>"+string(bts)+"</pre>"), err
}

func wrap_err(err error, tp reflect.Type) error {
	if err == nil {
		return nil
	}
	return fmt.Errorf("can not gen doc for type %v: %v", tp, err)
}
