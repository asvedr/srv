package std_engine

import (
	"net/http"

	"gitlab.com/asvedr/srv/internal/proto"
)

type StdEngine struct{}

var _ proto.IEngine = StdEngine{}

type handler struct {
	dispatcher proto.IDispatcher
}

func (self handler) ServeHTTP(
	out http.ResponseWriter,
	in *http.Request,
) {
	resp := self.dispatcher.Dispatch(AdaptReq(in, out))
	out.WriteHeader(resp.Status)
	out.Write(resp.Data)
}

func (StdEngine) Spawn(
	addr string,
	dispatcher proto.IDispatcher,
	tls_cert string,
	tls_key string,
) error {
	h := handler{dispatcher: dispatcher}
	if tls_cert == "" || tls_key == "" {
		return http.ListenAndServe(addr, h)
	}
	return http.ListenAndServeTLS(addr, tls_cert, tls_key, h)
}
