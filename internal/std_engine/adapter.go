package std_engine

import (
	"errors"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"

	"gitlab.com/asvedr/srv/internal/proto"
)

type adapted_req struct {
	src   *http.Request
	res   http.ResponseWriter
	query url.Values
}

func (self *adapted_req) Method() string {
	return self.src.Method
}

func (self *adapted_req) Path() string {
	return self.src.URL.Path
}

func (self *adapted_req) QueryRaw() string {
	return self.src.URL.RawQuery
}

func (self *adapted_req) QueryGet(key string) (string, bool) {
	self.force_query()
	lst := self.query[key]
	if len(lst) > 0 {
		return lst[0], true
	}
	return "", false
}

func (self *adapted_req) QueryGetMany(key string) ([]string, bool) {
	self.force_query()
	lst, ok := self.query[key]
	return lst, ok
}

func (self *adapted_req) HeaderGet(key string) string {
	return self.src.Header.Get(key)

}

func (self *adapted_req) HeaderSet(key, val string) {
	self.src.Header.Set(key, val)
	self.res.Header().Set(key, val)
}

func (self *adapted_req) HeaderGetMany(key string) []string {
	return self.src.Header.Values(key)
}

func (self *adapted_req) GetBody() ([]byte, error) {
	defer self.src.Body.Close()
	return io.ReadAll(self.src.Body)
}

func (self *adapted_req) FormFile(key string) (*multipart.FileHeader, error) {
	_, hdr, err := self.src.FormFile(key)
	if errors.Is(err, http.ErrMissingFile) {
		return nil, nil
	}
	return hdr, err
}

func (self *adapted_req) FormValue(key string) string {
	return self.src.FormValue(key)
}

func (self *adapted_req) force_query() {
	if self.query == nil {
		self.query = self.src.URL.Query()
	}
}

func AdaptReq(req *http.Request, res http.ResponseWriter) proto.IRawRequest {
	return &adapted_req{src: req, res: res}
}
