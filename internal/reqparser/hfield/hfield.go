package hfield

import (
	"fmt"
	"reflect"

	"gitlab.com/asvedr/srv/internal/names"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type F struct {
	types.HParam
	Parse func(string) (reflect.Value, error)
}

func New(field reflect.StructField) (F, error) {
	tp := field.Type
	name := field.Tag.Get(names.HeaderTagName)
	if name == "" {
		return F{}, fmt.Errorf(
			`header field %v has no "%s" tag`,
			field.Name,
			names.HeaderTagName,
		)
	}
	ptr := reflect.New(tp).Interface()
	int_prs, casted := ptr.(proto.IHeaderParam)
	if !casted {
		return F{}, fmt.Errorf("unsupported type: %v (try implement srv.IHeaderParam)", tp)
	}
	parser := interface_parser(tp, int_prs)
	hparam := types.HParam{Key: name, Type: tp}
	return F{HParam: hparam, Parse: parser}, nil
}

func interface_parser(tp reflect.Type, parser proto.IHeaderParam) func(string) (reflect.Value, error) {
	return func(src string) (reflect.Value, error) {
		raw, err := parser.ParseHeader(src)
		if err != nil {
			return reflect.Value{}, err
		}
		if raw == nil {
			err = fmt.Errorf(
				"custom parser returns <nil> when expected %v", tp,
			)
			return reflect.Value{}, err
		}
		val := reflect.ValueOf(raw)
		if val.Type() != tp {
			err = fmt.Errorf(
				"custom parser returns %v when expected %v", val.Type(), tp,
			)
			return val, err
		}
		return val, nil
	}
}
