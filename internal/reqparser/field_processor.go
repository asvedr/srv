package reqparser

type field_processor[T any] struct {
	index  int
	name   string
	parser T
}
