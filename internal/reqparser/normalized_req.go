package reqparser

import (
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type normalized_req struct {
	req       proto.IRawRequest
	path_vars map[string]string
	context   types.Context
}

var _ proto.IQueryRequest = normalized_req{}

const escaped_brackets = "5B%5D"
const raw_brakets = "[]"

func normalize_req(
	req proto.IRawRequest,
	path_vars map[string]string,
	context types.Context,
) normalized_req {
	return normalized_req{
		req:       req,
		path_vars: path_vars,
		context:   context,
	}
}

func (self normalized_req) QueryGet(key string) (string, bool) {
	val, ok := self.path_vars[key]
	if ok {
		return val, true
	}
	return self.req.QueryGet(key)
}

func (self normalized_req) QueryGetMany(src string) ([]string, bool) {
	val, ok := self.path_vars[src]
	if ok {
		return []string{val}, true
	}
	for _, key := range key_variants(src) {
		val, ok := self.req.QueryGetMany(key)
		if ok {
			return val, ok
		}
	}
	return nil, false
}

func key_variants(key string) []string {
	return []string{
		key, key + raw_brakets, key + escaped_brackets,
	}
}
