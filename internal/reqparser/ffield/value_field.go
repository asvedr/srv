package ffield

import (
	"encoding/json"
	"reflect"

	"gitlab.com/asvedr/srv/internal/proto"
)

type ValueParser struct {
	key      string
	tp       reflect.Type
	default_ *string
	opt      bool
	parser   func(string) (reflect.Value, error)
}

var tp_str = reflect.TypeFor[string]()
var tp_str_opt = reflect.TypeFor[*string]()

func make_value_parser(
	key string,
	tp reflect.Type,
	default_ *string,
	opt bool,
) (*ValueParser, error) {
	return &ValueParser{
		key:      key,
		tp:       tp,
		default_: default_,
		opt:      opt,
		parser:   make_parser(tp),
	}, nil
}

func make_parser(tp reflect.Type) func(string) (reflect.Value, error) {
	intrf, casted := reflect.New(tp).Interface().(proto.IFormParam)
	switch {
	case tp == tp_str:
		return func(src string) (reflect.Value, error) {
			return reflect.ValueOf(src), nil
		}
	case tp == tp_str_opt:
		return func(src string) (reflect.Value, error) {
			return reflect.ValueOf(&src), nil
		}
	case casted:
		return func(src string) (reflect.Value, error) {
			val, err := intrf.ParseForm(src)
			return reflect.ValueOf(val), err
		}
	default:
		return func(src string) (reflect.Value, error) {
			return json_parser(tp, src)
		}
	}
}

func (self *ValueParser) Parse(src proto.IRawRequest) (reflect.Value, error) {
	val := src.FormValue(self.key)
	is_set := val != ""
	switch {
	case is_set:
		return self.parser(val)
	case self.default_ != nil:
		return self.parser(*self.default_)
	case self.opt:
		return reflect.Zero(self.tp), nil
	default:
		return reflect.Value{}, err_param_not_set
	}
}

func json_parser(tp reflect.Type, text string) (reflect.Value, error) {
	ptr := reflect.New(tp)
	err := json.Unmarshal([]byte(text), ptr.Interface())
	return ptr.Elem(), err
}
