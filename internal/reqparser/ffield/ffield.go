package ffield

import (
	"errors"
	"reflect"
	"strings"

	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/names"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

var err_param_not_set = errors.New("parameter not set")

type F struct {
	Key         string
	Type        reflect.Type
	Default     *string
	Optional    bool
	FileParser  *FilePaser
	ValueParser *ValueParser
}

var file_type = reflect.TypeFor[types.FormFile]()
var opt_file_type = reflect.TypeFor[*types.FormFile]()

func New(field reflect.StructField) (F, error) {
	key := get_key(field)
	res := F{
		Key:      key,
		Type:     field.Type,
		Default:  get_default(field),
		Optional: field.Type.Kind() == reflect.Pointer,
	}
	var err error
	switch field.Type {
	case file_type:
		res.FileParser, err = make_file_parser(
			res.Key, res.Optional, false,
		)
	case opt_file_type:
		res.FileParser, err = make_file_parser(
			res.Key, true, true,
		)
	default:
		res.ValueParser, err = make_value_parser(
			res.Key, res.Type, res.Default, res.Optional,
		)
	}
	return res, err
}

func (self F) Parse(src proto.IRawRequest) (res reflect.Value, err error) {
	if self.FileParser != nil {
		res, err = self.FileParser.Parse(src)
	} else {
		res, err = self.ValueParser.Parse(src)
	}
	if err != nil {
		err = errs.ErrInvalidForm{
			Param: self.Key,
			Cause: err,
		}
	}
	return
}

func (self F) FParam() types.FParam {
	return types.FParam{
		Key:      self.Key,
		Type:     self.Type,
		Default:  self.Default,
		Optional: self.Optional,
		IsFile:   self.FileParser != nil,
	}
}

func get_key(field reflect.StructField) string {
	name := field.Tag.Get(names.QueryTagName)
	if name == "" {
		name = strings.ToLower(field.Name)
	}
	return name
}

func get_default(field reflect.StructField) *string {
	val, found := field.Tag.Lookup(names.QueryTagDefault)
	if !found {
		return nil
	}
	return &val
}
