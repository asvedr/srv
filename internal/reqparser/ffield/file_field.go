package ffield

import (
	"errors"
	"mime/multipart"
	"reflect"

	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type FilePaser struct {
	key    string
	is_opt bool
	is_ptr bool
}

var ErrMissingFile = errors.New("missing file")

func make_file_parser(
	key string, is_opt, is_ptr bool,
) (*FilePaser, error) {
	return &FilePaser{
		key:    key,
		is_opt: is_opt,
		is_ptr: is_ptr,
	}, nil
}

func (self *FilePaser) Parse(src proto.IRawRequest) (reflect.Value, error) {
	header, err := src.FormFile(self.key)
	if err != nil {
		return reflect.Value{}, err
	}
	if header != nil {
		return self.to_out(header), nil
	}
	var nil_ptr *types.FormFile
	res := reflect.ValueOf(nil_ptr)
	if !self.is_opt {
		err = ErrMissingFile
	}
	return res, err
}

func (self *FilePaser) to_out(header *multipart.FileHeader) reflect.Value {
	res := types.FormFile{Header: header}
	if self.is_ptr {
		return reflect.ValueOf(&res)
	}
	return reflect.ValueOf(res)
}
