package reqparser

import (
	"fmt"

	"gitlab.com/asvedr/srv/internal/errs"
)

func err_query_already_declared(param, field, prev string) error {
	reason := fmt.Sprintf(
		`query param "%s" already declared at field "%s"`,
		param,
		prev,
	)
	return errs.ErrReqIsInvalid{Field: field, Reason: reason}
}

func err_form_already_declared(param, field, prev string) error {
	reason := fmt.Sprintf(
		`form param "%s" already declared at field "%s"`,
		param,
		prev,
	)
	return errs.ErrReqIsInvalid{Field: field, Reason: reason}
}

func err_already_declared(field, slot, prev string) error {
	return errs.ErrReqIsInvalid{
		Field:  field,
		Reason: slot + " already declared at " + prev,
	}
}

func err_form_with_other_params(field string) error {
	return errs.ErrReqIsInvalid{
		Field:  field,
		Reason: "can not use form with other params",
	}
}
