package qfield

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"gitlab.com/asvedr/srv/internal/names"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type F struct {
	types.QParam
	parser func([]string) (reflect.Value, error)
}

var bool_values = map[string]bool{
	"true":  true,
	"1":     true,
	"false": false,
	"0":     false,
}

var err_invalid_boolean = errors.New("invalid boolean")
var err_param_not_set = errors.New("parameter not set")

func New(field reflect.StructField) (F, error) {
	tp := field.Type
	f, err := make_value_parser(tp)
	qparam := types.QParam{
		Key:      get_key(field),
		Type:     tp,
		Default:  get_default(field),
		Optional: tp.Kind() == reflect.Pointer,
		List:     tp.Kind() == reflect.Slice,
	}
	return F{QParam: qparam, parser: f}, err
}

var empty_list = []string{}

func (self F) Parse(src proto.IQueryRequest) (reflect.Value, error) {
	var ok bool
	var values []string
	var value string
	if self.List {
		values, ok = src.QueryGetMany(self.Key)
	} else {
		value, ok = src.QueryGet(self.Key)
		values = []string{value}
	}
	if !ok {
		if self.Default != nil {
			values = self.Default
		} else if self.Optional {
			return reflect.Zero(self.Type), nil
		} else if self.List {
			values = empty_list
		} else {
			return reflect.Value{}, err_param_not_set
		}
	}
	return self.parser(values)
}

func get_key(field reflect.StructField) string {
	name := field.Tag.Get(names.QueryTagName)
	if name == "" {
		name = strings.ToLower(field.Name)
	}
	return name
}

func get_default(field reflect.StructField) []string {
	val, found := field.Tag.Lookup(names.QueryTagDefault)
	if !found {
		return nil
	}
	return []string{val}
}

func make_value_parser(
	tp reflect.Type,
) (func([]string) (reflect.Value, error), error) {
	if tp.Kind() == reflect.Pointer {
		p, err := make_single_value_parser(tp.Elem())
		return opt_parser(tp, p), err
	} else if tp.Kind() == reflect.Slice {
		p, err := make_single_value_parser(tp.Elem())
		return slice_parser(tp, p), err
	} else {
		p, err := make_single_value_parser(tp)
		return single_parser(p), err
	}
}

func make_single_value_parser(tp reflect.Type) (func(string) (reflect.Value, error), error) {
	switch tp.Kind() {
	case reflect.Bool:
		return bool_parser, nil
	case reflect.Int:
		return int_parser, nil
	case reflect.Int8:
		return int_sized_parser(8), nil
	case reflect.Int16:
		return int_sized_parser(16), nil
	case reflect.Int32:
		return int_sized_parser(32), nil
	case reflect.Int64:
		return int_sized_parser(64), nil
	case reflect.Uint:
		return uint_parser, nil
	case reflect.Uint8:
		return uint_sized_parser(8), nil
	case reflect.Uint16:
		return uint_sized_parser(16), nil
	case reflect.Uint32:
		return uint_sized_parser(32), nil
	case reflect.Uint64:
		return uint_sized_parser(64), nil
	case reflect.Float32:
		return float_parser(32), nil
	case reflect.Float64:
		return float_parser(64), nil
	case reflect.String:
		return string_parser, nil
	}
	ptr_to_tp := reflect.New(tp).Interface()
	int_prs, casted := ptr_to_tp.(proto.IQueryParam)
	if !casted {
		return nil, fmt.Errorf("unsupported type: %v (try implement srv.IQueryParam)", tp)
	}
	return interface_parser(tp, int_prs), nil
}

func bool_parser(src string) (reflect.Value, error) {
	val, found := bool_values[src]
	if !found {
		return reflect.Value{}, err_invalid_boolean
	}
	return reflect.ValueOf(val), nil
}

func int_parser(src string) (reflect.Value, error) {
	val, err := strconv.Atoi(src)
	if err != nil {
		return reflect.Value{}, err
	}
	return reflect.ValueOf(val), nil
}

func int_sized_parser(bits int) func(string) (reflect.Value, error) {
	var cast func(int64) reflect.Value
	switch bits {
	case 8:
		cast = func(i int64) reflect.Value {
			return reflect.ValueOf(int8(i))
		}
	case 16:
		cast = func(i int64) reflect.Value {
			return reflect.ValueOf(int16(i))
		}
	case 32:
		cast = func(i int64) reflect.Value {
			return reflect.ValueOf(int32(i))
		}
	case 64:
		cast = func(i int64) reflect.Value {
			return reflect.ValueOf(i)
		}
	}
	return func(src string) (reflect.Value, error) {
		val, err := strconv.ParseInt(src, 10, bits)
		if err != nil {
			return reflect.Value{}, err
		}
		return cast(val), nil
	}
}

func uint_parser(src string) (reflect.Value, error) {
	val, err := strconv.ParseUint(src, 10, 64)
	if err != nil {
		return reflect.Value{}, err
	}
	return reflect.ValueOf(uint(val)), nil
}

func uint_sized_parser(bits int) func(string) (reflect.Value, error) {
	var cast func(uint64) reflect.Value
	switch bits {
	case 8:
		cast = func(i uint64) reflect.Value {
			return reflect.ValueOf(uint8(i))
		}
	case 16:
		cast = func(i uint64) reflect.Value {
			return reflect.ValueOf(uint16(i))
		}
	case 32:
		cast = func(i uint64) reflect.Value {
			return reflect.ValueOf(uint32(i))
		}
	case 64:
		cast = func(i uint64) reflect.Value {
			return reflect.ValueOf(i)
		}
	}
	return func(src string) (reflect.Value, error) {
		val, err := strconv.ParseUint(src, 10, bits)
		if err != nil {
			return reflect.Value{}, err
		}
		return cast(val), nil
	}
}

func float_parser(bits int) func(string) (reflect.Value, error) {
	var cast func(float64) reflect.Value
	switch bits {
	case 32:
		cast = func(i float64) reflect.Value {
			return reflect.ValueOf(float32(i))
		}
	case 64:
		cast = func(i float64) reflect.Value {
			return reflect.ValueOf(i)
		}
	}
	return func(src string) (reflect.Value, error) {
		val, err := strconv.ParseFloat(src, bits)
		if err != nil {
			return reflect.Value{}, err
		}
		return cast(val), nil
	}
}

func string_parser(src string) (reflect.Value, error) {
	return reflect.ValueOf(src), nil
}

func interface_parser(tp reflect.Type, parser proto.IQueryParam) func(string) (reflect.Value, error) {
	return func(src string) (reflect.Value, error) {
		raw, err := parser.ParseQuery(src)
		if err != nil {
			return reflect.Value{}, err
		}
		if raw == nil {
			err = fmt.Errorf(
				"custom parser returns <nil> when expected %v", tp,
			)
			return reflect.Value{}, err
		}
		val := reflect.ValueOf(raw)
		if val.Type() != tp {
			err = fmt.Errorf(
				"custom parser returns %v when expected %v", val.Type(), tp,
			)
			return val, err
		}
		return val, nil
	}
}

func single_parser(
	f func(string) (reflect.Value, error),
) func([]string) (reflect.Value, error) {
	return func(args []string) (reflect.Value, error) {
		if len(args) == 0 {
			return reflect.Value{}, err_param_not_set
		}
		return f(args[0])
	}
}

func opt_parser(
	ptr_tp reflect.Type,
	f func(string) (reflect.Value, error),
) func([]string) (reflect.Value, error) {
	return func(args []string) (reflect.Value, error) {
		if len(args) == 0 {
			return reflect.Zero(ptr_tp), nil
		}
		val, err := f(args[0])
		if err != nil {
			return val, err
		}
		ptr_val := reflect.New(ptr_tp.Elem())
		ptr_val.Elem().Set(val)
		return ptr_val, nil
	}
}

func slice_parser(
	tp reflect.Type,
	f func(string) (reflect.Value, error),
) func([]string) (reflect.Value, error) {
	return func(args []string) (reflect.Value, error) {
		slice := reflect.MakeSlice(tp, 0, 0)
		for _, arg := range args {
			item, err := f(arg)
			if err != nil {
				return reflect.Value{}, err
			}
			slice = reflect.Append(slice, item)
		}
		return slice, nil
	}
}
