package bfield

import (
	"encoding/json"
	"reflect"
)

type F struct {
	Type  reflect.Type
	Parse func([]byte) (reflect.Value, error)
}

var tp_bytes = reflect.TypeOf([]byte{})
var tp_str = reflect.TypeOf("")

func New(field reflect.StructField) F {
	tp := field.Type
	var f func([]byte) (reflect.Value, error)
	if tp == tp_bytes {
		f = deser_bytes
	} else if tp == tp_str {
		f = deser_string
	} else {
		f = make_deser_json(tp)
	}
	return F{Type: tp, Parse: f}
}

func make_deser_json(tp reflect.Type) func([]byte) (reflect.Value, error) {
	return func(src []byte) (reflect.Value, error) {
		ptr := reflect.New(tp)
		err := json.Unmarshal(src, ptr.Interface())
		return ptr.Elem(), err
	}
}

func deser_string(src []byte) (reflect.Value, error) {
	return reflect.ValueOf(string(src)), nil
}

func deser_bytes(src []byte) (reflect.Value, error) {
	return reflect.ValueOf(src), nil
}
