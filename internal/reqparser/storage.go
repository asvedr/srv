package reqparser

import (
	"reflect"
	"sync"
)

var storage_map = map[reflect.Type]*Parser{}
var storage_mu sync.Mutex
