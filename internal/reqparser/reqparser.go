package reqparser

import (
	"errors"
	"net/textproto"
	"reflect"

	"gitlab.com/asvedr/collections/slices"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/names"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/reqparser/bfield"
	"gitlab.com/asvedr/srv/internal/reqparser/cfield"
	"gitlab.com/asvedr/srv/internal/reqparser/ffield"
	"gitlab.com/asvedr/srv/internal/reqparser/hfield"
	"gitlab.com/asvedr/srv/internal/reqparser/qfield"
	"gitlab.com/asvedr/srv/internal/types"
)

type Parser struct {
	tp      reflect.Type
	include []field_processor[*Parser]
	headers []field_processor[hfield.F]
	query   []field_processor[qfield.F]
	body    *field_processor[bfield.F]
	form    []field_processor[ffield.F]
	context []field_processor[cfield.F]
}

var _ proto.IReqParser = (*Parser)(nil)

func New[T any]() (*Parser, error) {
	var t T
	return NewDyn(t)
}

func NewDyn(strct any) (*Parser, error) {
	storage_mu.Lock()
	defer storage_mu.Unlock()
	return get_or_create(reflect.TypeOf(strct))
}

func get_or_create(tp reflect.Type) (*Parser, error) {
	p := storage_map[tp]
	if p != nil {
		return p, nil
	}
	p, err := new_tp(tp)
	if err != nil {
		return nil, err
	}
	storage_map[tp] = p
	return p, nil
}

func new_tp(tp reflect.Type) (*Parser, error) {
	self := &Parser{tp: tp}
	var err error
	for i := range tp.NumField() {
		field := tp.Field(i)
		mode := field.Tag.Get(names.QueryTagSrv)
		switch mode {
		case names.SrvReqModeInclude:
			err = self.include_field(i, field)
		case names.SrvReqModeHeader:
			err = self.add_header_field(i, field)
		case names.SrvReqModeQuery:
			err = self.add_query_field(i, field)
		case names.SrvReqModeBody:
			err = self.add_body_field(i, field)
		case names.SrvReqModeForm:
			err = self.add_form_field(i, field)
		case names.SrvReqModeCtx:
			err = self.add_context_field(i, field)
		default:
			err = errs.ErrReqIsInvalid{
				Field:  field.Name,
				Reason: "tag " + names.QueryTagSrv + " not set",
			}
		}
		if err != nil {
			return nil, err
		}
	}
	err = self.validate()
	return self, err
}

func (self *Parser) Parse(
	req proto.IRawRequest,
	path_vars map[string]string,
	context types.Context,
) (any, error) {
	val, err := self.parse_value(normalize_req(req, path_vars, context))
	return val.Interface(), err
}

func (self *Parser) HParams() []types.HParam {
	res := []types.HParam{}
	for _, field := range self.include {
		res = append(res, field.parser.HParams()...)
	}
	for _, field := range self.headers {
		res = append(res, field.parser.HParam)
	}
	return res
}

func (self *Parser) QParams() []types.QParam {
	res := []types.QParam{}
	for _, field := range self.include {
		res = append(res, field.parser.QParams()...)
	}
	for _, field := range self.query {
		res = append(res, field.parser.QParam)
	}
	return res
}

func (self *Parser) FParams() []types.FParam {
	res := []types.FParam{}
	for _, field := range self.include {
		res = append(res, field.parser.FParams()...)
	}
	for _, field := range self.form {
		res = append(res, field.parser.FParam())
	}
	return res
}

func (self *Parser) BodyType() *reflect.Type {
	if self.body != nil {
		return &self.body.parser.Type
	}
	for _, field := range self.include {
		body := field.parser.BodyType()
		if body != nil {
			return body
		}
	}
	return nil
}

func (self *Parser) parse_value(req normalized_req) (reflect.Value, error) {
	res := reflect.New(self.tp).Elem()
	err := process_fields(self.include, parse_include, req, res)
	if err != nil {
		return res, err
	}
	err = process_fields(self.headers, self.parse_header_param, req, res)
	if err != nil {
		return res, err
	}
	err = process_fields(self.query, self.parse_query_param, req, res)
	if err != nil {
		return res, err
	}
	err = process_fields(self.context, self.parse_context_param, req, res)
	if err != nil {
		return res, err
	}
	if self.body != nil {
		obj, err := self.parse_body(req)
		if err != nil {
			return res, err
		}
		res.Field(self.body.index).Set(obj)
	}
	err = process_fields(self.form, self.parse_form_param, req, res)
	return res, err
}

func parse_include(p *Parser, req normalized_req) (reflect.Value, error) {
	return p.parse_value(req)
}

func process_fields[F any](
	fields []field_processor[F],
	f func(F, normalized_req) (reflect.Value, error),
	req normalized_req,
	res reflect.Value,
) error {
	for _, field := range fields {
		obj, err := f(field.parser, req)
		if err != nil {
			return err
		}
		res.Field(field.index).Set(obj)
	}
	return nil
}

func (self *Parser) parse_header_param(
	parser hfield.F,
	req normalized_req,
) (reflect.Value, error) {
	key := textproto.CanonicalMIMEHeaderKey(parser.Key)
	header := req.req.HeaderGet(key)
	obj, err := parser.Parse(header)
	if err != nil {
		err = errs.ErrInvalidHeader{
			Param: parser.Key,
			Cause: err,
		}
	}
	return obj, err
}

func (self *Parser) parse_query_param(
	parser qfield.F,
	req normalized_req,
) (reflect.Value, error) {
	obj, err := parser.Parse(req)
	if err != nil {
		err = errs.ErrInvalidQuery{Param: parser.Key, Cause: err}
	}
	return obj, err
}

func (self *Parser) parse_form_param(
	parser ffield.F,
	req normalized_req,
) (reflect.Value, error) {
	return parser.Parse(req.req)
}

func (self *Parser) parse_context_param(
	parser cfield.F,
	req normalized_req,
) (reflect.Value, error) {
	obj, err := parser.Parse(req.context)
	if err != nil {
		err = errs.ErrInvalidContext{Param: parser.Key, Cause: err}
	}
	return obj, err
}

var err_can_not_read = errors.New("can not read")

func (self *Parser) parse_body(req normalized_req) (reflect.Value, error) {
	var obj reflect.Value
	body_bts, err := req.req.GetBody()
	if err != nil {
		return obj, errs.ErrInternal{Cause: err_can_not_read}
	}
	obj, err = self.body.parser.Parse(body_bts)
	if err != nil {
		return obj, errs.ErrInvalidBody{Cause: err}
	}
	return obj, nil
}

func (self *Parser) include_field(
	id int,
	field reflect.StructField,
) error {
	proc, err := get_or_create(field.Type)
	fp := field_processor[*Parser]{
		index:  id,
		name:   field.Name,
		parser: proc,
	}
	self.include = append(self.include, fp)
	return err
}

func (self *Parser) add_header_field(
	id int,
	field reflect.StructField,
) error {
	proc, err := hfield.New(field)
	fp := field_processor[hfield.F]{
		index:  id,
		name:   field.Name,
		parser: proc,
	}
	self.headers = append(self.headers, fp)
	return err
}

func (self *Parser) add_query_field(
	id int,
	field reflect.StructField,
) error {
	proc, err := qfield.New(field)
	fp := field_processor[qfield.F]{
		index:  id,
		name:   field.Name,
		parser: proc,
	}
	self.query = append(self.query, fp)
	return err
}

func (self *Parser) add_body_field(
	id int,
	field reflect.StructField,
) error {
	if self.body != nil {
		return err_already_declared(
			field.Name, "body", self.body.name,
		)
	}
	proc := bfield.New(field)
	self.body = &field_processor[bfield.F]{
		index:  id,
		name:   field.Name,
		parser: proc,
	}
	return nil
}

func (self *Parser) add_form_field(
	id int,
	field reflect.StructField,
) error {
	parser, err := ffield.New(field)
	if err != nil {
		return err
	}
	field_proc := field_processor[ffield.F]{
		index:  id,
		name:   field.Name,
		parser: parser,
	}
	self.form = append(self.form, field_proc)
	return nil
}

func (self *Parser) add_context_field(
	id int,
	field reflect.StructField,
) error {
	parser, err := cfield.New(field)
	if err != nil {
		return err
	}
	proc := field_processor[cfield.F]{
		index:  id,
		name:   field.Name,
		parser: parser,
	}
	self.context = append(self.context, proc)
	return nil
}

func (self *Parser) validate() error {
	var body_field string
	if self.body != nil {
		body_field = self.body.name
	}
	q_used := map[string]string{}
	f_used := map[string]string{}

	for _, field := range self.include {
		if field.parser.BodyType() != nil {
			if body_field != "" {
				return err_already_declared(field.name, "body", body_field)
			}
			body_field = field.name
		}
		for _, fparam := range field.parser.FParams() {
			key := fparam.Key
			if f_used[key] != "" {
				return err_form_already_declared(
					key,
					field.name,
					f_used[key],
				)
			}
			f_used[key] = field.name
		}
		for _, qparam := range field.parser.QParams() {
			key := qparam.Key
			if q_used[key] != "" {
				return err_query_already_declared(
					key,
					field.name,
					q_used[key],
				)
			}
			q_used[key] = field.name
		}
	}
	for _, field := range self.form {
		key := field.parser.Key
		if f_used[key] != "" {
			return err_form_already_declared(
				key,
				field.name,
				f_used[key],
			)
		}
		f_used[key] = field.name
	}
	for _, field := range self.query {
		key := field.parser.QParam.Key
		if q_used[key] != "" {
			return err_query_already_declared(
				key,
				field.name,
				q_used[key],
			)
		}
		q_used[key] = field.name
	}

	if len(f_used) > 0 && (body_field != "" || len(q_used) != 0) {
		f_fields := slices.MapValues(f_used)
		return err_form_with_other_params(f_fields[0])
	}
	return nil
}
