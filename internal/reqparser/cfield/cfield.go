package cfield

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/asvedr/srv/internal/names"
	"gitlab.com/asvedr/srv/internal/types"
)

type F struct {
	Key   string
	Type  reflect.Type
	IsPtr bool
	PtrTo reflect.Type
}

func New(field reflect.StructField) (F, error) {
	key := get_key(field)
	res := F{Key: key, Type: field.Type}
	if field.Type.Kind() == reflect.Pointer {
		res.IsPtr = true
		res.PtrTo = field.Type.Elem()
		if res.PtrTo.Kind() == reflect.Pointer {
			return res, fmt.Errorf(
				"unsupported type: %v (pointer to pointer)",
				field.Type,
			)
		}
	}
	return res, nil
}

func (self F) Parse(src types.Context) (reflect.Value, error) {
	raw, found := src[self.Key]
	if !found {
		if self.IsPtr {
			return reflect.Zero(self.Type), nil
		} else {
			err := fmt.Errorf("context var %s not found", self.Key)
			return reflect.Value{}, err
		}
	}
	if raw == nil {
		return reflect.Zero(self.Type), nil
	}
	return self.coerse_type(reflect.ValueOf(raw))
}

func (self F) coerse_type(val reflect.Value) (reflect.Value, error) {
	tp := val.Type()
	if tp == self.Type {
		return val, nil
	}
	if tp == self.PtrTo {
		res := reflect.New(self.PtrTo)
		res.Elem().Set(val)
		return res, nil
	}
	return reflect.Value{}, fmt.Errorf(
		"expected %v, got %v", self.Type, val.Type(),
	)
}

func get_key(field reflect.StructField) string {
	name := field.Tag.Get(names.QueryTagName)
	if name == "" {
		name = strings.ToLower(field.Name)
	}
	return name
}
