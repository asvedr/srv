package errs

import (
	"fmt"
	"reflect"
)

type IToApiError interface {
	error
	ApiError() (status int, code string)
}

type ErrEmptyHandlers struct{}
type ErrCanNotUseTypeInQuery struct {
	Type  reflect.Type
	Cause error
}
type ErrCanNotUseTypeInForm struct {
	Type  reflect.Type
	Cause error
}
type ErrCanNotUseTypeInHeader struct {
	Type  reflect.Type
	Cause error
}
type ErrCanNotCreateFormParams struct {
	Handler string
	Cause   error
}

type ErrNotFound struct{}

type ErrMethodNotAllowed struct{}

type ErrInternal struct{ Cause error }

type ErrInvalidBody struct{ Cause error }
type ErrInvalidQuery struct {
	Param string
	Cause error
}
type ErrInvalidForm struct {
	Param string
	Cause error
}
type ErrInvalidContext struct {
	Param string
	Cause error
}
type ErrInvalidHeader struct {
	Param string
	Cause error
}

type ErrReqIsInvalid struct {
	Field  string
	Reason string
}

type ErrRestMustBeLast struct {
	Path string
}

type TlsNotFull struct{}

func (TlsNotFull) Error() string {
	return "tls key and cert must be set together"
}

func (self ErrRestMustBeLast) Error() string {
	return fmt.Sprintf("path(%s) rest obj must be last", self.Path)
}

func (self ErrReqIsInvalid) Error() string {
	return fmt.Sprintf("request is invalid. field: %s, reason: %s", self.Field, self.Reason)
}

func (self ErrCanNotCreateFormParams) Error() string {
	return fmt.Sprintf(
		"Can not create form params for %s: %v",
		self.Handler,
		self.Cause,
	)
}

func (self ErrCanNotUseTypeInQuery) Error() string {
	msg := fmt.Sprintf(
		"Can not use type %s in query(try implement srv.IQueryParam)",
		self.Type,
	)
	if self.Cause != nil {
		msg += "\nCause: " + self.Cause.Error()
	}
	return msg
}

func (self ErrCanNotUseTypeInForm) Error() string {
	msg := fmt.Sprintf(
		"Can not use type %s in form(try implement srv.IFormParam)",
		self.Type,
	)
	if self.Cause != nil {
		msg += "\nCause: " + self.Cause.Error()
	}
	return msg
}

func (self ErrCanNotUseTypeInHeader) Error() string {
	msg := fmt.Sprintf(
		"Can not use type %s in header(try implement srv.IHeaderParam)",
		self.Type,
	)
	if self.Cause != nil {
		msg += "\nCause: " + self.Cause.Error()
	}
	return msg
}

func (ErrEmptyHandlers) Error() string {
	return "Handlers list is empty"
}

func (ErrNotFound) Error() string {
	return "Url not found"
}

func (ErrNotFound) ApiError() (int, string) {
	return 404, "NOT_FOUND"
}

func (ErrMethodNotAllowed) Error() string {
	return "Method not allowed"
}

func (ErrMethodNotAllowed) ApiError() (int, string) {
	return 405, "NOT_ALLOWED"
}

func (self ErrInternal) Error() string {
	return fmt.Sprintf("INTERNAL_ERROR: %v", self.Cause)
}

func (ErrInternal) ApiError() (int, string) {
	return 500, "INTERNAL"
}

func (self ErrInvalidBody) Error() string {
	return fmt.Sprintf("INVALID_BODY: %v", self.Cause)
}

func (self ErrInvalidHeader) Error() string {
	return fmt.Sprintf(
		"Invalid header(%s): %v",
		self.Param,
		self.Cause,
	)
}

func (self ErrInvalidQuery) Error() string {
	return fmt.Sprintf(
		"Invalid query param(%s): %v",
		self.Param,
		self.Cause,
	)
}

func (self ErrInvalidForm) Error() string {
	return fmt.Sprintf(
		"Invalid form field(%s): %v",
		self.Param,
		self.Cause,
	)
}

func (self ErrInvalidContext) Error() string {
	return fmt.Sprintf(
		"Invalid context param(%s): %v",
		self.Param,
		self.Cause,
	)
}

func (self ErrInvalidContext) ApiError() (int, string) {
	return get_api_attrs(self.Cause, 400, "INVALID_CONTEXT")
}

func (self ErrInvalidBody) ApiError() (int, string) {
	return get_api_attrs(self.Cause, 400, "INVALID_BODY")
}

func (self ErrInvalidQuery) ApiError() (int, string) {
	return get_api_attrs(self.Cause, 400, "INVALID_QUERY")
}

func (self ErrInvalidForm) ApiError() (int, string) {
	return get_api_attrs(self.Cause, 400, "INVALID_FORM")
}

func (self ErrInvalidHeader) ApiError() (int, string) {
	return get_api_attrs(self.Cause, 400, "INVALID_HEADER")
}

func get_api_attrs(
	err error,
	default_status int,
	default_code string,
) (int, string) {
	if err == nil {
		return default_status, default_code
	}
	to_api_err, casted := err.(IToApiError)
	if !casted {
		return default_status, default_code
	}
	return to_api_err.ApiError()
}
