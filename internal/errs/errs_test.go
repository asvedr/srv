package errs_test

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/errs"
)

var common_errs = []error{
	errs.ErrEmptyHandlers{},
	errs.ErrCanNotUseTypeInQuery{},
	errs.ErrCanNotUseTypeInQuery{Cause: errs.ErrNotFound{}},
	errs.ErrCanNotUseTypeInForm{},
	errs.ErrCanNotUseTypeInForm{Cause: errs.ErrNotFound{}},
	errs.ErrCanNotUseTypeInHeader{},
	errs.ErrCanNotUseTypeInHeader{Cause: errs.ErrNotFound{}},
	errs.ErrCanNotCreateFormParams{},
	errs.ErrCanNotCreateFormParams{Cause: errs.ErrNotFound{}},
	errs.ErrReqIsInvalid{},
	errs.ErrInvalidHeader{},
	errs.ErrRestMustBeLast{},
	errs.TlsNotFull{},
}

var api_errs = []errs.IToApiError{
	errs.ErrNotFound{},
	errs.ErrMethodNotAllowed{},
	errs.ErrInternal{},
	errs.ErrInternal{Cause: errs.ErrNotFound{}},
	errs.ErrInvalidBody{},
	errs.ErrInvalidBody{Cause: errs.ErrNotFound{}},
	errs.ErrInvalidQuery{Cause: errs.ErrNotFound{}},
	errs.ErrInvalidContext{},
	errs.ErrInvalidHeader{},
	errs.ErrInvalidForm{},
}

func TestCommonError(t *testing.T) {
	for _, obj := range common_errs {
		t.Run(fmt.Sprintf("%T", obj), func(t *testing.T) {
			// No panic
			_ = obj.Error()
		})
	}
}

func TestApiError(t *testing.T) {
	for _, obj := range api_errs {
		t.Run(fmt.Sprintf("%T", obj), func(t *testing.T) {
			// No panic
			_ = obj.Error()
			_, _ = obj.ApiError()
		})
	}
}

func TestErrToApi(t *testing.T) {
	status, code := errs.ErrInvalidHeader{}.ApiError()
	assert.Equal(t, 400, status)
	assert.Equal(t, "INVALID_HEADER", code)

	status, code = errs.ErrInvalidHeader{
		Cause: errs.ErrNotFound{},
	}.ApiError()
	assert.Equal(t, 404, status)
	assert.Equal(t, "NOT_FOUND", code)

	status, code = errs.ErrInvalidHeader{
		Cause: errors.New("custom"),
	}.ApiError()
	assert.Equal(t, 400, status)
	assert.Equal(t, "INVALID_HEADER", code)
}
