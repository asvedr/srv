package html

import (
	"fmt"
	"strings"
)

func Div(
	class string,
	children ...string,
) string {
	res := fmt.Sprintf("<div class=\"%s\">", class)
	res += strings.Join(children, "")
	return res + "</div>"
}

func A(
	href string,
	text string,
) string {
	return fmt.Sprintf("<a href=\"%s\">%s</a>", href, text)
}
