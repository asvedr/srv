package router

import (
	"net/url"

	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type Router struct {
	path     types.PathPttrn
	children []proto.IRoutable
	Join     func(string, ...string) (string, error)
}

func New(
	path string,
	children []proto.IRoutable,
) proto.IRouter {
	pp, err := types.PathPttrn{}.Parse(path)
	if err != nil {
		panic(err)
	}
	return &Router{
		path:     pp,
		children: children,
		Join:     url.JoinPath,
	}
}

func (self *Router) AddChildren(children []proto.IRoutable) {
	self.children = append(self.children, children...)
}

func (self *Router) Path() types.PathPttrn {
	return self.path
}

func (self *Router) IsRouter() bool {
	return true
}

const empty_method string = ""

func (self *Router) Method() string {
	return empty_method
}

func (self *Router) Children() []proto.IRoutable {
	return self.children
}

func (self *Router) DowncastHandler() proto.IWrappedHandler {
	return nil
}

func (self *Router) DowncastRouter() proto.IRouter {
	return self
}
