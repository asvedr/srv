package proto

import (
	"mime/multipart"
	"reflect"

	"gitlab.com/asvedr/srv/internal/types"
)

type IQueryRequest interface {
	QueryGet(string) (string, bool)
	QueryGetMany(string) ([]string, bool)
}

type IHeaderRequest interface {
	HeaderSet(string, string)
	HeaderGet(string) string
	HeaderGetMany(string) []string
}

type IRawRequest interface {
	IQueryRequest
	IHeaderRequest
	Method() string
	Path() string
	QueryRaw() string
	GetBody() ([]byte, error)
	FormFile(string) (*multipart.FileHeader, error)
	FormValue(string) string
}

type IApiHandler[Request, Response any] interface {
	FormParams() any
	Errors() []error
	Method() string
	Path() string
	Description() string
	Handle(Request) (Response, error)
}

type IRoutable interface {
	Path() types.PathPttrn
	IsRouter() bool
	Method() string

	// WithPrefix(string) IRoutable
	Children() []IRoutable
	DowncastHandler() IWrappedHandler
	DowncastRouter() IRouter
}

type IRouter interface {
	IRoutable
	AddChildren([]IRoutable)
}

type IWrappedHandler interface {
	IRoutable
	FormParams() any
	// Method() string
	// Path() string
	Description(full_path string) string
	RequestParser() IReqParser
	Handle(
		IRawRequest,
		types.InternalRequest,
	) (types.InternalResponse, error)
}

type ParamHandlerMaker = func(
	path types.PathPttrn, params any,
) (IRoutable, error)

type IErrToApiProcessor interface {
	Wrap(error) (int, []byte)
}

type IHandlerContainer interface {
	GetHandler(method, path string) (IWrappedHandler, map[string]string)
	GetRoot() IRoutable
}

type IReqParser interface {
	Parse(
		req IRawRequest,
		path_vars map[string]string,
		context types.Context,
	) (any, error)
	HParams() []types.HParam
	QParams() []types.QParam
	FParams() []types.FParam
	BodyType() *reflect.Type // may be nil
}

type ResSerializer = func(
	obj any,
	headers IHeaderRequest,
) (types.InternalResponse, error)

type IIntoResponse interface {
	ResponseType() reflect.Type
	IntoResponse() types.Response
}

type IQueryParam interface {
	ParseQuery(string) (any, error) // return self
	GenQuerySample() string
}

type IFormParam interface {
	ParseForm(string) (any, error) // return self
	GenFormSample() string
}

type IHeaderParam interface {
	ParseHeader(string) (any, error) // return self
	GenHeaderSample() string
}

type IMiddlewareReqArg interface {
	IHeaderRequest
	Path() string
	Method() string
	Context() map[string]any
}

type IMiddleware interface {
	OnReq(IMiddlewareReqArg) error
	OnResp(IMiddlewareRespArg) error
	OnErr(IMiddlewareErrArg) error
}

type IMiddlewareRespArg interface {
	IMiddlewareReqArg
	GetStatus() int
	SetStatus(int)
	GetBody() []byte
}

type IMiddlewareErrArg interface {
	IMiddlewareReqArg
	GetErr() error
}

type IDispatcher interface {
	Dispatch(IRawRequest) types.InternalResponse
}

type IEngine interface {
	Spawn(
		addr string,
		disp IDispatcher,
		tls_cert string,
		tls_key string,
	) error
}
