package names

const QueryTagSrv = "srv"
const QueryTagName = "name"
const QueryTagDefault = "default"

const HeaderTagName = "name"

const SrvReqModeInclude = "include"
const SrvReqModeHeader = "header"
const SrvReqModeQuery = "query"
const SrvReqModeBody = "body"
const SrvReqModeForm = "form"
const SrvReqModeCtx = "context"
