package form_params_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/consts"
	"gitlab.com/asvedr/srv/internal/form_params"
	"gitlab.com/asvedr/srv/internal/types"
)

func TestMaker(t *testing.T) {
	params := map[string]any{
		"x": 1,
		"y": 2,
	}
	path, err := types.PathPttrn{}.Parse("/path/")
	assert.NoError(t, err)
	hndlr, err := form_params.New(path, params)
	assert.Nil(t, err)

	assert.Nil(t, hndlr.FormParams())
	assert.Equal(t, consts.MethodParams, hndlr.Method())
	assert.Equal(t, path.Src, hndlr.Path())
	data, err := hndlr.Handle(types.None{})
	assert.Nil(t, err)
	assert.Equal(t, `{"x":1,"y":2}`, string(data))
}

func TestMakerError(t *testing.T) {
	params := map[string]any{
		"x": make(chan int),
		"y": 2,
	}
	path, err := types.PathPttrn{}.Parse("/path/")
	assert.NoError(t, err)
	_, err = form_params.New(path, params)
	msg := `Can not create form params for path/: json: unsupported type: chan int`
	assert.Equal(t, msg, err.Error())
}
