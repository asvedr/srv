package form_params

import (
	"encoding/json"

	"gitlab.com/asvedr/srv/internal/base"
	"gitlab.com/asvedr/srv/internal/consts"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type Handler struct {
	base.BaseHandler
	path   string
	params []byte
}

var _ proto.IApiHandler[types.None, []byte] = Handler{}

func New(
	path types.PathPttrn,
	params any,
) (Handler, error) {
	resp, err := json.Marshal(params)
	if err != nil {
		return Handler{}, errs.ErrCanNotCreateFormParams{
			Handler: path.Src,
			Cause:   err,
		}
	}
	return Handler{path: path.Src, params: resp}, nil
}

func (self Handler) FormParams() any {
	return nil
}

func (self Handler) Method() string {
	return consts.MethodParams
}

func (self Handler) Path() string {
	return self.path
}

func (self Handler) Handle(types.None) ([]byte, error) {
	return self.params, nil
}
