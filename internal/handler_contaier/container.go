package handler_contaier

import (
	"strings"

	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type Container struct {
	root proto.IRoutable
}

var _ proto.IHandlerContainer = Container{}

func New(
	param_maker func(types.PathPttrn, any) (proto.IRoutable, error),
	router_factory func(string, []proto.IRoutable) proto.IRouter,
	routable []proto.IRoutable,
) (Container, error) {
	self := Container{root: router_factory("", routable)}
	if param_maker == nil {
		return self, nil
	}
	_, err := add_params(param_maker, self.root)
	if err != nil {
		return self, err
	}
	return self, nil
}

func (self Container) GetRoot() proto.IRoutable {
	return self.root
}

func (self Container) GetHandler(method, path string) (proto.IWrappedHandler, map[string]string) {
	split := split_path(path)
	vars := map[string]string{}
	handler := find_handler(self.root, vars, method, split)
	return handler, vars
}

func split_path(src string) []string {
	res := []string{}
	for _, token := range strings.Split(src, "/") {
		token = strings.TrimSpace(token)
		if len(token) > 0 {
			res = append(res, token)
		}
	}
	return res
}

func find_handler(
	router proto.IRoutable,
	vars map[string]string,
	method string,
	path []string,
) proto.IWrappedHandler {
	for _, child := range router.Children() {
		pattern := child.Path()
		matched, matched_path, rest_path := match_path(path, pattern)
		if !matched {
			continue
		}
		var handler proto.IWrappedHandler
		if child.IsRouter() {
			handler = find_handler(child, vars, method, rest_path)
		} else {
			if method == child.Method() && len(rest_path) == 0 {
				handler = child.DowncastHandler()
			}
		}
		if handler != nil {
			add_vars(vars, pattern, matched_path)
			return handler
		}
	}
	return nil
}

func match_path(
	path []string,
	pattern types.PathPttrn,
) (bool, []string, []string) {
	for i, tp := range pattern.ItemTypes {
		var ok bool
		switch tp {
		case types.PathItemCommon:
			ok = check_common_item(i, pattern, path)
		case types.PathItemVar:
			ok = i < len(path)
		case types.PathItemRest:
			ok = true
		default:
			panic("unknown path item type")
		}
		if !ok {
			return false, nil, nil
		}
	}
	if pattern.EndsWithRest() {
		return true, path, nil
	} else {
		cnt := len(pattern.ItemTypes)
		return true, path[:cnt], path[cnt:]
	}
}

func check_common_item(i int, ptt types.PathPttrn, path []string) bool {
	if i >= len(path) {
		return false
	}
	return path[i] == ptt.Items[i]
}

func add_params(
	param_maker proto.ParamHandlerMaker,
	routable proto.IRoutable,
) (proto.IRoutable, error) {
	if !routable.IsRouter() {
		handler := routable.DowncastHandler()
		params := handler.FormParams()
		if params == nil {
			return nil, nil
		}
		return param_maker(handler.Path(), params)
	}
	to_add := []proto.IRoutable{}
	for _, child := range routable.Children() {
		handler, err := add_params(param_maker, child)
		if err != nil {
			return nil, err
		}
		if handler != nil {
			to_add = append(to_add, handler)
		}
	}
	routable.DowncastRouter().AddChildren(to_add)
	return nil, nil
}

func add_vars(
	vars map[string]string,
	pattern types.PathPttrn,
	matched_path []string,
) {
	for i, tp := range pattern.ItemTypes {
		if tp == types.PathItemCommon {
			continue
		}
		key := pattern.Items[i]
		if tp == types.PathItemVar {
			vars[key] = matched_path[i]
		} else {
			vars[key] = strings.Join(matched_path[i:], "/")
		}
	}
}
