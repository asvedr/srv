package consts

const MethodParams string = "PARAMS"
const PathVarPrefix string = ":"
const PathRestPrefix string = "::"

const MethodGet = "GET"
const MethodPost = "POST"
const MethodPut = "PUT"
const MethodPatch = "PATCH"
const MethodDelete = "DELETE"

const InternalErrorStatus = 500
const InternalErrorCode = "INTERNAL"
