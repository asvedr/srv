package dispatcher

import (
	"slices"

	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type MReqArg struct {
	req    proto.IRawRequest
	path   string
	method string
	// headers map[string]string
	context map[string]any
}

var _ proto.IMiddlewareReqArg = (*MReqArg)(nil)

type MRespArg struct {
	MReqArg
	resp types.InternalResponse
}

var _ proto.IMiddlewareRespArg = (*MRespArg)(nil)

type MErrArg struct {
	MReqArg
	err error
}

var _ proto.IMiddlewareErrArg = (*MErrArg)(nil)

func (self *MReqArg) Path() string {
	return self.path
}

func (self *MReqArg) Method() string {
	return self.method
}

func (self *MReqArg) HeaderGet(key string) string {
	return self.req.HeaderGet(key)
}

func (self *MReqArg) HeaderSet(key, val string) {
	self.req.HeaderSet(key, val)
}

func (self *MReqArg) HeaderGetMany(key string) []string {
	return self.req.HeaderGetMany(key)
}

func (self *MReqArg) Context() map[string]any {
	return self.context
}

func (self *MRespArg) GetStatus() int {
	return self.resp.Status
}

func (self *MRespArg) SetStatus(status int) {
	self.resp.Status = status
}

func (self *MRespArg) GetBody() []byte {
	return self.resp.Data
}

func (self *MErrArg) GetErr() error {
	return self.err
}

type SeparatedMiddleware struct {
	OnReq  []func(proto.IMiddlewareReqArg) error
	OnResp []func(proto.IMiddlewareRespArg) error
	OnErr  []func(proto.IMiddlewareErrArg) error
}

func SeparateMiddleware(src []proto.IMiddleware) SeparatedMiddleware {
	var req_m []func(proto.IMiddlewareReqArg) error
	var resp_m []func(proto.IMiddlewareRespArg) error
	var err_m []func(proto.IMiddlewareErrArg) error

	for _, m := range src {
		req_m = append(req_m, m.OnReq)
		resp_m = append(resp_m, m.OnResp)
		err_m = append(err_m, m.OnErr)
	}
	if resp_m != nil {
		slices.Reverse(resp_m)
	}
	if err_m != nil {
		slices.Reverse(err_m)
	}
	return SeparatedMiddleware{
		OnReq:  req_m,
		OnResp: resp_m,
		OnErr:  err_m,
	}
}

func (self SeparatedMiddleware) apply_req(
	req proto.IRawRequest,
	context types.Context,
	method string,
	path string,
) error {
	if self.OnReq == nil {
		return nil
	}
	arg := &MReqArg{
		req:     req,
		path:    path,
		method:  method,
		context: context,
	}
	for _, m := range self.OnReq {
		err := m(arg)
		if err != nil {
			return err
		}
	}
	return nil
}

func (self SeparatedMiddleware) apply_resp(
	req proto.IRawRequest,
	context types.Context,
	method string,
	path string,
	resp types.InternalResponse,
) (types.InternalResponse, error) {
	if self.OnResp == nil {
		return resp, nil
	}
	req_arg := MReqArg{
		req:     req,
		path:    path,
		method:  method,
		context: context,
	}
	arg := &MRespArg{MReqArg: req_arg, resp: resp}
	for _, m := range self.OnResp {
		err := m(arg)
		if err != nil {
			return resp, err
		}
	}
	return arg.resp, nil
}

func (self SeparatedMiddleware) apply_err(
	req proto.IRawRequest,
	context types.Context,
	method string,
	path string,
	err error,
) error {
	if self.OnErr == nil {
		return err
	}
	req_arg := MReqArg{
		req:     req,
		path:    path,
		method:  method,
		context: context,
	}
	arg := &MErrArg{MReqArg: req_arg, err: err}
	for _, m := range self.OnErr {
		arg.err = m(arg)
	}
	return arg.err
}
