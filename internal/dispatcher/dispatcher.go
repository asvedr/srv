package dispatcher

import (
	"log"
	"net/textproto"
	"strconv"

	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type Dispatcher[
	HC proto.IHandlerContainer,
	E proto.IErrToApiProcessor,
] struct {
	handlers       HC
	err_to_api     E
	middlewares    SeparatedMiddleware
	DefaultHeaders map[string]string
}

var _ proto.IDispatcher = (*Dispatcher[
	proto.IHandlerContainer,
	proto.IErrToApiProcessor,
])(nil)

func New[
	HC proto.IHandlerContainer,
	E proto.IErrToApiProcessor,
](
	handlers HC,
	err_to_api E,
	def_headers map[string]string,
	middlewares []proto.IMiddleware,
) *Dispatcher[HC, E] {
	if def_headers == nil {
		def_headers = map[string]string{}
	}
	norm_headers := map[string]string{}
	for k, v := range def_headers {
		norm_headers[textproto.CanonicalMIMEHeaderKey(k)] = v
	}
	return &Dispatcher[HC, E]{
		handlers:       handlers,
		err_to_api:     err_to_api,
		DefaultHeaders: norm_headers,
		middlewares:    SeparateMiddleware(middlewares),
	}
}

func (self *Dispatcher[HC, E]) Dispatch(req proto.IRawRequest) types.InternalResponse {
	var int_resp types.InternalResponse
	var err error
	context := types.Context(map[string]any{})
	method := req.Method()
	path := req.Path()
	int_resp, err = self.handle(req, method, path, context)
	if err != nil {
		new_err := self.middlewares.apply_err(
			req,
			context,
			method,
			path,
			err,
		)
		if new_err != nil {
			err = new_err
		}
		int_resp.Status, int_resp.Data = self.err_to_api.Wrap(err)
	}
	data_len := strconv.Itoa(len(int_resp.Data))
	req.HeaderSet("Content-Length", data_len)
	if int_resp.Status/100 == 5 {
		log.Printf(
			"%s %s ? %s => %d: %v",
			method,
			path,
			req.QueryRaw(),
			int_resp.Status,
			err,
		)
	}
	return int_resp
}

func (self *Dispatcher[HC, E]) handle(
	req proto.IRawRequest,
	method, path string,
	context types.Context,
) (types.InternalResponse, error) {
	handler, path_vars := self.handlers.GetHandler(method, path)
	if handler == nil {
		return types.InternalResponse{}, errs.ErrNotFound{}
	}
	for key, val := range self.DefaultHeaders {
		if req.HeaderGet(key) == "" {
			req.HeaderSet(key, val)
		}
	}
	err := self.middlewares.apply_req(req, context, method, path)
	if err != nil {
		return types.InternalResponse{}, err
	}
	parser := handler.RequestParser()
	parsed_req, err := parser.Parse(req, path_vars, context)
	if err != nil {
		return types.InternalResponse{}, err
	}
	internal_req := types.InternalRequest{
		Req:     parsed_req,
		Context: context,
	}
	resp, err := handler.Handle(req, internal_req)
	if err != nil {
		return types.InternalResponse{}, err
	}
	resp, err = self.middlewares.apply_resp(
		req, context, method, path, resp,
	)
	if err != nil {
		return types.InternalResponse{}, err
	}
	return resp, nil
}
