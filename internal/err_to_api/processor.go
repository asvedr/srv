package err_to_api

import (
	"encoding/json"

	"gitlab.com/asvedr/srv/internal/consts"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/proto"
)

type TProc struct{}

var _ proto.IErrToApiProcessor = TProc{}
var Proc = TProc{}

type api_err struct {
	Code   string `json:"code"`
	Detail string `json:"detail"`
}

func (TProc) Wrap(err error) (int, []byte) {
	to_api_err, casted := err.(errs.IToApiError)
	if !casted {
		return internal_error(err)
	}
	status, code := to_api_err.ApiError()
	return status, marshal(code, err)
}

func internal_error(err error) (int, []byte) {
	status := consts.InternalErrorStatus
	return status, marshal(consts.InternalErrorCode, err)
}

func marshal(code string, err error) []byte {
	bts, _ := json.Marshal(api_err{Code: code, Detail: err.Error()})
	return bts
}
