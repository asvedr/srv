package err_to_api_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/err_to_api"
)

type NotFound struct{}

func (NotFound) Error() string {
	return "not found"
}

func (NotFound) ApiError() (int, string) {
	return 404, "NOT_FOUND"
}

func TestWrapNotFound(t *testing.T) {
	code, data := err_to_api.Proc.Wrap(NotFound{})
	assert.Equal(t, 404, code)
	msg_a := `{"code":"NOT_FOUND","detail":"not found"}`
	msg_b := `{"detail":"not found","code":"NOT_FOUND"}`
	sdata := string(data)
	assert.True(t, sdata == msg_a || sdata == msg_b)
}

func TestWrapInternal(t *testing.T) {
	err := errors.New("oops")
	code, data := err_to_api.Proc.Wrap(err)
	assert.Equal(t, 500, code)
	msg_a := `{"code":"INTERNAL","detail":"oops"}`
	msg_b := `{"detail":"oops","code":"INTERNAL"}`
	sdata := string(data)
	assert.True(t, sdata == msg_a || sdata == msg_b)
}
