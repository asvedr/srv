package types

type HeaderDiffType int

const (
	HeaderDiffTypeSet HeaderDiffType = iota
	HeaderDiffTypeDel
)

type HeaderDiff struct {
	Key  string
	Type HeaderDiffType
	Val  string
}

type Response struct {
	Data      any
	Status    int
	HeaderOps []HeaderDiff
}

type InternalResponse struct {
	Data   []byte
	Status int
}

// func (self Response) PrepareHeaders(headers map[string]string) map[string]string {
// 	for _, diff := range self.HeaderOps {
// 		if diff.Type == HeaderDiffTypeAdd {
// 			headers[diff.Key] = diff.Val
// 		} else if diff.Key == "" {
// 			headers = map[string]string{}
// 		} else {
// 			delete(headers, diff.Key)
// 		}
// 	}
// 	return headers
// }
