package types

import (
	"io"
	"mime/multipart"
	"net/textproto"
	"reflect"
	"strings"

	"gitlab.com/asvedr/srv/internal/consts"
	"gitlab.com/asvedr/srv/internal/errs"
)

type PathItemTp int

const (
	PathItemCommon PathItemTp = iota
	PathItemVar
	PathItemRest
)

type PathPttrn struct {
	Src       string
	Items     []string
	ItemTypes []PathItemTp
}

type HParam struct {
	Key  string
	Type reflect.Type
}

type QParam struct {
	Key      string
	Type     reflect.Type
	Default  []string
	Optional bool
	List     bool
}

type FParam struct {
	Key      string
	Type     reflect.Type
	Default  *string
	Optional bool
	IsFile   bool
}

type FormFile struct {
	Header  *multipart.FileHeader
	content []byte
}

func (self FormFile) Mock(
	filename string,
	Header textproto.MIMEHeader,
	content []byte,
) FormFile {
	return FormFile{
		Header: &multipart.FileHeader{
			Filename: filename,
			Header:   Header,
			Size:     int64(len(content)),
		},
		content: content,
	}
}

func (self FormFile) ReadAll() ([]byte, error) {
	if self.content != nil {
		return self.content, nil
	}
	file, err := self.Header.Open()
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return io.ReadAll(file)
}

func (self PathPttrn) Parse(src string) (PathPttrn, error) {
	items := self.split(src)
	tps := make([]PathItemTp, len(items))
	for i, item := range items {
		if strings.HasPrefix(item, consts.PathRestPrefix) {
			tps[i] = PathItemRest
			items[i] = strings.TrimPrefix(items[i], consts.PathRestPrefix)
			if i != len(items)-1 {
				err := errs.ErrRestMustBeLast{Path: src}
				return PathPttrn{}, err
			}
		} else if strings.HasPrefix(item, consts.PathVarPrefix) {
			tps[i] = PathItemVar
			items[i] = strings.TrimPrefix(items[i], consts.PathVarPrefix)
		} else {
			tps[i] = PathItemCommon
		}
	}
	// normalized := strings.Join(items, "/")
	normalized := self.normalize(items, tps)
	if !strings.HasSuffix(normalized, "/") {
		normalized += "/"
	}
	res := PathPttrn{
		Src:       normalized,
		Items:     items,
		ItemTypes: tps,
	}
	return res, nil
}

func (PathPttrn) normalize(items []string, tps []PathItemTp) string {
	var to_join []string
	for i, tp := range tps {
		var item string
		if tp == PathItemVar {
			item = consts.PathVarPrefix + items[i]
		} else if tp == PathItemRest {
			item = consts.PathRestPrefix + items[i]
		} else {
			item = items[i]
		}
		if item != "" {
			to_join = append(to_join, item)
		}
	}
	return strings.Join(to_join, "/")
}

func (PathPttrn) split(src string) []string {
	res := []string{}
	for _, item := range strings.Split(src, "/") {
		item = strings.TrimSpace(item)
		if len(item) > 0 {
			res = append(res, item)
		}
	}
	return res
}

func (self PathPttrn) EndsWithRest() bool {
	if len(self.ItemTypes) == 0 {
		return false
	}
	last := self.ItemTypes[len(self.ItemTypes)-1]
	return last == PathItemRest
}

type InternalRequest struct {
	Req     any
	Context Context
}
