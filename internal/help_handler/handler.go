package help_handler

import (
	_ "embed"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/asvedr/collections/cons"
	"gitlab.com/asvedr/srv/internal/base"
	"gitlab.com/asvedr/srv/internal/consts"
	"gitlab.com/asvedr/srv/internal/docgen/jsgen"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/handler_contaier"
	"gitlab.com/asvedr/srv/internal/html"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/router"
	"gitlab.com/asvedr/srv/internal/types"
)

//go:embed style.css
var StyleCss []byte

type handler struct {
	base.BaseGetHandler
	path         string
	handler_list []child
}

type child struct {
	full_path string
	path_vars []string
	handler   proto.IWrappedHandler
}

type Query struct {
	Method string `srv:"query" name:"method" default:"GET"`
	Path   string `srv:"query" name:"path" default:""`
}

type Response struct {
	Data string
}

var _ proto.IIntoResponse = Response{}

func (self Response) ResponseType() reflect.Type {
	return reflect.TypeFor[string]()
}

func (self Response) IntoResponse() types.Response {
	return types.Response{
		Data:   self.Data,
		Status: 200,
		HeaderOps: []types.HeaderDiff{{
			Key:  "Content-Type",
			Type: types.HeaderDiffTypeSet,
			Val:  "text/html; charset=utf-8",
		}},
	}
}

func New(
	path string, raw []proto.IRoutable,
) (proto.IApiHandler[Query, Response], error) {
	handlers, err := handler_contaier.New(
		nil,
		router.New,
		raw,
	)
	if err != nil {
		return nil, err
	}
	as_list := make_children(handlers.GetRoot())
	self := &handler{path: path, handler_list: as_list}
	return self, nil
}

func (self *handler) Path() string {
	return self.path
}

func (self *handler) Description() string {
	return "get description for path"
}

func (self *handler) Handle(query Query) (Response, error) {
	if query.Path == "" {
		data := self.prepare_list()
		return Response{Data: data}, nil
	}
	handler, ok := self.get_handler(query.Method, query.Path)
	if !ok {
		return Response{}, errs.ErrNotFound{}
	}
	lines := []string{
		"<html>",
		"<head>",
		"<style>",
		string(StyleCss),
		"</style>",
		"</head>",
		"<body>",
		handler.handler.Description(query.Path),
	}
	lines = append(lines, self.prepare_params_help(handler)...)
	lines = append(lines, "</body>", "</html>")
	data := strings.Join(lines, "\n")
	return Response{Data: data}, nil
}

func (self *handler) get_handler(method, path string) (child, bool) {
	for _, child := range self.handler_list {
		if child.full_path == path && child.handler.Method() == method {
			return child, true
		}
	}
	return child{}, false
}

func (self *handler) prepare_list() string {
	lines := []string{
		"<html>",
		"<head>",
		"<style>",
		string(StyleCss),
		"</style>",
		"</head>",
		"<body>",
		`<div class="header">Available methods:</div>`,
		`<div class="link_list">`,
	}
	for _, handler := range self.handler_list {
		lines = append(lines, self.wrap_url(handler))
	}
	lines = append(lines, "</div>", "</body>", "</html>")
	return strings.Join(lines, "\n")
}

func (self *handler) prepare_params_help(handler child) []string {
	params := handler.handler.FormParams()
	if params == nil {
		return []string{}
	}
	doc, err := jsgen.DumpDyn(params)
	if err != nil {
		msg := fmt.Sprintf(
			"ERROR: can not prepare help for params at %s: %v",
			handler.full_path,
			err,
		)
		return []string{msg}
	}
	return []string{
		html.Div("qbrd_label", "Form params:"),
		html.Div(
			"url",
			html.Div("method", consts.MethodParams),
			html.Div("path", handler.full_path),
		),
		html.Div("qbrd_label", "Constant response:"),
		doc,
	}
}

func (self *handler) wrap_url(handler child) string {
	href := fmt.Sprintf(
		"%s?method=%s&path=%s",
		self.path,
		handler.handler.Method(),
		handler.full_path,
	)
	mclass := "method_" + strings.ToLower(handler.handler.Method())
	children := []string{
		html.Div("method "+mclass, html.A(href, handler.handler.Method())),
		html.Div("path "+mclass, html.A(href, handler.full_path)),
	}
	if handler.handler.FormParams() != nil {
		a := html.A(href, "has form params")
		children = append(children, html.Div("label_params", a))
	}
	return html.Div("url", children...)
}

func make_children(router proto.IRoutable) []child {
	return make_children_rec("", cons.List[[]string]{}, router)
}

func make_children_rec(
	prefix string,
	vars cons.List[[]string],
	parent proto.IRoutable,
) []child {
	var local_vars []string
	ptt := parent.Path()
	for i, tp := range ptt.ItemTypes {
		if tp == types.PathItemVar {
			local_vars = append(local_vars, ptt.Items[i])
		} else if tp == types.PathItemRest {
			local_vars = append(local_vars, ptt.Items[i])
		}
	}
	prefix = prefix + "/" + ptt.Src
	vars = cons.Cons(local_vars, vars)
	if parent.IsRouter() {
		var result []child
		for _, child := range parent.Children() {
			result = append(
				result,
				make_children_rec(prefix, vars, child)...,
			)
		}
		return result
	} else {
		pp, _ := types.PathPttrn{}.Parse(prefix)
		return []child{{
			full_path: pp.Src,
			handler:   parent.DowncastHandler(),
			path_vars: unwrap_vars(vars),
		}}
	}
}

func unwrap_vars(src cons.List[[]string]) []string {
	var result []string
	for !src.IsNil() {
		chunk, _ := src.Head()
		src = src.Tail()
		result = append(result, chunk...)
	}
	return result
}
