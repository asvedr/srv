package rserializer

import (
	"encoding/json"
	"reflect"

	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

var str_type = reflect.TypeOf("")
var bts_type = reflect.TypeOf([]byte{})

func MakeStruct[T any]() proto.ResSerializer {
	var t T
	var as_any any = t
	tp := as_any.(proto.IIntoResponse).ResponseType()
	if is_null(tp) {
		return make_ser_struct(ser_default)
	}
	switch tp {
	case str_type:
		return make_ser_struct(ser_string)
	case bts_type:
		return make_ser_struct(ser_bytes)
	default:
		return make_ser_struct(json.Marshal)
	}
}

func MakeStd[T any]() proto.ResSerializer {
	var t T
	if is_null(reflect.TypeOf(t)) {
		return make_ser_std(ser_default)
	}
	var f func(any, proto.IHeaderRequest) (types.InternalResponse, error)
	switch any(t).(type) {
	case string:
		f = make_ser_std(ser_string)
	case []byte:
		f = make_ser_std(ser_bytes)
	default:
		f = make_ser_std(json.Marshal)
	}
	return f
}

func make_ser_struct(
	f func(any) ([]byte, error),
) func(any, proto.IHeaderRequest) (types.InternalResponse, error) {
	return func(
		src any,
		headers proto.IHeaderRequest,
	) (types.InternalResponse, error) {
		resp := src.(proto.IIntoResponse).IntoResponse()
		bts, err := f(resp.Data)
		if err != nil {
			return types.InternalResponse{}, err
		}
		prepare_headers(resp, headers)
		return types.InternalResponse{
			Data: bts, Status: resp.Status,
		}, nil
	}
}

func make_ser_std(
	f func(any) ([]byte, error),
) func(any, proto.IHeaderRequest) (types.InternalResponse, error) {
	return func(
		src any,
		headers proto.IHeaderRequest,
	) (types.InternalResponse, error) {
		bts, err := f(src)
		if err != nil {
			return types.InternalResponse{}, err
		}
		status := 200
		if len(bts) == 0 {
			status = 204
		}
		return types.InternalResponse{
			Data:   bts,
			Status: status,
		}, nil
	}
}

var default_resp = []byte("{}")

func ser_default(src any) ([]byte, error) {
	return default_resp, nil
}

func ser_string(src any) ([]byte, error) {
	return []byte(src.(string)), nil
}

func ser_bytes(src any) ([]byte, error) {
	if src == nil {
		return []byte{}, nil
	}
	return src.([]byte), nil
}

func is_null(tp reflect.Type) bool {
	if tp.Kind() != reflect.Struct {
		return false
	}
	return tp.NumField() == 0
}

func prepare_headers(
	resp types.Response,
	headers proto.IHeaderRequest,
) {
	for _, diff := range resp.HeaderOps {
		if diff.Type == types.HeaderDiffTypeSet {
			headers.HeaderSet(diff.Key, diff.Val)
		} else {
			headers.HeaderSet(diff.Key, "")
		}
	}
}
