package base

import "gitlab.com/asvedr/srv/internal/consts"

type BaseHandler struct{}
type BaseGetHandler struct{ BaseHandler }
type BasePostHandler struct{ BaseHandler }
type BasePutHandler struct{ BaseHandler }
type BasePatchHandler struct{ BaseHandler }
type BaseDeleteHandler struct{ BaseHandler }

func (self BaseHandler) Description() string {
	return ""
}

func (self BaseHandler) Errors() []error {
	return nil
}

func (self BaseHandler) FormParams() any {
	return nil
}

func (self BaseGetHandler) Method() string {
	return consts.MethodGet
}

func (self BasePostHandler) Method() string {
	return consts.MethodPost
}

func (self BasePutHandler) Method() string {
	return consts.MethodPut
}

func (self BasePatchHandler) Method() string {
	return consts.MethodPatch
}

func (self BaseDeleteHandler) Method() string {
	return consts.MethodDelete
}
