package base

import "gitlab.com/asvedr/srv/internal/proto"

type BaseMiddleware struct {
}

func (BaseMiddleware) OnReq(proto.IMiddlewareReqArg) error {
	return nil
}

func (BaseMiddleware) OnResp(proto.IMiddlewareRespArg) error {
	return nil
}

func (BaseMiddleware) OnErr(proto.IMiddlewareErrArg) error {
	return nil
}
