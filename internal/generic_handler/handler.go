package generic_handler

import (
	"gitlab.com/asvedr/srv/internal/base"
	"gitlab.com/asvedr/srv/internal/proto"
)

type Handler[Req, Resp any] struct {
	base.BaseHandler
	method string
	path   string
	f      func(Req) (Resp, error)
}

var _ proto.IApiHandler[int, rune] = Handler[int, rune]{}

func New[Req, Resp any](
	method string,
	path string,
	f func(Req) (Resp, error),
) proto.IApiHandler[Req, Resp] {
	return Handler[Req, Resp]{
		method: method,
		path:   path,
		f:      f,
	}
}

func (self Handler[Req, Resp]) Method() string {
	return self.method
}

func (self Handler[Req, Resp]) Path() string {
	return self.path
}

func (self Handler[Req, Resp]) Handle(req Req) (Resp, error) {
	return self.f(req)
}
