package std_handlers

import (
	"embed"

	"gitlab.com/asvedr/srv/internal/base"
)

type FileHandler struct {
	base.BaseGetHandler
	path    string
	data    []byte
	headers map[string]string
}

func StaticBts(
	path string,
	data []byte,
	headers map[string]string,
) FileHandler {
	return FileHandler{path: path, data: data, headers: headers}
}

func StaticEmbedFile(
	path string,
	fs embed.FS,
	key string,
	headers map[string]string,
) FileHandler {
	data, err := fs.ReadFile(key)
	if err != nil {
		panic(err)
	}
	return FileHandler{path: path, data: data, headers: headers}
}

func (self FileHandler) Path() string {
	return self.path
}

func (self FileHandler) Handle(struct{}) (Response, error) {
	res := Response{data: self.data}
	res.AddHeaders(self.headers)
	return res, nil
}
