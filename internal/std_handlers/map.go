package std_handlers

import (
	"embed"
	"io/fs"
	"net/url"
	"sort"
	"strings"

	"gitlab.com/asvedr/srv/internal/base"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/types"
)

type MapHandler struct {
	base.BaseGetHandler
	path           string
	data           map[string][]byte
	headers_by_ext map[string]map[string]string
}

type RequestMap struct {
	Path string `srv:"query" name:"path"`
}

func StaticBtsMap(
	path string,
	data map[string][]byte,
	headers_by_ext map[string]map[string]string,
) MapHandler {
	respath, err := url.JoinPath(path, "::path")
	if err != nil {
		panic(err)
	}
	if headers_by_ext == nil {
		headers_by_ext = map[string]map[string]string{}
	}
	return MapHandler{
		path:           respath,
		data:           data,
		headers_by_ext: headers_by_ext,
	}
}

func StaticEmbedMap(
	path string,
	root embed.FS,
	headers_by_ext map[string]map[string]string,
) MapHandler {
	objs, _ := root.ReadDir(".")
	data := map[string][]byte{}
	var rec_down func(parent string, obj fs.DirEntry)
	rec_down = func(parent string, obj fs.DirEntry) {
		var path string
		if parent != "" {
			path = parent + "/" + obj.Name()
		} else {
			path = obj.Name()
		}
		if !obj.IsDir() {
			data[path], _ = root.ReadFile(path)
			return
		}
		children, _ := root.ReadDir(path)
		for _, child := range children {
			rec_down(path, child)
		}
	}
	for _, obj := range objs {
		rec_down("", obj)
	}
	return StaticBtsMap(path, data, headers_by_ext)
}

func (self MapHandler) FormParams() any {
	paths := []string{}
	for key := range self.data {
		paths = append(paths, key)
	}
	sort.Strings(paths)
	return map[string][]string{"path": paths}
}

func (self MapHandler) Path() string {
	return self.path
}

func (self MapHandler) Handle(req RequestMap) (Response, error) {
	var res Response
	res.data = self.data[req.Path]
	if res.data == nil {
		return res, errs.ErrNotFound{}
	}

	ext := GetExt(req.Path)
	res.AddHeaders(self.headers_by_ext[ext])
	return res, nil
}

func GetExt(name string) string {
	ptt, err := types.PathPttrn{}.Parse(name)
	if err != nil {
		return ""
	}
	index := len(ptt.Items) - 1
	if index < 0 {
		return ""
	}
	split := strings.Split(ptt.Items[index], ".")
	if len(split) == 1 {
		return ""
	}
	return split[len(split)-1]
}
