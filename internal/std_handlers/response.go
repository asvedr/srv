package std_handlers

import (
	"reflect"

	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type Response struct {
	data       []byte
	header_ops []types.HeaderDiff
}

var _ proto.IIntoResponse = Response{}

func (self *Response) AddHeaders(headers map[string]string) {
	if headers == nil {
		return
	}
	for key, val := range headers {
		hdiff := types.HeaderDiff{
			Key:  key,
			Type: types.HeaderDiffTypeSet,
			Val:  val,
		}
		self.header_ops = append(self.header_ops, hdiff)
	}
}

func (self Response) ResponseType() reflect.Type {
	return reflect.TypeOf([]byte{})
}

func (self Response) IntoResponse() types.Response {
	return types.Response{
		Data:      self.data,
		Status:    200,
		HeaderOps: self.header_ops,
	}
}
