package wrapper

import (
	"net/url"
	"reflect"
	"strings"

	"gitlab.com/asvedr/srv/internal/docgen"
	"gitlab.com/asvedr/srv/internal/docgen/jsgen"
	"gitlab.com/asvedr/srv/internal/html"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type WrappedHandler[
	Req, Res any,
	ReqParser proto.IReqParser,
	Handler proto.IApiHandler[Req, Res],
] struct {
	path_pattern types.PathPttrn
	req_parser   ReqParser
	response_ser proto.ResSerializer
	handler      Handler
	Join         func(string, ...string) (string, error)
}

func New[
	Req, Res any,
	ReqParser proto.IReqParser,
	Handler proto.IApiHandler[Req, Res],
](
	parser ReqParser,
	response_ser proto.ResSerializer,
	handler Handler,
) (proto.IWrappedHandler, error) {
	path_pattern, err := types.PathPttrn{}.Parse(handler.Path())
	if err != nil {
		return nil, err
	}
	return &WrappedHandler[Req, Res, ReqParser, Handler]{
		path_pattern: path_pattern,
		req_parser:   parser,
		response_ser: response_ser,
		handler:      handler,
		Join:         url.JoinPath,
	}, nil
}

func (self *WrappedHandler[Req, Resp, RP, H]) IsRouter() bool {
	return false
}

func (self *WrappedHandler[Req, Resp, RP, H]) Children() []proto.IRoutable {
	return nil
}

func (self *WrappedHandler[Req, Resp, RP, H]) DowncastHandler() proto.IWrappedHandler {
	return self
}

func (self *WrappedHandler[Req, Resp, RP, H]) DowncastRouter() proto.IRouter {
	return nil
}

func (self *WrappedHandler[Req, Resp, RP, H]) FormParams() any {
	return self.handler.FormParams()
}

func (self *WrappedHandler[Req, Resp, RP, H]) Method() string {
	return self.handler.Method()
}

func (self *WrappedHandler[Req, Resp, RP, H]) Path() types.PathPttrn {
	return self.path_pattern
}

func (self *WrappedHandler[Req, Resp, RP, H]) Description(full_path string) string {
	doc := html.Div(
		"url",
		html.Div("method", self.Method()),
		html.Div("path", full_path),
	)
	doc += "\n" + docgen.Gen(self.req_parser)
	resp_tp := get_resp_type[Resp]()
	if !is_null(resp_tp) {
		txt, err := jsgen.GenDocType(resp_tp)
		if err != nil {
			txt = "ERROR: " + err.Error()
		}
		doc += html.Div("qbrd_label", "Response:")
		doc += "\n" + txt + "\n"
	}
	descr := strings.TrimSpace(self.handler.Description())
	if descr != "" {
		doc += html.Div("qbrd_label", "Description:")
		doc += html.Div("description", descr)
	}
	errs := self.handler.Errors()
	if len(errs) > 0 {
		doc += "\n" + docgen.GenErrList(errs)
	}
	return doc
}

func (self *WrappedHandler[Req, Resp, RP, H]) RequestParser() proto.IReqParser {
	return self.req_parser
}

func (self *WrappedHandler[Req, Resp, RP, H]) Handle(
	raw proto.IRawRequest,
	req types.InternalRequest,
) (types.InternalResponse, error) {
	typed := req.Req.(Req)
	resp, err := self.handler.Handle(typed)
	if err != nil {
		return types.InternalResponse{}, err
	}
	// req_headers := req.ReqHeaders
	return self.response_ser(resp, raw)
}

func get_resp_type[T any]() reflect.Type {
	var obj T
	var as_any any = obj
	into_resp, casted := as_any.(proto.IIntoResponse)
	if casted {
		return into_resp.ResponseType()
	} else {
		return reflect.TypeOf(obj)
	}
}

func is_null(tp reflect.Type) bool {
	if tp.Kind() != reflect.Struct {
		return false
	}
	return tp.NumField() == 0
}
