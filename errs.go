package srv

import (
	"gitlab.com/asvedr/srv/internal/errs"
)

type tErr struct {
	EmptyHandlers           error
	ErrCanNotUseTypeInQuery error
	ErrRestMustBeLast       error
	NotFound                errs.IToApiError
	MethodNotAllowed        errs.IToApiError
	Internal                errs.IToApiError
	InvalidBody             errs.IToApiError
	InvalidQuery            errs.IToApiError
	InvalidForm             errs.IToApiError
	InvalidHeader           errs.IToApiError
}

var Err = &tErr{
	EmptyHandlers:           errs.ErrEmptyHandlers{},
	ErrCanNotUseTypeInQuery: errs.ErrCanNotUseTypeInQuery{},
	ErrRestMustBeLast:       errs.ErrRestMustBeLast{},
	NotFound:                errs.ErrNotFound{},
	MethodNotAllowed:        errs.ErrMethodNotAllowed{},
	Internal:                errs.ErrInternal{},
	InvalidBody:             errs.ErrInvalidBody{},
	InvalidQuery:            errs.ErrInvalidQuery{},
	InvalidForm:             errs.ErrInvalidForm{},
	InvalidHeader:           errs.ErrInvalidHeader{},
}

type generic_api_err struct {
	status  int
	code    string
	details string
}

func (self generic_api_err) Error() string {
	return self.details
}

func (self generic_api_err) ApiError() (int, string) {
	return self.status, self.code
}

func (*tErr) New(status int, code string, details string) IToApiError {
	return generic_api_err{status: status, code: code, details: details}
}
