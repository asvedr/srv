package srv

import "gitlab.com/asvedr/srv/internal/consts"

const MethodGet = consts.MethodGet
const MethodPost = consts.MethodPost
const MethodPut = consts.MethodPut
const MethodPatch = consts.MethodPatch
const MethodDelete = consts.MethodDelete
