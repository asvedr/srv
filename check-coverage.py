import subprocess as sp
import re
import sys

RE = re.compile

BLACKLIST = [RE(r".*/testing/")]
PROF_FILE = "c.out"
OUT_FILE = "coverage.html"
TARGET_COVERAGE = 95.0

def exec_out(cmd: str) -> str:
    return sp.check_output(cmd, shell=True).decode()


def exec_no_out(cmd: str) -> None:
    return sp.check_call(cmd, shell=True)


def filter_blacklist(src: list[str]) -> list[str]:
    def in_blacklist(pkg: str) -> bool:
        for re in BLACKLIST:
            if re.match(pkg):
                return True

    return [pkg for pkg in src if not in_blacklist(pkg)]


def extract_num(src: list[str]) -> list[tuple[str, float]]:
    for line in src:
        # if any(f in line for f in EXCLUDE_FILES):
        #     continue
        if '<option value="file' in line:
            name = line.split(">")[1].split(" ")[0]
            num = line.split(" (")[1].split("%)")[0]
            yield (name, float(num))


def validate_coverage():
    with open(OUT_FILE) as handler:
        cov_list = dict(extract_num(handler))
    min_coverage = min(cov_list.values())
    avg_coverage = sum(cov_list.values()) / len(cov_list)
    print(f"avg coverage: {avg_coverage}")

    if min_coverage < TARGET_COVERAGE:
        for name, cov in cov_list.items():
            if cov < TARGET_COVERAGE:
                print(f"FAILED ON {name}: {cov} < {TARGET_COVERAGE}")
        sys.exit(1)
    print(f"OK min coverage: {min_coverage} >= {TARGET_COVERAGE}")


def main():
    pkgs_raw = exec_out("go list ./...").strip().split("\n")
    pkgs_raw = filter_blacklist(pkgs_raw)
    pkgs = ",".join(pkgs_raw)
    print(f"PKGS: {pkgs}")
    exec_no_out(
        f"go test -timeout 4s -cover -coverpkg {pkgs}"
        f" -coverprofile {PROF_FILE} ./..."
    )
    exec_no_out(
        f"go tool cover -html={PROF_FILE} -o {OUT_FILE}"
    )
    validate_coverage()

main()
