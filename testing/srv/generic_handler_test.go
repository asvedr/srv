package srv_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/types"
	"gitlab.com/asvedr/srv/testing/mocks"
)

func TestGenericHandler(t *testing.T) {
	handler := srv.GenericHandler(
		srv.MethodGet,
		"/a",
		func(req srv.Body[string]) (string, error) {
			return req.Body + "!", nil
		},
	)
	casted := handler.DowncastHandler()
	assert.Equal(t, srv.MethodGet, casted.Method())
	raw_req := &mocks.RawRequest{}
	int_req := types.InternalRequest{
		Req:     srv.Body[string]{Body: "abc"},
		Context: types.Context{},
	}
	res, err := casted.Handle(raw_req, int_req)
	assert.NoError(t, err)
	assert.Equal(t, "abc!", string(res.Data))
}
