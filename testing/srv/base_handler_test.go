package srv_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
)

type IBaseHandler interface {
	Description() string
	FormParams() any
	Method() string
}

var base_handlers = []struct {
	Handler IBaseHandler
	Method  string
}{
	{srv.BaseGetHandler{}, srv.MethodGet},
	{srv.BasePostHandler{}, srv.MethodPost},
	{srv.BasePutHandler{}, srv.MethodPut},
	{srv.BasePatchHandler{}, srv.MethodPatch},
	{srv.BaseDeleteHandler{}, srv.MethodDelete},
}

func TestBaseHandlers(t *testing.T) {
	for _, tc := range base_handlers {
		t.Run(tc.Method, func(t *testing.T) {
			assert.Equal(t, "", tc.Handler.Description())
			assert.Nil(t, tc.Handler.FormParams())
			assert.Equal(t, tc.Method, tc.Handler.Method())
		})
	}
}

func TestBaseMiddleware(t *testing.T) {
	m := srv.BaseMiddleware{}
	assert.NoError(t, m.OnReq(nil))
	assert.NoError(t, m.OnResp(nil))
	assert.NoError(t, m.OnErr(nil))
}
