package srv_test

import (
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/types"
	"gitlab.com/asvedr/srv/testing/mocks"
)

type param_handler struct {
	srv.BaseGetHandler
	name string
}

func (self param_handler) Path() string {
	return self.name
}

func (self param_handler) Handle(req srv.Body[string]) (string, error) {
	return self.name + ":" + req.Body, nil
}

func TestRoutable(t *testing.T) {
	router := srv.Router(
		"/api/",
		srv.MakeHandler(param_handler{name: "a"}),
		srv.MakeHandler(param_handler{name: "b"}),
	)
	handlers := router.Children()
	assert.Equal(t, 2, len(handlers))
	assert.Equal(t, "a/", handlers[0].Path().Src)
	assert.Equal(t, "b/", handlers[1].Path().Src)

	raw_req := &mocks.RawRequest{}
	int_req := types.InternalRequest{Req: srv.Body[string]{Body: "req"}}

	resp, err := handlers[0].DowncastHandler().Handle(raw_req, int_req)
	assert.NoError(t, err)
	assert.Equal(t, "a:req", string(resp.Data))

	resp, err = handlers[1].DowncastHandler().Handle(raw_req, int_req)
	assert.NoError(t, err)
	assert.Equal(t, "b:req", string(resp.Data))

	assert.True(
		t,
		strings.HasPrefix(
			handlers[0].DowncastHandler().Description("my/path/"),
			"<div class=",
		),
	)
}

func TestRunServeError(t *testing.T) {
	err := srv.Srv{
		Addr: ":8080",
		Handlers: []srv.IRoutable{
			srv.MakeHandler(param_handler{name: "a"}),
		},
		HelpPath: "/help/",
		Engine:   "fake",
	}.Run()
	assert.Equal(t, "the engine is faked", err.Error())
}

type InvalidHandler struct {
	srv.BaseGetHandler
}

type InvalidQuery struct {
	X chan int `srv:"query"`
}

func (self InvalidHandler) Path() string {
	return "a"
}

func (self InvalidHandler) Handle(req InvalidQuery) (string, error) {
	return "", nil
}

func TestPanicOnBadBuild(t *testing.T) {
	assert.Panics(t, func() {
		_ = srv.MakeHandler(InvalidHandler{})
	})
}

type FormParamHandler struct {
	srv.BaseGetHandler
}

func (self FormParamHandler) FormParams() any {
	return map[string]any{
		"x": make(chan int),
	}
}

func (self FormParamHandler) Path() string {
	return "a"
}

func (self FormParamHandler) Handle(srv.None) (string, error) {
	return "", nil
}

func TestFailOnBadFormParams(t *testing.T) {
	handler := srv.MakeHandler(FormParamHandler{})
	err := srv.Srv{
		Addr:     ":8080",
		HelpPath: "/help",
		Handlers: []srv.IRoutable{handler},
		Engine:   "fake",
	}.Run()
	msg := "Can not create form params for a/: json: unsupported type: chan int"
	assert.Equal(t, msg, err.Error())
}

func TestStartTlsBadConfig(t *testing.T) {
	err := srv.Srv{
		Addr:        ":8080",
		HelpPath:    "/help",
		TlsCertPath: "cert.pem",
		Engine:      "fake",
	}.Run()
	assert.ErrorIs(t, err, errs.TlsNotFull{})
}

func TestStartTlsNoFiles(t *testing.T) {
	err := srv.Srv{
		Addr:        ":8080",
		HelpPath:    "/help",
		TlsCertPath: "cert.pem",
		TlsKeyPath:  "key.pem",
		Middleware:  []srv.IMiddleware{srv.BaseMiddleware{}},
	}.Run()
	var target *fs.PathError
	assert.ErrorAs(t, err, &target)
}

type my_test_engine struct{}

func (my_test_engine) Spawn(
	addr string,
	dispatcher srv.IDispatcher,
	tls_cert string,
	tls_key string,
) error {
	return errors.New("new_engine")
}

func TestRegisterEngine(t *testing.T) {
	err := srv.Srv{
		Addr:     ":8080",
		HelpPath: "/help",
		Handlers: []srv.IRoutable{
			srv.MakeHandler(param_handler{name: "a"}),
		},
		Engine: "my_test_engine",
	}.Run()
	assert.Equal(t, "Unknown engine: my_test_engine", err.Error())

	srv.RegisterEngine("my_test_engine", my_test_engine{})

	err = srv.Srv{
		Addr:     ":8080",
		HelpPath: "/help",
		Handlers: []srv.IRoutable{
			srv.MakeHandler(param_handler{name: "a"}),
		},
		Engine: "my_test_engine",
	}.Run()
	assert.Equal(t, "new_engine", err.Error())
}

func TestSpawnStdServer(t *testing.T) {
	type MathReq struct {
		A int `srv:"query"`
		B int `srv:"query"`
	}

	server := srv.Srv{
		Addr:     ":8123",
		HelpPath: "/help",
		Handlers: []srv.IRoutable{
			srv.GenericHandler(
				srv.MethodGet,
				"/add",
				func(req MathReq) (string, error) {
					return fmt.Sprint(req.A + req.B), nil
				},
			),
		},
	}
	go server.Run()
	time.Sleep(time.Millisecond * 100)
	resp, err := http.Get("http://localhost:8123/add?a=1&b=2")
	assert.NoError(t, err)
	bts, err := io.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "3", string(bts))
}

func TestSetHeader(t *testing.T) {
	server := srv.Srv{
		Addr:     ":8124",
		HelpPath: "/help",
		Handlers: []srv.IRoutable{
			srv.GenericHandler(
				srv.MethodGet,
				"/add",
				func(srv.None) (srv.Response[srv.None], error) {
					var res srv.Response[srv.None]
					res.AddHeader("Custom", "1")
					return res, nil
				},
			),
		},
	}
	go server.Run()
	time.Sleep(time.Millisecond * 100)
	resp, err := http.Get("http://localhost:8124/add")
	assert.NoError(t, err)
	val := resp.Header.Get("Custom")
	assert.Equal(t, "1", val)
}
