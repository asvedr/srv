package srv_test

import (
	"embed"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/consts"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/std_handlers"
	"gitlab.com/asvedr/srv/internal/types"
	"gitlab.com/asvedr/srv/testing/mocks"
)

//go:embed data/*
var ThisDir embed.FS

func TestStaticBts(t *testing.T) {
	handler := srv.Static.Bts(
		"/bts/",
		[]byte{1, 2, 3},
		map[string]string{"a": "b"},
	).DowncastHandler()
	assert.Equal(t, consts.MethodGet, handler.Method())
	assert.Equal(t, "bts/", handler.Path().Src)
	raw_req := &mocks.RawRequest{}
	raw_req.On("HeaderSet", "a", "b")
	int_req := types.InternalRequest{Req: struct{}{}}
	resp, err := handler.Handle(raw_req, int_req)
	assert.NoError(t, err)
	assert.Equal(t, 200, resp.Status)
	assert.Equal(t, []byte{1, 2, 3}, resp.Data)
	raw_req.AssertExpectations(t)
}

func TestStaticEmbed(t *testing.T) {
	handler := srv.Static.EmbedFile(
		"/bts/",
		ThisDir,
		"data/subdir/file2.txt",
		map[string]string{"a": "b"},
	).DowncastHandler()
	assert.Equal(t, consts.MethodGet, handler.Method())
	assert.Equal(t, "bts/", handler.Path().Src)
	raw_req := &mocks.RawRequest{}
	raw_req.On("HeaderSet", "a", "b")
	int_req := types.InternalRequest{Req: struct{}{}}
	resp, err := handler.Handle(raw_req, int_req)
	assert.NoError(t, err)
	assert.Equal(t, 200, resp.Status)
	assert.Equal(t, []byte("def"), resp.Data)
	raw_req.AssertExpectations(t)
}

func TestStaticEmbedPanic(t *testing.T) {
	maker := func() {
		srv.Static.EmbedFile("/bts/", ThisDir, "abcdef", nil)
	}
	assert.Panics(t, maker)
}

func TestStaticBtsMap(t *testing.T) {
	cases := []struct {
		path    string
		data    string
		err     error
		headers map[string]string
	}{
		{"pic.png", "123", nil, map[string]string{"a": "p"}},
		{"pic.jpg", "456", nil, map[string]string{"a": "j"}},
		{"abc", "", errs.ErrNotFound{}, nil},
	}

	handler := srv.Static.BtsMap(
		"/map/",
		map[string][]byte{
			"pic.png": []byte("123"), "pic.jpg": []byte("456"),
		},
		map[string]map[string]string{
			"png": {"a": "p"}, "jpg": {"a": "j"},
		},
	).DowncastHandler()
	assert.Equal(t, consts.MethodGet, handler.Method())
	assert.Equal(t, "map/::path/", handler.Path().Src)
	assert.Equal(
		t,
		map[string][]string{"path": {"pic.jpg", "pic.png"}},
		handler.FormParams(),
	)

	test_ok := func(i int, t *testing.T) {
		raw_req := &mocks.RawRequest{}
		headers := cases[i].headers
		if headers == nil {
			headers = map[string]string{}
		}
		for k, v := range headers {
			raw_req.On("HeaderSet", k, v)
		}
		int_req := types.InternalRequest{
			Req: std_handlers.RequestMap{Path: cases[i].path},
		}
		resp, err := handler.Handle(raw_req, int_req)
		assert.NoError(t, err)
		assert.Equal(t, 200, resp.Status)
		assert.Equal(t, cases[i].data, string(resp.Data))
		raw_req.AssertExpectations(t)
	}
	test_err := func(i int, t *testing.T) {
		raw_req := &mocks.RawRequest{}
		int_req := types.InternalRequest{
			Req: std_handlers.RequestMap{Path: cases[i].path},
		}
		_, err := handler.Handle(raw_req, int_req)
		assert.ErrorIs(t, err, cases[i].err)
	}

	for i := range cases {
		label := fmt.Sprint(i)
		if cases[i].err == nil {
			t.Run(label, func(t *testing.T) { test_ok(i, t) })
		} else {
			t.Run(label, func(t *testing.T) { test_err(i, t) })
		}
	}
}

func TestStaticBtsMapNoHdr(t *testing.T) {
	handler := srv.Static.BtsMap(
		"/map/",
		map[string][]byte{
			"pic.png": []byte("123"), "pic.jpg": []byte("456"),
		},
		nil,
	).DowncastHandler()
	assert.Equal(t, consts.MethodGet, handler.Method())
	assert.Equal(t, "map/::path/", handler.Path().Src)
	assert.Equal(
		t,
		map[string][]string{"path": {"pic.jpg", "pic.png"}},
		handler.FormParams(),
	)
	raw_req := &mocks.RawRequest{}
	int_req := types.InternalRequest{
		Req: std_handlers.RequestMap{Path: "pic.png"},
	}
	resp, err := handler.Handle(raw_req, int_req)
	assert.NoError(t, err)
	assert.Equal(t, 200, resp.Status)
	assert.Equal(t, "123", string(resp.Data))
	raw_req.AssertExpectations(t)
}

func TestStaticEmbedMap(t *testing.T) {
	cases := []struct {
		path    string
		data    string
		err     error
		headers map[string]string
	}{
		{
			"data/file1.txt",
			"abc",
			nil,
			map[string]string{"a": "p"},
		},
		{
			"data/subdir/file3.t",
			"ghi",
			nil,
			map[string]string{"a": "j"},
		},
		{"abc", "", errs.ErrNotFound{}, nil},
	}

	handler := srv.Static.EmbedMap(
		"/map/",
		ThisDir,
		map[string]map[string]string{
			"txt": {"a": "p"}, "t": {"a": "j"},
		},
	).DowncastHandler()
	assert.Equal(t, consts.MethodGet, handler.Method())
	assert.Equal(t, "map/::path/", handler.Path().Src)
	assert.Equal(
		t,
		map[string][]string{
			"path": {
				"data/file1.txt",
				"data/subdir/file2.txt",
				"data/subdir/file3.t",
			},
		},
		handler.FormParams(),
	)

	test_ok := func(i int, t *testing.T) {
		headers := cases[i].headers
		if headers == nil {
			headers = map[string]string{}
		}
		raw_req := &mocks.RawRequest{}
		for k, v := range headers {
			raw_req.On("HeaderSet", k, v)
		}
		int_req := types.InternalRequest{
			Req: std_handlers.RequestMap{
				Path: cases[i].path,
			},
		}
		resp, err := handler.Handle(raw_req, int_req)
		assert.NoError(t, err)
		assert.Equal(t, 200, resp.Status)
		assert.Equal(t, cases[i].data, string(resp.Data))
		raw_req.AssertExpectations(t)
		// assert.Equal(t, cases[i].headers, resp.Headers)
	}
	test_err := func(i int, t *testing.T) {
		headers := cases[i].headers
		if headers == nil {
			headers = map[string]string{}
		}
		raw_req := &mocks.RawRequest{}
		for k, v := range headers {
			raw_req.On("HeaderSet", k, v)
		}
		int_req := types.InternalRequest{
			Req: std_handlers.RequestMap{Path: cases[i].path},
		}
		_, err := handler.Handle(raw_req, int_req)
		assert.ErrorIs(t, err, cases[i].err)
	}

	for i := range cases {
		label := fmt.Sprint(i)
		if cases[i].err == nil {
			t.Run(label, func(t *testing.T) { test_ok(i, t) })
		} else {
			t.Run(label, func(t *testing.T) { test_err(i, t) })
		}
	}
}

func TestGetExt(t *testing.T) {
	cases := map[string]string{
		"":                            "",
		"abc":                         "",
		"abc.txt":                     "txt",
		"abc.txt.j":                   "j",
		"dir/abc":                     "",
		"dir/abc.x":                   "x",
		"dir.x/abc":                   "",
		string([]byte{1, 2, 3, 0, 1}): "",
	}
	for name, ext := range cases {
		t.Run(fmt.Sprintf("%q", name), func(t *testing.T) {
			assert.Equal(
				t, ext, std_handlers.GetExt(name),
			)
		})
	}
}
