package help_handler_test

import (
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/base"
	"gitlab.com/asvedr/srv/internal/help_handler"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/router"
	"gitlab.com/asvedr/srv/internal/types"
)

type Response[T any] struct{}

var _ proto.IIntoResponse = Response[string]{}

func (self Response[T]) ResponseType() reflect.Type {
	var t T
	return reflect.TypeOf(t)
}

func (self Response[T]) IntoResponse() types.Response {
	return types.Response{}
}

type none struct{}

type handler_a struct{ base.BaseGetHandler }
type handler_b struct{ base.BasePostHandler }

func (handler_a) Path() string { return "/a" }
func (handler_a) Description() string {
	return "get a"
}
func (handler_a) Handle(none) (none, error) {
	return none{}, nil
}

func (handler_b) Path() string { return "/b/" }
func (handler_b) Description() string {
	return "post b"
}
func (handler_b) Handle(none) (none, error) {
	return none{}, nil
}

type handler_bad_params struct{ handler_a }
type handler_ok_params struct{ handler_a }
type handler_rest struct{ handler_a }
type handler_index struct{ handler_a }
type handler_resp[T any] struct{ handler_a }
type handler_req[T any] struct{ base.BasePostHandler }

func (handler_bad_params) FormParams() any {
	return map[string]any{"x": make(chan int)}
}

func (handler_ok_params) FormParams() any {
	return map[string]any{"x": []int{1, 2}}
}

func (handler_rest) Path() string {
	return "/a/::rst"
}

func (handler_index) Path() string {
	return "/"
}

func (handler_resp[T]) Handle(none) (Response[T], error) {
	return Response[T]{}, nil
}

func (handler_req[T]) Path() string {
	return "/a"
}

func (handler_req[T]) Handle(T) (none, error) {
	return none{}, nil
}

func TestBadFormParams(t *testing.T) {
	wrapped := srv.MakeHandler(handler_bad_params{})
	handlers := []proto.IRoutable{wrapped}
	hh, err := help_handler.New("/help", handlers)
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "a/",
		Method: "GET",
	}
	data, err := hh.Handle(req)
	assert.NoError(t, err)
	msg := `ERROR: can not prepare help for params at a/: can not gen doc for type map[string]interface {}: json: unsupported type: chan int`
	assert.True(t, strings.Contains(data.Data, msg))
}

func TestGoodFormParams(t *testing.T) {
	wrapped := srv.MakeHandler(handler_ok_params{})
	handlers := []proto.IRoutable{wrapped}
	hh, err := help_handler.New("/help", handlers)
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "a/",
		Method: "GET",
	}
	data, err := hh.Handle(req)
	assert.NoError(t, err)
	lines := []string{
		`<div class="value json"><pre>{`,
		` "x": [`,
		`  1,`,
		`  2`,
		` ]`,
		`}</pre></div>`,
	}
	msg := strings.Join(lines, "\n")
	assert.True(t, strings.Contains(data.Data, msg))
}

func TestEmptyFormParams(t *testing.T) {
	handler := srv.MakeHandler(handler_b{})
	handlers := []proto.IRoutable{handler}
	hh, err := help_handler.New("/help", handlers)
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "b/",
		Method: "POST",
	}
	data, err := hh.Handle(req)
	assert.NoError(t, err)
	mark := `class="value json"`
	assert.False(t, strings.Contains(data.Data, mark))
}

func TestNotFound(t *testing.T) {
	a := srv.MakeHandler(handler_a{})
	b := srv.MakeHandler(handler_b{})
	handler, err := help_handler.New("/help", []proto.IRoutable{a, b})
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "/x/",
		Method: "GET",
	}
	_, err = handler.Handle(req)
	assert.Equal(t, "Url not found", err.Error())
	req = help_handler.Query{
		Path:   "/a/",
		Method: "POST",
	}
	_, err = handler.Handle(req)
	assert.Equal(t, "Url not found", err.Error())
}

func TestStdMethods(t *testing.T) {
	a := srv.MakeHandler(handler_a{})
	handler, err := help_handler.New("/help", []proto.IRoutable{a})
	assert.NoError(t, err)
	assert.Nil(t, handler.FormParams())
	assert.Equal(t, "GET", handler.Method())
	// no panic
	_ = handler.Description()
}

var list_body = []string{
	`<body>`,
	`<div class="header">Available methods:</div>`,
	`<div class="link_list">`,
	`<div class="url"><div class="method method_get"><a href="/help?method=GET&path=a/">GET</a></div><div class="path method_get"><a href="/help?method=GET&path=a/">a/</a></div><div class="label_params"><a href="/help?method=GET&path=a/">has form params</a></div></div>`,
	`<div class="url"><div class="method method_post"><a href="/help?method=POST&path=b/">POST</a></div><div class="path method_post"><a href="/help?method=POST&path=b/">b/</a></div></div>`,
	`</div>`,
	`</body>`,
}

func TestList(t *testing.T) {
	a := srv.MakeHandler(handler_ok_params{})
	b := srv.MakeHandler(handler_b{})
	handler, err := help_handler.New("/help", []proto.IRoutable{a, b})
	assert.NoError(t, err)
	req := help_handler.Query{}
	data, err := handler.Handle(req)
	assert.NoError(t, err)
	msg := strings.Join(list_body, "\n")
	assert.True(t, strings.Contains(data.Data, msg))
}

func TestVarPath(t *testing.T) {
	wrapped := srv.MakeHandler(handler_rest{})
	router := router.New(
		"/api/:id/",
		[]proto.IRoutable{wrapped},
	)
	hh, err := help_handler.New("/help", []proto.IRoutable{router})
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "api/:id/a/::rst/",
		Method: "GET",
	}
	res, err := hh.Handle(req)
	assert.NoError(t, err)
	lines := []string{
		`<body>`,
		`<div class="url"><div class="method">GET</div><div class="path">api/:id/a/::rst/</div></div>`,
		`<div class="qbrd_label">Description:</div><div class="description">get a</div>`,
	}
	msg := strings.Join(lines, "\n")
	assert.True(t, strings.Contains(res.Data, msg))
}

func TestIndexPath(t *testing.T) {
	wrapped := srv.MakeHandler(handler_index{})
	hh, err := help_handler.New("/help", []proto.IRoutable{wrapped})
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "/",
		Method: "GET",
	}
	res, err := hh.Handle(req)
	assert.NoError(t, err)
	lines := []string{
		`<body>`,
		`<div class="url"><div class="method">GET</div><div class="path">/</div></div>`,
		`<div class="qbrd_label">Description:</div><div class="description">get a</div>`,
		`</body>`,
	}
	msg := strings.Join(lines, "\n")
	assert.True(t, strings.Contains(res.Data, msg))
}

func TestStdResponse(t *testing.T) {
	wrapped := srv.MakeHandler(handler_resp[none]{})
	hh, err := help_handler.New("/help", []proto.IRoutable{wrapped})
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "a/",
		Method: "GET",
	}
	resp, err := hh.Handle(req)
	assert.NoError(t, err)
	lines := []string{
		`<body>`,
		`<div class="url"><div class="method">GET</div><div class="path">a/</div></div>`,
		`<div class="qbrd_label">Description:</div><div class="description">get a</div>`,
		`</body>`,
	}
	joined := strings.Join(lines, "\n")
	assert.True(
		t, strings.Contains(resp.Data, joined),
	)

	wrapped = srv.MakeHandler(handler_resp[string]{})
	hh, err = help_handler.New("/help", []proto.IRoutable{wrapped})
	assert.NoError(t, err)
	req = help_handler.Query{
		Path:   "a/",
		Method: "GET",
	}
	resp, err = hh.Handle(req)
	assert.NoError(t, err)
	lines = []string{
		`<body>`,
		`<div class="url"><div class="method">GET</div><div class="path">a/</div></div>`,
		`<div class="qbrd_label">Response:</div>`,
		`<div class="value string">string</div>`,
		`<div class="qbrd_label">Description:</div><div class="description">get a</div>`,
		`</body>`,
	}
	joined = strings.Join(lines, "\n")
	assert.True(
		t, strings.Contains(resp.Data, joined),
	)
}

type form_req struct {
	File srv.FormFile `srv:"form"`
	Val  int          `srv:"form"`
}

type form_def_val struct {
	Val int `srv:"form" default:"1"`
}

type form_bad_val struct {
	Val chan int `srv:"form"`
}

type form_opt_file struct {
	File *srv.FormFile `srv:"form"`
}

func TestHelpFormFields(t *testing.T) {
	wrapped := srv.MakeHandler(handler_req[form_req]{})
	hh, err := help_handler.New("/help", []proto.IRoutable{wrapped})
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "a/",
		Method: "POST",
	}
	resp, err := hh.Handle(req)
	assert.NoError(t, err)

	lines := []string{
		`<div class="form_param"><div class="form_param_key">`,
		`file:</div><div class="value"><div class="value file">`,
		`FILE</div></div></div>`,
		"\n",
		`<div class="form_param"><div class="form_param_key">`,
		`val:</div><div class="value"><div class="value json">`,
		`<div class="value json">0</div></div></div>`,
		`</div>`,
	}
	exp := strings.Join(lines, "")
	assert.True(t, strings.Contains(resp.Data, exp))
}

func TestHelpFormDefVal(t *testing.T) {
	wrapped := srv.MakeHandler(handler_req[form_def_val]{})
	hh, err := help_handler.New("/help", []proto.IRoutable{wrapped})
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "a/",
		Method: "POST",
	}
	resp, err := hh.Handle(req)
	assert.NoError(t, err)

	lines := []string{
		`<div class="form_param"><div class="form_param_key">`,
		`val:</div><div class="value"><div class="value json">`,
		`<div class="value json">0</div></div></div>`,
		`<div class="form_param_default">default=1</div></div>`,
	}
	exp := strings.Join(lines, "")
	assert.True(t, strings.Contains(resp.Data, exp))
}

func TestHelpFormBadVal(t *testing.T) {
	wrapped := srv.MakeHandler(handler_req[form_bad_val]{})
	hh, err := help_handler.New("/help", []proto.IRoutable{wrapped})
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "a/",
		Method: "POST",
	}
	resp, err := hh.Handle(req)
	assert.NoError(t, err)

	exp := "Can not use type chan int in form"
	assert.True(t, strings.Contains(resp.Data, exp))
}

func TestHelpFormOptFile(t *testing.T) {
	wrapped := srv.MakeHandler(handler_req[form_opt_file]{})
	hh, err := help_handler.New("/help", []proto.IRoutable{wrapped})
	assert.NoError(t, err)
	req := help_handler.Query{
		Path:   "a/",
		Method: "POST",
	}
	resp, err := hh.Handle(req)
	assert.NoError(t, err)

	lines := []string{
		`<div class="form_param"><div class="form_param_key">`,
		`file:</div><div class="value"><div class="value file">`,
		`FILE</div></div><div class="form_param_optional">`,
		`optional</div></div>`,
	}
	exp := strings.Join(lines, "")
	assert.True(t, strings.Contains(resp.Data, exp))
}
