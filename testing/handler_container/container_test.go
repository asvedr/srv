package handler_contaier_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/form_params"
	"gitlab.com/asvedr/srv/internal/handler_contaier"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/router"
	"gitlab.com/asvedr/srv/internal/types"
	"gitlab.com/asvedr/srv/testing/mocks"
)

type post_a struct{ srv.BasePostHandler }
type post_a_b struct{ srv.BasePostHandler }
type get_a struct{ srv.BaseGetHandler }
type bad_form_params struct{ srv.BaseGetHandler }

type none struct{}

func (post_a) Path() string { return "/a" }
func (post_a) Description() string {
	return "post a"
}
func (post_a) Handle(none) (none, error) {
	return none{}, nil
}

func (post_a_b) Path() string { return "/a/b/" }
func (post_a_b) Description() string {
	return "post a b"
}
func (post_a_b) Handle(none) (none, error) {
	return none{}, nil
}

func (get_a) Path() string { return "/a" }
func (get_a) Description() string {
	return "get a"
}
func (get_a) Handle(none) (none, error) {
	return none{}, nil
}

func (bad_form_params) FormParams() any {
	return map[string]any{
		"x": make(chan int),
	}
}
func (bad_form_params) Path() string { return "/bfp" }
func (bad_form_params) Description() string {
	return "get a"
}
func (bad_form_params) Handle(none) (none, error) {
	return none{}, nil
}

type Setup struct {
	cnt handler_contaier.Container
	pa  proto.IWrappedHandler
	pab proto.IWrappedHandler
	ga  proto.IWrappedHandler
}

func fp_maker(
	path types.PathPttrn, param any,
) (srv.IRoutable, error) {
	h, err := form_params.New(path, param)
	var res srv.IRoutable
	if err == nil {
		res = srv.MakeHandler(h)
	}
	return res, err
}

func setup() Setup {
	pa := srv.MakeHandler(post_a{})
	pab := srv.MakeHandler(post_a_b{})
	ga := srv.MakeHandler(get_a{})
	handlers := []proto.IRoutable{pa, pab, ga}
	cnt, err := handler_contaier.New(
		fp_maker, router.New, handlers,
	)
	if err != nil {
		panic(err)
	}
	return Setup{
		cnt: cnt,
		pa:  pa.DowncastHandler(),
		pab: pab.DowncastHandler(),
		ga:  ga.DowncastHandler(),
	}
}

func TestGet(t *testing.T) {
	s := setup()
	h, _ := s.cnt.GetHandler("GET", "/a")
	assert.NotNil(t, h)
	assert.Equal(t, h.Description("x"), s.ga.Description("x"))
	h, _ = s.cnt.GetHandler("GET", "/a/")
	assert.Equal(t, h.Description("y"), s.ga.Description("y"))
	h, _ = s.cnt.GetHandler("GET", "/a/b/")
	assert.Nil(t, h)
	h, _ = s.cnt.GetHandler("GET", "/x")
	assert.Nil(t, h)
}

func TestPost(t *testing.T) {
	s := setup()
	h, _ := s.cnt.GetHandler("POST", "/a")
	assert.Equal(t, h.Description("x"), s.pa.Description("x"))
	h, _ = s.cnt.GetHandler("POST", "/a/")
	assert.Equal(t, h.Description("x"), s.pa.Description("x"))
	h, _ = s.cnt.GetHandler("POST", "/a/b/")
	assert.Equal(t, h.Description("x"), s.pab.Description("x"))
	h, _ = s.cnt.GetHandler("POST", "/x")
	assert.Nil(t, h)
}

func TestUnknownMethod(t *testing.T) {
	s := setup()
	h, _ := s.cnt.GetHandler("XXX", "/a")
	assert.Nil(t, h)
}

type handler_with_params struct{ srv.BaseGetHandler }

type handler_with_rest struct{ handler_with_params }

func (handler_with_params) FormParams() any {
	return map[string]any{
		"s": []string{"a", "b"},
		"i": []int{1, 2},
	}
}
func (handler_with_params) Path() string { return "/a" }
func (handler_with_params) Description() string {
	return "with params"
}
func (handler_with_params) Handle(none) (none, error) {
	return none{}, nil
}

func (handler_with_rest) Path() string {
	return "pref/:id/act/::path"
}

func TestFormParams(t *testing.T) {
	inner := srv.MakeHandler(handler_with_params{})
	handlers := []proto.IRoutable{inner}
	cnt, err := handler_contaier.New(
		fp_maker, router.New, handlers,
	)
	assert.NoError(t, err)
	handler, _ := cnt.GetHandler("GET", "/a")
	assert.Equal(t, "a/", handler.Path().Src)
	assert.Equal(t, "GET", handler.Method())
	handler, _ = cnt.GetHandler("PARAMS", "/a")
	assert.Equal(t, "a/", handler.Path().Src)
	assert.Equal(t, "PARAMS", handler.Method())
	raw := &mocks.RawRequest{}
	req := types.InternalRequest{Req: types.None{}}
	resp, err := handler.Handle(raw, req)
	assert.Nil(t, err)
	s_resp := string(resp.Data)
	variant_a := `{"s":["a","b"],"i":[1,2]}`
	variant_b := `{"i":[1,2],"s":["a","b"]}`
	assert.True(
		t,
		s_resp == variant_a || s_resp == variant_b,
	)
}

func TestBadFormParams(t *testing.T) {
	handler := srv.MakeHandler(bad_form_params{})
	handlers := []proto.IRoutable{handler}
	_, err := handler_contaier.New(
		fp_maker,
		router.New,
		handlers,
	)
	msg := "Can not create form params for bfp/: json: unsupported type: chan int"
	assert.Equal(t, msg, err.Error())
}

func TestMatchPathWithVarAndRest(t *testing.T) {
	cases := map[string]map[string]string{
		"pref/123/act/path": {"id": "123", "path": "path"},
		"pref/1/act/":       {"id": "1", "path": ""},
		"pref/x/act/a/b":    {"id": "x", "path": "a/b"},
	}

	f := func(
		t *testing.T,
		path string,
		exp_var map[string]string,
	) {
		wrapped := srv.MakeHandler(handler_with_rest{})
		cnt, err := handler_contaier.New(
			fp_maker,
			router.New,
			[]proto.IRoutable{wrapped},
		)
		assert.NoError(t, err)
		handler, vars := cnt.GetHandler("GET", path)
		assert.Equal(t, wrapped, handler)
		assert.Equal(t, exp_var, vars)
	}

	for key, val := range cases {
		t.Run(key, func(t *testing.T) {
			f(t, key, val)
		})
	}
}

func TestMatchPathWithVarAndRestErr(t *testing.T) {
	cases := []string{"pref/123/x/path", "pref/x/"}

	f := func(
		t *testing.T,
		path string,
	) {
		wrapped := srv.MakeHandler(handler_with_rest{})
		cnt, err := handler_contaier.New(
			fp_maker,
			router.New,
			[]proto.IRoutable{wrapped},
		)
		assert.NoError(t, err)
		handler, _ := cnt.GetHandler("GET", path)
		assert.Nil(t, handler)
	}

	for _, path := range cases {
		t.Run(path, func(t *testing.T) { f(t, path) })
	}
}

func TestVarFromRouter(t *testing.T) {
	wrapped := srv.MakeHandler(post_a{})
	cnt, err := handler_contaier.New(
		fp_maker,
		router.New,
		[]proto.IRoutable{
			router.New("api/:ver/", []proto.IRoutable{wrapped}),
		},
	)
	assert.NoError(t, err)
	handler, vars := cnt.GetHandler("POST", "/api/v1/a")
	assert.Equal(t, wrapped, handler)
	assert.Equal(t, map[string]string{"ver": "v1"}, vars)
}
