package reqparser_test

import (
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/reqparser"
	adapter "gitlab.com/asvedr/srv/internal/std_engine"
)

type query_xy struct {
	X int    `srv:"query" name:"x"`
	Y string `srv:"query" name:"y"`
}

func TestQueryAtom(t *testing.T) {
	parser, err := reqparser.New[query_xy]()
	assert.NoError(t, err)

	req, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=1&y=abc", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, query_xy{X: 1, Y: "abc"}, req)
}

func TestQueryNotSet(t *testing.T) {
	parser, err := reqparser.New[query_xy]()
	assert.NoError(t, err)

	_, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?y=abc", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	var exp errs.ErrInvalidQuery
	assert.ErrorAs(t, err, &exp)
	assert.Equal(t, exp.Param, "x")
}

func TestQueryCantParse(t *testing.T) {
	parser, err := reqparser.New[query_xy]()
	assert.NoError(t, err)

	_, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=abc&y=abc", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	var exp errs.ErrInvalidQuery
	assert.ErrorAs(t, err, &exp)
	assert.Equal(t, exp.Param, "x")
}

func TestQueryCantParseParent(t *testing.T) {
	type query struct {
		Parent query_xy `srv:"include"`
	}
	parser, err := reqparser.New[query]()
	assert.NoError(t, err)

	_, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=abc&y=abc", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	var exp errs.ErrInvalidQuery
	assert.ErrorAs(t, err, &exp)
	assert.Equal(t, exp.Param, "x")
}

func TestPathVarsSingle(t *testing.T) {
	parser, err := reqparser.New[query_xy]()
	assert.NoError(t, err)
	req, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path/", nil),
			httptest.NewRecorder(),
		),
		map[string]string{
			"x": "1",
			"y": "abc",
		},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, query_xy{X: 1, Y: "abc"}, req)
}

type query_list struct {
	X []int `srv:"query" name:"x"`
}

func TestPathVarsList(t *testing.T) {
	parser, err := reqparser.New[query_list]()
	assert.NoError(t, err)
	req, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path/", nil),
			httptest.NewRecorder(),
		),
		map[string]string{"x": "1"},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, query_list{X: []int{1}}, req)
}
