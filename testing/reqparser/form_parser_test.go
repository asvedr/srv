package reqparser_test

import (
	"bytes"
	"errors"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/asvedr/collections/opt"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/reqparser"
	adapter "gitlab.com/asvedr/srv/internal/std_engine"
	"gitlab.com/asvedr/srv/testing/mocks"
)

type RequestForm struct {
	File1 srv.FormFile  `srv:"form"`
	File2 *srv.FormFile `srv:"form" name:"abc"`
	X     int           `srv:"form"`
	Y     string        `srv:"form" name:"yy"`
}

type req_file struct {
	field, file_name string
	data             []byte
}

func create_form(
	files []req_file, values map[string]string,
) (*bytes.Buffer, string) {
	body := bytes.NewBuffer(nil)
	writer := multipart.NewWriter(body)
	for _, file := range files {
		file_writer, err := writer.CreateFormFile(
			file.field, file.file_name,
		)
		if err != nil {
			panic(err)
		}
		buf := bytes.NewBuffer(file.data)
		_, err = io.Copy(file_writer, buf)
		if err != nil {
			panic(err)
		}
	}
	for key, val := range values {
		err := writer.WriteField(key, val)
		if err != nil {
			panic(err)
		}
	}
	writer.Close()
	return body, writer.FormDataContentType()
}

func TestParseRequestForm(t *testing.T) {
	body, content_type := create_form(
		[]req_file{
			{
				field:     "file1",
				file_name: "file.txt",
				data:      []byte("text data"),
			},
			{
				field:     "abc",
				file_name: "file.png",
				data:      []byte{1, 2, 3, 4},
			},
		},
		map[string]string{"x": "1", "yy": "val"},
	)
	std_req := httptest.NewRequest(
		http.MethodPost, "/submit", body,
	)
	std_req.Header.Set("Content-Type", content_type)
	req := adapter.AdaptReq(std_req, httptest.NewRecorder())

	parser, err := reqparser.New[RequestForm]()
	assert.NoError(t, err)
	val, err := parser.Parse(req, map[string]string{}, nil)
	assert.NoError(t, err)
	casted := val.(RequestForm)
	assert.Equal(t, 1, casted.X)
	assert.Equal(t, "val", casted.Y)
	assert.Equal(t, "file.txt", casted.File1.Header.Filename)
	data, err := casted.File1.ReadAll()
	assert.NoError(t, err)
	assert.Equal(t, []byte("text data"), data)
	assert.Equal(t, "file.png", casted.File2.Header.Filename)
	data, err = casted.File2.ReadAll()
	assert.NoError(t, err)
	assert.Equal(t, []byte{1, 2, 3, 4}, data)
}

func TestParseMissedFile(t *testing.T) {
	body, content_type := create_form(
		[]req_file{
			// {
			// 	field:     "file1",
			// 	file_name: "file.txt",
			// 	data:      []byte("text data"),
			// },
			{
				field:     "abc",
				file_name: "file.png",
				data:      []byte{1, 2, 3, 4},
			},
		},
		map[string]string{"x": "1", "yy": "val"},
	)
	std_req := httptest.NewRequest(
		http.MethodPost, "/submit", body,
	)
	std_req.Header.Set("Content-Type", content_type)
	req := adapter.AdaptReq(std_req, httptest.NewRecorder())

	parser, err := reqparser.New[RequestForm]()
	assert.NoError(t, err)
	_, err = parser.Parse(req, map[string]string{}, nil)
	assert.Equal(
		t,
		"Invalid form field(file1): missing file",
		err.Error(),
	)
}

func TestParseMissedOptFile(t *testing.T) {
	body, content_type := create_form(
		[]req_file{
			{
				field:     "file1",
				file_name: "file.txt",
				data:      []byte("text data"),
			},
			// {
			// 	field:     "abc",
			// 	file_name: "file.png",
			// 	data:      []byte{1, 2, 3, 4},
			// },
		},
		map[string]string{"x": "1", "yy": "val"},
	)
	std_req := httptest.NewRequest(
		http.MethodPost, "/submit", body,
	)
	std_req.Header.Set("Content-Type", content_type)

	parser, err := reqparser.New[RequestForm]()
	assert.NoError(t, err)
	req := adapter.AdaptReq(std_req, httptest.NewRecorder())
	res, err := parser.Parse(req, map[string]string{}, nil)
	assert.NoError(t, err)
	casted := res.(RequestForm)
	assert.NotNil(t, casted.File1)
	assert.Nil(t, casted.File2)
}

func TestParseFileErr(t *testing.T) {
	req := &mocks.RawRequest{}
	req.On("FormFile", mock.Anything).Return(nil, errors.New("err"))
	parser, err := reqparser.New[RequestForm]()
	assert.NoError(t, err)
	_, err = parser.Parse(req, map[string]string{}, nil)
	assert.Equal(t, "Invalid form field(file1): err", err.Error())
	req.AssertExpectations(t)
}

type RequestFormIntrfType struct {
	F CustomType `srv:"form"`
}

type CustomType struct {
	val string
}

func (CustomType) ParseForm(val string) (any, error) {
	return CustomType{val: val + "!"}, nil
}

func (CustomType) GenFormSample() string {
	return "!"
}

func TestCustomFormType(t *testing.T) {
	body, content_type := create_form(
		nil, map[string]string{"f": "x"},
	)
	std_req := httptest.NewRequest(
		http.MethodPost, "/submit", body,
	)
	std_req.Header.Set("Content-Type", content_type)
	req := adapter.AdaptReq(std_req, httptest.NewRecorder())

	parser, err := reqparser.New[RequestFormIntrfType]()
	assert.NoError(t, err)
	res, err := parser.Parse(req, map[string]string{}, nil)
	assert.NoError(t, err)
	field := res.(RequestFormIntrfType).F
	assert.Equal(t, "x!", field.val)
}

type RequestFormDefField struct {
	F int `srv:"form" default:"2"`
}

func TestFormDefField(t *testing.T) {
	body, content_type := create_form(nil, nil)
	std_req := httptest.NewRequest(
		http.MethodPost, "/submit", body,
	)
	std_req.Header.Set("Content-Type", content_type)
	req := adapter.AdaptReq(std_req, httptest.NewRecorder())

	parser, err := reqparser.New[RequestFormDefField]()
	assert.NoError(t, err)
	res, err := parser.Parse(req, map[string]string{}, nil)
	assert.NoError(t, err)
	field := res.(RequestFormDefField).F
	assert.Equal(t, 2, field)
}

type RequestFormOptField struct {
	A *int    `srv:"form"`
	B *string `srv:"form"`
}

func TestFormOptIntField(t *testing.T) {
	body, content_type := create_form(
		nil, map[string]string{"a": "3"},
	)
	std_req := httptest.NewRequest(
		http.MethodPost, "/submit", body,
	)
	std_req.Header.Set("Content-Type", content_type)
	req := adapter.AdaptReq(std_req, httptest.NewRecorder())

	parser, err := reqparser.New[RequestFormOptField]()
	assert.NoError(t, err)
	res, err := parser.Parse(req, map[string]string{}, nil)
	assert.NoError(t, err)
	casted := res.(RequestFormOptField)
	assert.Equal(t, opt.FromValue(3), casted.A)
	assert.Nil(t, casted.B)
}

func TestFormOptStrField(t *testing.T) {
	body, content_type := create_form(
		nil, map[string]string{"b": "abc"},
	)
	std_req := httptest.NewRequest(
		http.MethodPost, "/submit", body,
	)
	std_req.Header.Set("Content-Type", content_type)
	req := adapter.AdaptReq(std_req, httptest.NewRecorder())

	parser, err := reqparser.New[RequestFormOptField]()
	assert.NoError(t, err)
	res, err := parser.Parse(req, map[string]string{}, nil)
	assert.NoError(t, err)
	casted := res.(RequestFormOptField)
	assert.Equal(t, opt.FromValue("abc"), casted.B)
	assert.Nil(t, casted.A)
}

func TestFormValNotSet(t *testing.T) {
	req := adapter.AdaptReq(
		httptest.NewRequest(
			http.MethodPost, "/submit", nil,
		),
		httptest.NewRecorder(),
	)

	parser, err := reqparser.New[RequestFormIntrfType]()
	assert.NoError(t, err)
	_, err = parser.Parse(req, map[string]string{}, nil)
	assert.Equal(
		t,
		"Invalid form field(f): parameter not set",
		err.Error(),
	)
}
