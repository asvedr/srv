package reqparser_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/reqparser"
)

func TestBodyDeclaredTwice(t *testing.T) {
	type query struct {
		X int `srv:"body"`
		Y int `srv:"body"`
	}
	_, err := reqparser.New[query]()
	exp := &errs.ErrReqIsInvalid{}
	assert.ErrorAs(t, err, exp)
	assert.Equal(t, "Y", exp.Field)
	assert.Equal(t, "body already declared at X", exp.Reason)
}

func TestBodyDeclaredInParent(t *testing.T) {
	type Parent struct {
		A int `srv:"body"`
	}
	type query struct {
		Parent `srv:"include"`
		X      int `srv:"body"`
	}
	_, err := reqparser.New[query]()
	exp := &errs.ErrReqIsInvalid{}
	assert.ErrorAs(t, err, exp)
	assert.Equal(t, "Parent", exp.Field)
	assert.Equal(t, "body already declared at X", exp.Reason)
}

func TestFormDeclaredInParent(t *testing.T) {
	type FormParent struct {
		A int `srv:"form"`
	}
	type FormReq struct {
		FormParent `srv:"include"`
		X          int `srv:"form" name:"a"`
	}
	_, err := reqparser.New[FormReq]()
	exp := &errs.ErrReqIsInvalid{}
	assert.ErrorAs(t, err, exp)
	assert.Equal(t, "X", exp.Field)
	assert.Equal(
		t,
		`form param "a" already declared at field "FormParent"`,
		exp.Reason,
	)
}

func TestQueryDeclaredTwice(t *testing.T) {
	type query struct {
		X int `srv:"query" name:"a"`
		Y int `srv:"query" name:"a"`
	}
	_, err := reqparser.New[query]()
	assert.NotNil(t, err)
	exp := &errs.ErrReqIsInvalid{}
	assert.ErrorAs(t, err, exp)
	assert.Equal(t, "Y", exp.Field)
	assert.Equal(t, `query param "a" already declared at field "X"`, exp.Reason)
}

func TestQueryDeclaredInParent(t *testing.T) {
	type QA struct {
		X int `srv:"query" name:"a"`
	}
	type QB struct {
		X int `srv:"query" name:"a"`
	}
	type query struct {
		QA `srv:"include"`
		QB `srv:"include"`
	}
	_, err := reqparser.New[query]()
	assert.NotNil(t, err)
	exp := &errs.ErrReqIsInvalid{}
	assert.ErrorAs(t, err, exp)
	assert.Equal(t, "QB", exp.Field)
	assert.Equal(t, `query param "a" already declared at field "QA"`, exp.Reason)
}

func TestTagSrvNotSet(t *testing.T) {
	type request struct {
		A int
	}

	_, err := reqparser.New[request]()
	exp := &errs.ErrReqIsInvalid{}
	assert.ErrorAs(t, err, exp)
	assert.Equal(t, "A", exp.Field)
	assert.Equal(t, "tag srv not set", exp.Reason)
}

func TestFormWithOtherParams(t *testing.T) {
	type ReqFormWithQuery struct {
		F int `srv:"form"`
		Q int `srv:"query"`
	}
	_, err := reqparser.New[ReqFormWithQuery]()
	assert.Equal(
		t,
		"request is invalid. field: F, reason: can not use form with other params",
		err.Error(),
	)
}
