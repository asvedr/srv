package reqparser_test

import (
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/reqparser"
	adapter "gitlab.com/asvedr/srv/internal/std_engine"
)

func TestParseNone(t *testing.T) {
	parser, err := reqparser.New[srv.None]()
	assert.NoError(t, err)
	assert.Nil(t, parser.BodyType())
	assert.Equal(t, 0, len(parser.QParams()))
	req, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, srv.None{}, req)
}

func TestStdBodyStr(t *testing.T) {
	parser, err := reqparser.New[srv.Body[string]]()
	assert.NoError(t, err)
	assert.Equal(t, reflect.TypeOf(""), *parser.BodyType())
	assert.Equal(t, 0, len(parser.QParams()))
	req, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path", strings.NewReader("abc")),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, "abc", req.(srv.Body[string]).Body)
}

func TestStdBodyBytes(t *testing.T) {
	parser, err := reqparser.New[srv.Body[[]byte]]()
	assert.NoError(t, err)
	tp := reflect.TypeOf([]byte{})
	assert.Equal(t, tp, *parser.BodyType())
	assert.Equal(t, 0, len(parser.QParams()))
	req, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path", strings.NewReader("abc")),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, []byte("abc"), req.(srv.Body[[]byte]).Body)
}

func TestNoBodyWhenExpected(t *testing.T) {
	parser, err := reqparser.New[srv.Body[string]]()
	assert.NoError(t, err)
	assert.Equal(t, reflect.TypeOf(""), *parser.BodyType())
	assert.Equal(t, 0, len(parser.QParams()))
	req, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, "", req.(srv.Body[string]).Body)
}
