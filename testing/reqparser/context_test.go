package reqparser_test

import (
	"fmt"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/opt"
	"gitlab.com/asvedr/srv/internal/reqparser"
	adapter "gitlab.com/asvedr/srv/internal/std_engine"
)

type context_var_req struct {
	X int `srv:"context" name:"a"`
}

type context_var_opt struct {
	A *int `srv:"context"`
}

func TestContextReq(t *testing.T) {
	var cases = []struct {
		ctx map[string]any
		res context_var_req
		err string
	}{
		{ctx: map[string]any{"a": 1}, res: context_var_req{X: 1}},
		{
			ctx: map[string]any{"a": opt.FromValue(2)},
			err: "Invalid context param(a): expected int, got *int",
		},
		{
			ctx: map[string]any{"a": "x"},
			err: "Invalid context param(a): expected int, got string",
		},
		{
			ctx: map[string]any{},
			err: "Invalid context param(a): context var a not found",
		},
		{
			ctx: map[string]any{"a": nil},
			res: context_var_req{X: 0},
		},
	}
	f := func(t *testing.T, i int) {
		parser, err := reqparser.New[context_var_req]()
		assert.NoError(t, err)
		obj, err := parser.Parse(
			adapter.AdaptReq(
				httptest.NewRequest(
					"GET",
					"/path?a=1&b=2&c=3",
					strings.NewReader("lorem ipsum"),
				),
				httptest.NewRecorder(),
			),
			map[string]string{},
			cases[i].ctx,
		)
		if cases[i].err == "" {
			assert.NoError(t, err)
			assert.Equal(
				t,
				cases[i].res,
				obj,
			)
		} else {
			assert.Equal(t, cases[i].err, err.Error())
		}
	}
	for i := range cases {
		t.Run(fmt.Sprint(i), func(t *testing.T) { f(t, i) })
	}
}

func TestContextOpt(t *testing.T) {
	var cases = []struct {
		ctx map[string]any
		res context_var_opt
		err string
	}{
		{
			ctx: map[string]any{"a": 1},
			res: context_var_opt{A: opt.FromValue(1)},
		},
		{
			ctx: map[string]any{"a": opt.FromValue(2)},
			res: context_var_opt{A: opt.FromValue(2)},
		},
		{
			ctx: map[string]any{"a": "x"},
			err: "Invalid context param(a): expected *int, got string",
		},
		{
			ctx: map[string]any{},
			res: context_var_opt{},
		},
		{
			ctx: map[string]any{"a": nil},
			res: context_var_opt{},
		},
	}
	f := func(t *testing.T, i int) {
		parser, err := reqparser.New[context_var_opt]()
		assert.NoError(t, err)
		obj, err := parser.Parse(
			adapter.AdaptReq(
				httptest.NewRequest(
					"GET", "/path",
					strings.NewReader("lorem ipsum"),
				),
				httptest.NewRecorder(),
			),
			map[string]string{},
			cases[i].ctx,
		)
		if cases[i].err == "" {
			assert.NoError(t, err)
			assert.Equal(t, cases[i].res, obj)
		} else {
			assert.Equal(t, cases[i].err, err.Error())
		}
	}
	for i := range cases {
		t.Run(fmt.Sprint(i), func(t *testing.T) { f(t, i) })
	}
}

func TestContextBuildErrs(t *testing.T) {
	type PtrToPtr struct {
		X **int `srv:"context" name:"a"`
	}
	_, err := reqparser.New[PtrToPtr]()
	assert.Equal(t, "unsupported type: **int (pointer to pointer)", err.Error())
}
