package reqparser_test

import (
	"encoding/base64"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/reqparser"
	adapter "gitlab.com/asvedr/srv/internal/std_engine"
)

type TokenHeader struct {
	Token string
}

func (self *TokenHeader) ParseHeader(src string) (any, error) {
	if src == "nil" {
		return nil, nil
	}
	if src == "badtype" {
		return 123, nil
	}
	bts, err := base64.StdEncoding.DecodeString(src)
	if err != nil {
		return nil, err
	}
	return TokenHeader{string(bts)}, nil
}

func (*TokenHeader) GenHeaderSample() string {
	return base64.StdEncoding.EncodeToString([]byte{1, 2, 3})
}

func TestParseHeaderOk(t *testing.T) {
	var _ proto.IHeaderParam = &TokenHeader{}

	type t_req struct {
		Token TokenHeader `srv:"header" name:"X-Token"`
	}

	parser, err := reqparser.New[t_req]()
	assert.NoError(t, err)

	httpreq := httptest.NewRequest("GET", "/my/path/", nil)
	httpreq.Header.Set("X-Token", "aGVsbG8=\n")
	req := adapter.AdaptReq(httpreq, httptest.NewRecorder())
	parsed, err := parser.Parse(req, map[string]string{}, nil)
	assert.NoError(t, err)
	token := parsed.(t_req).Token
	assert.Equal(t, "hello", token.Token)
}

func TestParseHeaderErr(t *testing.T) {
	var _ proto.IHeaderParam = &TokenHeader{}

	type t_req struct {
		Token TokenHeader `srv:"header" name:"X-Token"`
	}

	parser, err := reqparser.New[t_req]()
	assert.NoError(t, err)

	httpreq := httptest.NewRequest("GET", "/my/path/", nil)
	httpreq.Header.Set("X-Token", "ελληνικά")
	req := adapter.AdaptReq(httpreq, httptest.NewRecorder())
	_, err = parser.Parse(req, map[string]string{}, nil)
	assert.Error(t, err)
	assert.Equal(t, "Invalid header(X-Token): illegal base64 data at input byte 0", err.Error())
}

func TestErrHeaderNameNotSet(t *testing.T) {
	type t_req struct {
		Token TokenHeader `srv:"header"`
	}

	_, err := reqparser.New[t_req]()
	assert.Error(t, err)
	assert.Equal(t, `header field Token has no "name" tag`, err.Error())
}

func TestErrHeaderNotImplemented(t *testing.T) {
	type t_req struct {
		Token chan int `srv:"header" name:"X-Token"`
	}

	_, err := reqparser.New[t_req]()
	assert.Error(t, err)
	assert.Equal(t, `unsupported type: chan int (try implement srv.IHeaderParam)`, err.Error())
}

func TestErrHeaderRetNil(t *testing.T) {
	type t_req struct {
		Token TokenHeader `srv:"header" name:"X-Token"`
	}

	parser, err := reqparser.New[t_req]()
	assert.NoError(t, err)
	httpreq := httptest.NewRequest("GET", "/my/path/", nil)
	httpreq.Header.Set("X-Token", "nil")
	req := adapter.AdaptReq(httpreq, httptest.NewRecorder())
	_, err = parser.Parse(req, map[string]string{}, nil)
	assert.Error(t, err)
	assert.Equal(
		t,
		`Invalid header(X-Token): custom parser returns <nil> when expected reqparser_test.TokenHeader`,
		err.Error(),
	)
}

func TestErrHeaderRetBadType(t *testing.T) {
	type t_req struct {
		Token TokenHeader `srv:"header" name:"X-Token"`
	}

	parser, err := reqparser.New[t_req]()
	assert.NoError(t, err)
	httpreq := httptest.NewRequest("GET", "/my/path/", nil)
	httpreq.Header.Set("X-Token", "badtype")
	req := adapter.AdaptReq(httpreq, httptest.NewRecorder())
	_, err = parser.Parse(req, map[string]string{}, nil)
	assert.Error(t, err)
	assert.Equal(
		t,
		`Invalid header(X-Token): custom parser returns int when expected reqparser_test.TokenHeader`,
		err.Error(),
	)
}
