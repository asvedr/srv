package reqparser_test

import (
	"errors"
	"fmt"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/reqparser"
	adapter "gitlab.com/asvedr/srv/internal/std_engine"
)

type q_template[T any] struct {
	X T `srv:"query" name:"x"`
}

type iq_template interface {
	Get() string
}

func (self q_template[T]) Get() string {
	return fmt.Sprintf("%v", self.X)
}

func TestQIntTypes(t *testing.T) {
	queries := []any{
		q_template[int]{},
		q_template[int8]{},
		q_template[int16]{},
		q_template[int32]{},
		q_template[int64]{},

		q_template[uint]{},
		q_template[uint8]{},
		q_template[uint16]{},
		q_template[uint32]{},
		q_template[uint64]{},
	}
	for _, query := range queries {
		label := fmt.Sprintf("%T", query)
		t.Run(label, func(t *testing.T) {
			parser, err := reqparser.NewDyn(query)
			assert.NoError(t, err)
			req := adapter.AdaptReq(
				httptest.NewRequest("GET", "/my/path?x=1", nil),
				httptest.NewRecorder(),
			)
			parsed, err := parser.Parse(
				req, map[string]string{}, nil,
			)
			assert.NoError(t, err)
			assert.Equal(t, "1", parsed.(iq_template).Get())

			req = adapter.AdaptReq(
				httptest.NewRequest("GET", "/my/path?x=true", nil),
				httptest.NewRecorder(),
			)
			_, err = parser.Parse(req, map[string]string{}, nil)
			assert.Error(t, err)
		})
	}
}

func TestQFloatTypes(t *testing.T) {
	queries := []any{
		q_template[float32]{},
		q_template[float64]{},
	}
	for _, query := range queries {
		label := fmt.Sprintf("%T", query)
		t.Run(label, func(t *testing.T) {
			parser, err := reqparser.NewDyn(query)
			assert.NoError(t, err)
			req := adapter.AdaptReq(
				httptest.NewRequest("GET", "/my/path?x=1.5", nil),
				httptest.NewRecorder(),
			)
			parsed, err := parser.Parse(
				req, map[string]string{}, nil,
			)
			assert.NoError(t, err)
			assert.Equal(t, "1.5", parsed.(iq_template).Get())

			req = adapter.AdaptReq(
				httptest.NewRequest("GET", "/my/path?x=true", nil),
				httptest.NewRecorder(),
			)
			_, err = parser.Parse(req, map[string]string{}, nil)
			assert.Error(t, err)
		})
	}
}

func TestQBool(t *testing.T) {
	var valid_variants = []struct {
		val string
		res bool
	}{
		{"true", true},
		{"1", true},
		{"false", false},
		{"0", false},
	}
	parser, err := reqparser.New[q_template[bool]]()
	assert.NoError(t, err)
	for _, variant := range valid_variants {
		t.Run(variant.val, func(t *testing.T) {
			req := adapter.AdaptReq(
				httptest.NewRequest("GET", "/my/path?x="+variant.val, nil),
				httptest.NewRecorder(),
			)
			parsed, err := parser.Parse(
				req, map[string]string{}, nil,
			)
			assert.NoError(t, err)
			assert.Equal(t, variant.res, parsed.(q_template[bool]).X)
		})
	}
	t.Run("err", func(t *testing.T) {
		req := adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=abc", nil),
			httptest.NewRecorder(),
		)
		_, err = parser.Parse(req, map[string]string{}, nil)
		assert.Error(t, err)
	})
}

type CustomParser struct {
	Val int
}

func (self *CustomParser) ParseQuery(src string) (any, error) {
	if src == "a" {
		return CustomParser{1}, nil
	}
	if src == "b" {
		return CustomParser{2}, nil
	}
	if src == "nil" {
		return nil, nil
	}
	if src == "text" {
		return "text", nil
	}
	return nil, errors.New("custom")
}

func (self *CustomParser) GenQuerySample() string {
	return "a"
}

func TestCustomQType(t *testing.T) {
	// validate interface
	var _ srv.IQueryParam = &CustomParser{}
	type query struct {
		X CustomParser `srv:"query" name:"x"`
	}
	parser, err := reqparser.New[query]()
	assert.NoError(t, err)

	parsed, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=a", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, 1, parsed.(query).X.Val)

	parsed, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=b", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, 2, parsed.(query).X.Val)

	_, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=nil", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.Error(t, err)
	assert.Equal(
		t,
		"Invalid query param(x): custom parser returns <nil> when expected reqparser_test.CustomParser",
		err.Error(),
	)

	_, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=text", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.Error(t, err)
	assert.Equal(
		t,
		"Invalid query param(x): custom parser returns string when expected reqparser_test.CustomParser",
		err.Error(),
	)

	_, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=err", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.Error(t, err)
	assert.Equal(
		t,
		"Invalid query param(x): custom",
		err.Error(),
	)
}

func TestOptField(t *testing.T) {
	type query struct {
		X *int `srv:"query" name:"x"`
	}
	parser, err := reqparser.New[query]()
	assert.NoError(t, err)

	parsed, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=1", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, 1, *parsed.(query).X)

	parsed, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path/", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Nil(t, parsed.(query).X)

	_, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=abc", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.Error(t, err)
}

func TestDefaultField(t *testing.T) {
	type query struct {
		X int `srv:"query" name:"x" default:"2"`
	}
	parser, err := reqparser.New[query]()
	assert.NoError(t, err)

	parsed, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=1", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, 1, parsed.(query).X)

	parsed, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path/", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, 2, parsed.(query).X)

	_, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=abc", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.Error(t, err)
}

func TestListField(t *testing.T) {
	type query struct {
		X []int `srv:"query" name:"x"`
	}
	parser, err := reqparser.New[query]()
	assert.NoError(t, err)

	parsed, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=1&x=2", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, []int{1, 2}, parsed.(query).X)

	parsed, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x[]=1&x[]=2", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, []int{1, 2}, parsed.(query).X)

	parsed, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path/", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(t, []int{}, parsed.(query).X)

	_, err = parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest("GET", "/my/path?x=abc", nil),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.Error(t, err)
}
