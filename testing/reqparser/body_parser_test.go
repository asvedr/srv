package reqparser_test

import (
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/reqparser"
	adapter "gitlab.com/asvedr/srv/internal/std_engine"
)

type body struct {
	X int `json:"x"`
	Y int `json:"y"`
}

func TestBodyJson(t *testing.T) {
	parser, err := reqparser.New[srv.Body[body]]()
	assert.NoError(t, err)
	req, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest(
				"POST",
				"/my/path",
				strings.NewReader(`{"y": 2, "x": 1}`),
			),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)
	assert.NoError(t, err)
	assert.Equal(
		t,
		body{X: 1, Y: 2},
		req.(srv.Body[body]).Body,
	)
}
