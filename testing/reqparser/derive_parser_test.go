package reqparser_test

import (
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/reqparser"
	adapter "gitlab.com/asvedr/srv/internal/std_engine"
)

func TestIncludeQuery(t *testing.T) {
	type Q_grand_parent struct {
		X int `srv:"query" name:"a"`
	}
	type q_parent struct {
		Q_grand_parent `srv:"include"`
		X              int `srv:"query" name:"b"`
	}
	type b_parent struct {
		Val string `srv:"body"`
	}
	type child struct {
		Q q_parent `srv:"include"`
		B b_parent `srv:"include"`
		X int      `srv:"query" name:"c"`
	}
	parser, err := reqparser.New[child]()
	assert.NoError(t, err)

	t_params := parser.QParams()
	assert.Equal(t, 3, len(t_params))
	assert.Equal(t, "a", t_params[0].Key)
	assert.Equal(t, "b", t_params[1].Key)
	assert.Equal(t, "c", t_params[2].Key)

	t_body := parser.BodyType()
	assert.Equal(t, reflect.TypeOf(""), *t_body)

	req, err := parser.Parse(
		adapter.AdaptReq(
			httptest.NewRequest(
				"GET",
				"/path?a=1&b=2&c=3",
				strings.NewReader("lorem ipsum"),
			),
			httptest.NewRecorder(),
		),
		map[string]string{},
		nil,
	)

	assert.NoError(t, err)
	assert.Equal(
		t,
		child{
			Q: q_parent{
				Q_grand_parent: Q_grand_parent{X: 1},
				X:              2,
			},
			B: b_parent{Val: "lorem ipsum"},
			X: 3,
		},
		req,
	)
}
