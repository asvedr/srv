package rserializer_test

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/rserializer"
	"gitlab.com/asvedr/srv/internal/types"
	"gitlab.com/asvedr/srv/testing/mocks"
)

type Response[T any] struct {
	data       T
	status     int
	header_ops []types.HeaderDiff
}

var _ proto.IIntoResponse = Response[string]{}

func (self Response[T]) ResponseType() reflect.Type {
	var t T
	return reflect.TypeOf(t)
}

func (self Response[T]) IntoResponse() types.Response {
	return types.Response{
		Data:      self.data,
		Status:    self.status,
		HeaderOps: self.header_ops,
	}
}

var diff = types.HeaderDiff{
	Key:  "xxx",
	Type: types.HeaderDiffTypeSet,
	Val:  "1",
}

func TestSerializeStructJson(t *testing.T) {
	type Obj struct {
		X int
		Y int
	}
	ser := rserializer.MakeStruct[Response[Obj]]()
	resp := Response[Obj]{
		data:       Obj{X: 1, Y: 2},
		status:     201,
		header_ops: []types.HeaderDiff{diff},
	}
	headers := &mocks.RawRequest{}
	headers.On("HeaderSet", "xxx", "1")
	result, err := ser(resp, headers)
	assert.NoError(t, err)
	exp := types.InternalResponse{
		Data: []byte(`{"X":1,"Y":2}`), Status: 201,
	}
	assert.Equal(t, exp, result)
	headers.AssertExpectations(t)
}

func TestSerializeStructStr(t *testing.T) {
	ser := rserializer.MakeStruct[Response[string]]()
	resp := Response[string]{
		data:       "str",
		status:     201,
		header_ops: []types.HeaderDiff{diff},
	}
	headers := &mocks.RawRequest{}
	headers.On("HeaderSet", "xxx", "1")
	result, err := ser(resp, headers)
	assert.NoError(t, err)
	exp := types.InternalResponse{
		Data: []byte("str"), Status: 201,
	}
	assert.Equal(t, exp, result)
	headers.AssertExpectations(t)
}

func TestSerializeStructBts(t *testing.T) {
	ser := rserializer.MakeStruct[Response[[]byte]]()
	resp := Response[[]byte]{
		data:       []byte{1, 2, 3},
		status:     201,
		header_ops: []types.HeaderDiff{diff},
	}
	headers := &mocks.RawRequest{}
	headers.On("HeaderSet", "xxx", "1")
	result, err := ser(resp, headers)
	assert.NoError(t, err)
	exp := types.InternalResponse{
		Data: []byte{1, 2, 3}, Status: 201,
	}
	assert.Equal(t, exp, result)
	headers.AssertExpectations(t)
}

func TestSerializeStructNone(t *testing.T) {
	type none struct{}
	ser := rserializer.MakeStruct[Response[none]]()
	resp := Response[none]{
		status:     201,
		header_ops: []types.HeaderDiff{diff},
	}
	headers := &mocks.RawRequest{}
	headers.On("HeaderSet", "xxx", "1")
	result, err := ser(resp, headers)
	assert.NoError(t, err)
	exp := types.InternalResponse{
		Data: []byte("{}"), Status: 201,
	}
	assert.Equal(t, exp, result)
	headers.AssertExpectations(t)
}

func TestSerErr(t *testing.T) {
	type Struct struct {
		F chan int
	}
	ser1 := rserializer.MakeStd[Struct]()
	headers := &mocks.RawRequest{}
	_, err := ser1(Struct{F: make(chan int)}, headers)
	assert.Error(t, err)

	ser2 := rserializer.MakeStruct[Response[Struct]]()
	_, err = ser2(
		Response[Struct]{data: Struct{F: make(chan int)}},
		headers,
	)
	assert.Error(t, err)
}

func TestSerBts(t *testing.T) {
	headers := &mocks.RawRequest{}
	ser := rserializer.MakeStd[[]byte]()
	res, err := ser(nil, headers)
	assert.NoError(t, err)
	assert.Equal(t, []byte{}, res.Data)
}
