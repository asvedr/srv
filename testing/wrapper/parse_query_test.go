package wrapper_test

import (
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	adapter "gitlab.com/asvedr/srv/internal/std_engine"
)

func TestParseQueryStruct(t *testing.T) {
	type query struct {
		X int `srv:"query" name:"x"`
		Y int `srv:"query" name:"y"`
	}

	wrapped := srv.MakeHandler(pb_handler[query]{})
	req := adapter.AdaptReq(
		httptest.NewRequest("GET", "/my/path?x=1&y=2", nil),
		httptest.NewRecorder(),
	)
	parsed, err := wrapped.DowncastHandler().RequestParser().Parse(
		req, map[string]string{}, nil,
	)
	assert.Nil(t, err)
	assert.Equal(t, query{X: 1, Y: 2}, parsed)
}

func TestParseQueryNil(t *testing.T) {
	wrapped := srv.MakeHandler(pb_handler[struct{}]{})
	req := adapter.AdaptReq(
		httptest.NewRequest("GET", "/my/path?x=1&y=2", nil),
		httptest.NewRecorder(),
	)
	parsed, err := wrapped.DowncastHandler().RequestParser().Parse(
		req, map[string]string{}, nil,
	)
	assert.Nil(t, err)
	assert.Equal(t, struct{}{}, parsed)
}

func TestInvalidQuery(t *testing.T) {
	type query struct {
		X chan int `srv:"query"`
	}
	msg, ok := catch_panic(func() { srv.MakeHandler(pb_handler[query]{}) })
	assert.False(t, ok)
	assert.Contains(
		t,
		msg.(string),
		"unsupported type: chan int (try implement srv.IQueryParam)",
	)
}

func catch_panic(f func()) (val any, ok bool) {
	defer func() { val = recover() }()
	f()
	ok = true
	return
}
