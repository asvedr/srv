package wrapper_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
)

type form_params_handler struct {
	res_handler[none, string]
}

func (self form_params_handler) FormParams() any {
	return map[string][]string{
		"x": {"1"},
	}
}

func TestFormParams(t *testing.T) {
	handler := srv.MakeHandler(form_params_handler{})
	exp := map[string][]string{
		"x": {"1"},
	}
	assert.Equal(t, exp, handler.DowncastHandler().FormParams())
}

func TestDowncast(t *testing.T) {
	handler := srv.MakeHandler(form_params_handler{})
	unpacked := handler.DowncastHandler()
	assert.Equal(t, handler, unpacked)
}
