package wrapper_test

import (
	"errors"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/proto"
)

type descr_query struct {
	Ints []int   `srv:"query" name:"ints"`
	Name *string `srv:"query" name:"name"`
}

type body_value struct {
	Ints []int  `json:"ints"`
	Name string `json:"name"`
}

type descr_body struct {
	body body_value `srv:"body"`
}

type descr_request struct {
	descr_query `srv:"include"`
	descr_body  `srv:"include"`
}

type descr_handler[Req, Resp any] struct{}

func (self descr_handler[Req, Resp]) FormParams() any {
	return nil
}

func (self descr_handler[Req, Resp]) Errors() []error {
	return nil
}

func (self descr_handler[Req, Resp]) Method() string {
	return "GET"
}

func (self descr_handler[Req, Resp]) Description() string {
	return "help"
}

func (self descr_handler[Req, Resp]) Path() string {
	return "/my/path"
}

func (self descr_handler[Req, Resp]) Handle(Req) (Resp, error) {
	panic("unreachable")
}

var full_descr = strings.TrimSpace(`
<div class="url"><div class="method">GET</div><div class="path">my/path/</div></div>
<div class="qbrd_label">Query:</div>
<div class="query_param"><div class="query_param_key">ints:</div><div class="value">[-1]</div></div>
<div class="query_param"><div class="query_param_key">name:</div><div class="value">string</div><div class="query_param_optional">optional</div></div>
<div class="qbrd_label">Body:</div>
<div class="value json"><pre>{
 "ints": [
  -1
 ],
 "name": "string"
}</pre></div><div class="qbrd_label">Response:</div>
<div class="value json"><pre>{
 "ints": [
  -1
 ],
 "name": "string"
}</pre></div>
<div class="qbrd_label">Description:</div><div class="description">help</div>
`)

func TestDescriptionFull(t *testing.T) {
	wrapped := srv.MakeHandler(
		descr_handler[descr_request, body_value]{},
	)
	help := wrapped.DowncastHandler().Description("my/path/")
	assert.Equal(t, full_descr, help)
}

var no_q_descr = strings.TrimSpace(`
<div class="url"><div class="method">GET</div><div class="path">my/path/</div></div>
<div class="qbrd_label">Body:</div>
<div class="value json"><pre>{
 "ints": [
  -1
 ],
 "name": "string"
}</pre></div><div class="qbrd_label">Response:</div>
<div class="value json"><pre>{
 "ints": [
  -1
 ],
 "name": "string"
}</pre></div>
<div class="qbrd_label">Description:</div><div class="description">help</div>
`)

func TestDescriptionNoQuery(t *testing.T) {
	wrapped := srv.MakeHandler(descr_handler[descr_body, body_value]{})
	help := wrapped.DowncastHandler().Description("my/path/")
	assert.Equal(t, no_q_descr, help)
}

var no_b_descr = strings.TrimSpace(`
<div class="url"><div class="method">GET</div><div class="path">my/path/</div></div>
<div class="qbrd_label">Query:</div>
<div class="query_param"><div class="query_param_key">ints:</div><div class="value">[-1]</div></div>
<div class="query_param"><div class="query_param_key">name:</div><div class="value">string</div><div class="query_param_optional">optional</div></div><div class="qbrd_label">Response:</div>
<div class="value json"><pre>{
 "ints": [
  -1
 ],
 "name": "string"
}</pre></div>
<div class="qbrd_label">Description:</div><div class="description">help</div>
`)

func TestDescriptionNoBody(t *testing.T) {
	wrapped := srv.MakeHandler(
		descr_handler[descr_query, body_value]{},
	)
	help := wrapped.DowncastHandler().Description("my/path/")
	assert.Equal(t, no_b_descr, help)
}

var no_r_descr = strings.TrimSpace(`
<div class="url"><div class="method">GET</div><div class="path">my/path/</div></div>
<div class="qbrd_label">Query:</div>
<div class="query_param"><div class="query_param_key">ints:</div><div class="value">[-1]</div></div>
<div class="query_param"><div class="query_param_key">name:</div><div class="value">string</div><div class="query_param_optional">optional</div></div>
<div class="qbrd_label">Body:</div>
<div class="value json"><pre>{
 "ints": [
  -1
 ],
 "name": "string"
}</pre></div><div class="qbrd_label">Description:</div><div class="description">help</div>
`)

func TestDescriptionNoResp(t *testing.T) {
	wrapped := srv.MakeHandler(
		descr_handler[descr_request, struct{}]{},
	)
	help := wrapped.DowncastHandler().Description("my/path/")
	assert.Equal(t, no_r_descr, help)
}

const empty_descr = "<div class=\"url\"><div class=\"method\">GET</div><div class=\"path\">my/path/</div></div>\n<div class=\"qbrd_label\">Description:</div><div class=\"description\">help</div>"

func TestDescriptionEmpty(t *testing.T) {
	wrapped := srv.MakeHandler(
		descr_handler[srv.None, srv.None]{},
	)
	help := wrapped.DowncastHandler().Description("my/path/")
	assert.Equal(t, empty_descr, help)
}

var str_to_bts_descr = strings.TrimSpace(`
<div class="url"><div class="method">GET</div><div class="path">my/path/</div></div>
<div class="qbrd_label">Body:</div>
<div class="value string">string</div><div class="qbrd_label">Response:</div>
<div class="value bytes">bytes</div>
<div class="qbrd_label">Description:</div><div class="description">help</div>
`)

func TestDescriptionStrToBytes(t *testing.T) {
	type req struct {
		Body string `srv:"body"`
	}
	wrapped := srv.MakeHandler(descr_handler[req, []byte]{})
	help := wrapped.DowncastHandler().Description("my/path/")
	assert.Equal(t, str_to_bts_descr, help)
}

type QType struct{ Dst int }

func (*QType) ParseQuery(string) (any, error) {
	return nil, nil
}

func (*QType) GenQuerySample() string {
	return "OBJ"
}

var query_custom_descr = strings.TrimSpace(`
<div class="url"><div class="method">GET</div><div class="path">my/path/</div></div>
<div class="qbrd_label">Query:</div>
<div class="query_param"><div class="query_param_key">p:</div><div class="value">OBJ</div></div><div class="qbrd_label">Response:</div>
<div class="value bytes">bytes</div>
<div class="qbrd_label">Description:</div><div class="description">help</div>
`)

func TestCustomQueryType(t *testing.T) {
	var _ proto.IQueryParam = &QType{}
	type req struct {
		Param QType `srv:"query" name:"p"`
	}
	wrapped := srv.MakeHandler(
		descr_handler[req, []byte]{},
	)
	help := wrapped.DowncastHandler().Description("my/path/")
	assert.Equal(t, query_custom_descr, help)
}

type HType struct{}

func (*HType) ParseHeader(string) (any, error) {
	return nil, nil
}

func (*HType) GenHeaderSample() string {
	return "#TOKEN#"
}

var header_descr = strings.TrimSpace(`
<div class="url"><div class="method">GET</div><div class="path">my/path/</div></div>
<div class="qbrd_label">Headers:</div>
<div class="header_param"><div class="header_param_key">X-My-Header:</div><div class="value">#TOKEN#</div></div><div class="qbrd_label">Response:</div>
<div class="value bytes">bytes</div>
<div class="qbrd_label">Description:</div><div class="description">help</div>
`)

func TestDescriptionHeaders(t *testing.T) {
	var _ proto.IHeaderParam = &HType{}
	type req struct {
		Header HType `srv:"header" name:"X-My-Header"`
	}
	wrapped := srv.MakeHandler(
		descr_handler[req, []byte]{},
	)
	help := wrapped.DowncastHandler().Description("my/path/")
	assert.Equal(t, header_descr, help)
}

func TestParentMethods(t *testing.T) {
	var _ proto.IHeaderParam = &HType{}
	type req struct {
		Header HType `srv:"header" name:"X-My-Header"`
	}
	wrapped := srv.MakeHandler(
		descr_handler[req, []byte]{},
	)
	assert.False(t, wrapped.IsRouter())
	assert.Nil(t, wrapped.Children())
	assert.Nil(t, wrapped.DowncastRouter())
	assert.Equal(t, "my/path/", wrapped.Path().Src)
}

type errs_handler struct{ srv.BaseGetHandler }

func (errs_handler) Errors() []error {
	return []error{errors.New("biba")}
}

func (errs_handler) Path() string {
	return "/"
}

func (errs_handler) Handle(srv.None) (string, error) {
	return "", nil
}

const err_help = `
<div class="qbrd_label">Errors:</div>
<div class="error"><div class="error_status"><div class="value">500</div></div><div class="error_code"><div class="value">INTERNAL</div></div><div class="error_description"><div class="value">biba</div></div>
`

func TestDescriptionErrs(t *testing.T) {
	var _ proto.IHeaderParam = &HType{}
	wrapped := srv.MakeHandler(errs_handler{})
	help := wrapped.DowncastHandler().Description("my/path/")
	assert.True(
		t,
		strings.Contains(help, strings.TrimSpace(err_help)),
	)
}
