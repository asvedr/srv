package wrapper_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/types"
	"gitlab.com/asvedr/srv/testing/mocks"
)

type none struct{}

type res_handler[Req, Resp any] struct {
	res Resp
}

type err_handler[Req any] struct {
	res_handler[Req, none]
	err error
}

func (self res_handler[Req, Resp]) FormParams() any {
	return nil
}

func (self res_handler[Req, Resp]) Errors() []error {
	return nil
}

func (self res_handler[Req, Resp]) Method() string {
	return "GET"
}

func (self res_handler[Req, Resp]) Description() string {
	return "help"
}

func (self res_handler[Req, Resp]) Path() string {
	return "/my/path"
}

func (self res_handler[Req, Resp]) Handle(Req) (Resp, error) {
	return self.res, nil
}

func (self err_handler[Req]) Handle(Req) (none, error) {
	return none{}, self.err
}

func TestProcResultBytes(t *testing.T) {
	wrapped := srv.MakeHandler(
		res_handler[none, []byte]{[]byte{1, 2, 3}},
	)
	raw_req := &mocks.RawRequest{}
	res, err := wrapped.DowncastHandler().Handle(
		raw_req,
		types.InternalRequest{Req: none{}},
	)
	assert.Nil(t, err)
	assert.Equal(t, []byte{1, 2, 3}, res.Data)
}

func TestProcResultString(t *testing.T) {
	wrapped := srv.MakeHandler(
		res_handler[none, string]{"hello"},
	)
	raw_req := &mocks.RawRequest{}
	res, err := wrapped.DowncastHandler().Handle(
		raw_req,
		types.InternalRequest{Req: none{}},
	)
	assert.Nil(t, err)
	assert.Equal(t, []byte("hello"), res.Data)
}

func TestProcResultJson(t *testing.T) {
	type Val struct {
		X int `json:"x"`
	}
	wrapped := srv.MakeHandler(
		res_handler[none, Val]{Val{X: 1}},
	)
	raw_req := &mocks.RawRequest{}
	res, err := wrapped.DowncastHandler().Handle(
		raw_req,
		types.InternalRequest{Req: none{}},
	)
	assert.Nil(t, err)
	assert.Equal(t, []byte(`{"x":1}`), res.Data)
}

func TestProcResultNone(t *testing.T) {
	wrapped := srv.MakeHandler(
		res_handler[none, struct{}]{},
	)
	raw_req := &mocks.RawRequest{}
	res, err := wrapped.DowncastHandler().Handle(
		raw_req,
		types.InternalRequest{Req: none{}},
	)
	assert.Nil(t, err)
	assert.Equal(t, []byte(`{}`), res.Data)
}

func TestProcResErr(t *testing.T) {
	wrapped := srv.MakeHandler(
		err_handler[none]{err: errors.New("custom")},
	)
	raw_req := &mocks.RawRequest{}
	_, err := wrapped.DowncastHandler().Handle(
		raw_req,
		types.InternalRequest{Req: none{}},
	)
	assert.Equal(t, "custom", err.Error())
}
