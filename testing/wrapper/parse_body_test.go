package wrapper_test

import (
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	adapter "gitlab.com/asvedr/srv/internal/std_engine"
	"gitlab.com/asvedr/srv/internal/types"
)

type pb_handler[R any] struct{}

func (self pb_handler[R]) FormParams() any {
	return nil
}

func (self pb_handler[R]) Errors() []error {
	return nil
}

func (self pb_handler[R]) Method() string {
	return "GET"
}

func (self pb_handler[R]) Description() string {
	return "help"
}

func (self pb_handler[R]) Path() string {
	return "/my/path"
}

func (self pb_handler[R]) Handle(R) (struct{}, error) {
	panic("unreachable")
}

func TestParseBodyStruct(t *testing.T) {
	type body struct {
		X int `json:"x"`
		Y int `json:"y"`
	}

	type req struct {
		Body body `srv:"body"`
	}

	wrapped := srv.MakeHandler(pb_handler[req]{})
	r := adapter.AdaptReq(
		httptest.NewRequest("GET", "/my/path", strings.NewReader(`{"x": 1, "y": 2}`)),
		httptest.NewRecorder(),
	)
	parsed, err := wrapped.DowncastHandler().RequestParser().Parse(
		r, map[string]string{}, nil,
	)
	assert.Nil(t, err)
	assert.Equal(t, body{X: 1, Y: 2}, parsed.(req).Body)
}

func TestParseBodyString(t *testing.T) {
	type req struct {
		Body string `srv:"body"`
	}
	wrapped := srv.MakeHandler(pb_handler[req]{})
	r := adapter.AdaptReq(
		httptest.NewRequest("GET", "/my/path", strings.NewReader(`{"x": 1, "y": 2}`)),
		httptest.NewRecorder(),
	)
	parsed, err := wrapped.DowncastHandler().RequestParser().Parse(
		r, map[string]string{}, nil,
	)
	assert.Nil(t, err)
	assert.Equal(t, `{"x": 1, "y": 2}`, parsed.(req).Body)
}

func TestParseBodyBytes(t *testing.T) {
	type req struct {
		Body []byte `srv:"body"`
	}

	wrapped := srv.MakeHandler(pb_handler[req]{})
	r := adapter.AdaptReq(
		httptest.NewRequest("GET", "/my/path", strings.NewReader(`{"x": 1, "y": 2}`)),
		httptest.NewRecorder(),
	)
	parsed, err := wrapped.DowncastHandler().RequestParser().Parse(
		r, map[string]string{}, nil,
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		[]byte(`{"x": 1, "y": 2}`),
		parsed.(req).Body,
	)
}

func TestParseBodyNil(t *testing.T) {
	wrapped := srv.MakeHandler(pb_handler[srv.None]{})
	req := adapter.AdaptReq(
		httptest.NewRequest("GET", "/my/path", strings.NewReader(`{"x": 1, "y": 2}`)),
		httptest.NewRecorder(),
	)
	parsed, err := wrapped.DowncastHandler().RequestParser().Parse(
		req, map[string]string{}, nil,
	)
	assert.Nil(t, err)
	assert.Equal(t, types.None{}, parsed)
}
