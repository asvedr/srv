package mocks

import "github.com/stretchr/testify/mock"

func cast[T any](arg mock.Arguments) (res T) {
	obj := arg.Get(0)
	if obj != nil {
		res = obj.(T)
	}
	return
}

func cast2[A, B any](arg mock.Arguments) (a A, b B) {
	obj_a := arg.Get(0)
	obj_b := arg.Get(1)
	if obj_a != nil {
		a = obj_a.(A)
	}
	if obj_b != nil {
		b = obj_b.(B)
	}
	return
}
