package mocks

import (
	"mime/multipart"

	"github.com/stretchr/testify/mock"
	"gitlab.com/asvedr/srv/internal/proto"
)

type RawRequest struct {
	mock.Mock
}

var _ proto.IRawRequest = (*RawRequest)(nil)

func (self *RawRequest) QueryGet(key string) (string, bool) {
	return cast2[string, bool](self.Called(key))
}

func (self *RawRequest) QueryGetMany(key string) ([]string, bool) {
	return cast2[[]string, bool](self.Called(key))
}

func (self *RawRequest) HeaderSet(key string, val string) {
	self.Called(key, val)
}

func (self *RawRequest) HeaderGet(key string) string {
	return cast[string](self.Called(key))
}

func (self *RawRequest) HeaderGetMany(key string) []string {
	return cast[[]string](self.Called(key))
}

func (self *RawRequest) Method() string {
	return cast[string](self.Called())
}

func (self *RawRequest) Path() string {
	return cast[string](self.Called())
}

func (self *RawRequest) QueryRaw() string {
	return cast[string](self.Called())
}

func (self *RawRequest) GetBody() ([]byte, error) {
	return cast2[[]byte, error](self.Called())
}

func (self *RawRequest) FormFile(key string) (*multipart.FileHeader, error) {
	return cast2[*multipart.FileHeader, error](self.Called(key))
}

func (self *RawRequest) FormValue(key string) string {
	return cast[string](self.Called(key))
}
