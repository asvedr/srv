package docgen_test

import (
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv/internal/docgen"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type parser struct {
	hparams []types.HParam
	qparams []types.QParam
	fparams []types.FParam
	body    *reflect.Type
	form    bool
}

func (parser) Parse(proto.IRawRequest, map[string]string, types.Context) (any, error) {
	panic("unexpected")
}

func (self parser) HParams() []types.HParam {
	return self.hparams
}

func (self parser) QParams() []types.QParam {
	return self.qparams
}

func (self parser) FParams() []types.FParam {
	return self.fparams
}

func (self parser) BodyType() *reflect.Type {
	return self.body
}

func (self parser) FormUsed() bool {
	return self.form
}

func TestInvalidQuery(t *testing.T) {
	ch := make(chan int)
	tp := reflect.TypeOf(ch)
	err := docgen.Gen(parser{
		qparams: []types.QParam{{Key: "key", Type: tp}},
	})
	msg := []string{
		"Can not use type chan int in query(try implement srv.IQueryParam)",
		"Cause: non_empty_value: unsupported type: chan int",
	}
	assert.Equal(t, strings.Join(msg, "\n"), err)
}

func TestInvalidBody(t *testing.T) {
	ch := make(chan int)
	tp := reflect.TypeOf(ch)
	err := docgen.Gen(parser{body: &tp})
	msg := "can not gen body: can not gen doc for type chan int: non_empty_value: unsupported type: chan int"
	assert.Equal(t, msg, err)
}

func TestInvalidHeader(t *testing.T) {
	ch := make(chan int)
	tp := reflect.TypeOf(ch)
	err := docgen.Gen(parser{
		hparams: []types.HParam{{Key: "key", Type: tp}},
	})
	msg := []string{
		"Can not use type chan int in header(try implement srv.IHeaderParam)",
		"Cause: not implemented",
	}
	assert.Equal(t, strings.Join(msg, "\n"), err)
}
