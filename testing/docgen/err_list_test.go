package docgen_test

import (
	"errors"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/docgen"
)

var expected_doc = `
<div class="qbrd_label">Errors:</div>
<div class="error"><div class="error_status"><div class="value">500</div></div><div class="error_code"><div class="value">INTERNAL</div></div><div class="error_description"><div class="value">this is int</div></div></div>
<div class="error"><div class="error_status"><div class="value">404</div></div><div class="error_code"><div class="value">NOT_FOUND</div></div><div class="error_description"><div class="value">Url not found</div></div></div>
<div class="error"><div class="error_status"><div class="value">456</div></div><div class="error_code"><div class="value">CUROM_ERR</div></div><div class="error_description"><div class="value">my litte error</div></div></div>
`

func TestErrList(t *testing.T) {
	assert.Equal(t, "", docgen.GenErrList([]error{}))

	doc := docgen.GenErrList([]error{
		// 500
		errors.New("this is int"),
		// std 404
		srv.Err.NotFound,
		// full custom
		srv.Err.New(456, "CUROM_ERR", "my litte error"),
	})

	assert.Equal(t, strings.TrimSpace(expected_doc), doc)
}
