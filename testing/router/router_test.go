package router_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/router"
)

type handler struct{ srv.BaseGetHandler }

type request struct{}

func (handler) Path() string {
	return "a"
}

func (handler) Handle(request) ([]byte, error) {
	return nil, nil
}

func TestParentMethods(t *testing.T) {
	h := srv.MakeHandler(handler{})
	r := router.New("/api/", []proto.IRoutable{h})
	assert.Equal(t, "", r.Method())
	assert.Nil(t, r.DowncastHandler())
	assert.Equal(t, r, r.DowncastRouter())
}

func TestInvalidPath(t *testing.T) {
	assert.Panics(t, func() {
		router.New("/::a/b", []proto.IRoutable{})
	})
}
