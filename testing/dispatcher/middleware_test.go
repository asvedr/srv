package dispatcher_test

import (
	"errors"
	"fmt"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/dispatcher"
	"gitlab.com/asvedr/srv/internal/err_to_api"
	"gitlab.com/asvedr/srv/internal/handler_contaier"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/router"
	"gitlab.com/asvedr/srv/internal/std_engine"
)

type CtxReq struct {
	X int `srv:"context" name:"x"`
}

type CtxHandler struct {
	srv.BaseGetHandler
}

func (CtxHandler) Path() string {
	return "/a"
}

func (CtxHandler) Handle(req CtxReq) (string, error) {
	return fmt.Sprint(req.X + 1), nil
}

type MidSetCtx struct{ srv.BaseMiddleware }

func (MidSetCtx) OnReq(arg srv.IMiddlewareReqArg) error {
	arg.Context()["x"] = 1
	return nil
}

type MidSetHeader struct{ srv.BaseMiddleware }

func (MidSetHeader) OnReq(arg srv.IMiddlewareReqArg) error {
	x := arg.HeaderGet("x")
	y := arg.HeaderGetMany("y")
	z := fmt.Sprintf("x: %s, y: %v", x, y)
	arg.HeaderSet("z", z)
	return nil
}

func TestMiddlewareReqOk(t *testing.T) {
	handler := srv.MakeHandler(CtxHandler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(
		container,
		err_to_api.Proc,
		nil,
		[]srv.IMiddleware{MidSetCtx{}},
	)
	resp := disp.Dispatch(std_engine.AdaptReq(
		httptest.NewRequest("GET", "/a?x=1", nil),
		httptest.NewRecorder(),
	))
	assert.Equal(t, 200, resp.Status)
	assert.Equal(t, "2", string(resp.Data))
}

type MidReqErr struct {
	srv.BaseMiddleware
}

func (MidReqErr) OnReq(srv.IMiddlewareReqArg) error {
	return errors.New("custom")
}

func TestMiddlewareReqErr(t *testing.T) {
	handler := srv.MakeHandler(CtxHandler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(
		container,
		err_to_api.Proc,
		nil,
		[]proto.IMiddleware{MidReqErr{}},
	)
	resp := disp.Dispatch(std_engine.AdaptReq(
		httptest.NewRequest("GET", "/a?x=1", nil),
		httptest.NewRecorder(),
	))
	assert.Equal(t, 500, resp.Status)
	assert.Equal(
		t,
		`{"code":"INTERNAL","detail":"custom"}`,
		string(resp.Data),
	)
}

type MidFull struct {
	raise bool
	t     *testing.T
}

func (self MidFull) OnReq(arg proto.IMiddlewareReqArg) error {
	assert.Equal(self.t, "/a", arg.Path())
	assert.Equal(self.t, "GET", arg.Method())
	arg.Context()["x"] = 1
	arg.Context()["req"] = 1
	return nil
}

func (self MidFull) OnResp(arg proto.IMiddlewareRespArg) error {
	if self.raise {
		return errors.New("custom")
	}
	assert.Equal(self.t, "/a", arg.Path())
	assert.Equal(self.t, "GET", arg.Method())
	assert.Equal(self.t, 1, arg.Context()["req"])
	assert.Equal(self.t, "2", string(arg.GetBody()))
	assert.Equal(self.t, 200, arg.GetStatus())
	arg.SetStatus(201)
	return nil
}

func (self MidFull) OnErr(arg proto.IMiddlewareErrArg) error {
	assert.Equal(self.t, arg.GetErr().Error(), "custom")
	assert.Equal(self.t, "/a", arg.Path())
	assert.Equal(self.t, "GET", arg.Method())
	assert.Equal(self.t, 1, arg.Context()["req"])
	return errors.New("changed")
}

func test_middleware(raise bool, t *testing.T) proto.IMiddleware {
	return MidFull{raise: raise, t: t}
}

func TestMiddlewareRespErrCaseOk(t *testing.T) {
	handler := srv.MakeHandler(CtxHandler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(
		container,
		err_to_api.Proc,
		nil,
		[]proto.IMiddleware{test_middleware(false, t)},
	)
	resp := disp.Dispatch(std_engine.AdaptReq(
		httptest.NewRequest("GET", "/a", nil),
		httptest.NewRecorder(),
	))
	assert.Equal(t, 201, resp.Status)
	assert.Equal(t, "2", string(resp.Data))
}

func TestMiddlewareRespErrCaseErr(t *testing.T) {
	handler := srv.MakeHandler(CtxHandler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(
		container,
		err_to_api.Proc,
		nil,
		[]proto.IMiddleware{test_middleware(true, t)},
	)
	resp := disp.Dispatch(std_engine.AdaptReq(
		httptest.NewRequest("GET", "/a", nil),
		httptest.NewRecorder(),
	))
	assert.Equal(t, 500, resp.Status)
	assert.Equal(
		t,
		`{"code":"INTERNAL","detail":"changed"}`,
		string(resp.Data),
	)
}

func TestMiddlewareHeaders(t *testing.T) {
	handler := srv.GenericHandler(
		srv.MethodGet, "/a", func(srv.None) (string, error) {
			return "x", nil
		},
	)
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(
		container,
		err_to_api.Proc,
		nil,
		[]srv.IMiddleware{MidSetHeader{}},
	)
	req := httptest.NewRequest("GET", "/a", nil)
	req.Header.Set("x", "a")
	req.Header.Add("y", "1")
	req.Header.Add("y", "2")
	resp := disp.Dispatch(std_engine.AdaptReq(
		req,
		httptest.NewRecorder(),
	))
	assert.Equal(t, 200, resp.Status)
	assert.Equal(t, "x", string(resp.Data))

	assert.Equal(
		t,
		"x: a, y: [1 2]",
		req.Header.Get("z"),
	)
}
