package dispatcher_test

import (
	"bytes"
	"errors"
	"fmt"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/dispatcher"
	"gitlab.com/asvedr/srv/internal/err_to_api"
	"gitlab.com/asvedr/srv/internal/form_params"
	"gitlab.com/asvedr/srv/internal/handler_contaier"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/router"
	"gitlab.com/asvedr/srv/internal/std_engine"
	"gitlab.com/asvedr/srv/internal/types"
)

type get_handler struct {
	srv.BaseGetHandler
}

type post_handler struct {
	srv.BasePostHandler
}

type set_hdr_handler struct {
	srv.BaseGetHandler
}

type get_query struct {
	X int `srv:"query" name:"x"`
}

type body_data struct {
	X int `json:"x"`
}

type body struct {
	Body body_data `srv:"body"`
}

func (get_handler) Path() string {
	return "/a"
}

func (get_handler) Handle(req get_query) (string, error) {
	if req.X < 0 {
		return "", errors.New("custom")
	}
	return fmt.Sprint(req.X + 1), nil
}

func (post_handler) Path() string {
	return "/a"
}

func (post_handler) Handle(req body) (string, error) {
	return fmt.Sprint(req.Body.X - 1), nil
}

func (set_hdr_handler) Path() string {
	return "/hdr"
}

func (set_hdr_handler) Handle(req srv.None) (srv.Response[string], error) {
	resp := srv.Response[string]{Data: "ok"}
	resp.SetStatus(201)
	resp.AddHeader("X-My-Header", "1")
	return resp, nil
}

func param_maker(
	ptt types.PathPttrn,
	param any,
) (proto.IRoutable, error) {
	h, err := form_params.New(ptt, param)
	return srv.MakeHandler(h), err
}

func TestServeGetOk(t *testing.T) {
	handler := srv.MakeHandler(get_handler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(container, err_to_api.Proc, nil, nil)
	resp := disp.Dispatch(std_engine.AdaptReq(
		httptest.NewRequest("GET", "/a?x=1", nil),
		httptest.NewRecorder(),
	))
	assert.Equal(t, 200, resp.Status)
	assert.Equal(t, "2", string(resp.Data))
}

func TestServeGetBadQuery(t *testing.T) {
	handler := srv.MakeHandler(get_handler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(container, err_to_api.Proc, nil, nil)
	resp := disp.Dispatch(std_engine.AdaptReq(
		httptest.NewRequest("GET", "/a?x=abc", nil),
		httptest.NewRecorder(),
	))
	assert.Equal(t, 400, resp.Status)
}

func TestServeGetInternalErr(t *testing.T) {
	handler := srv.MakeHandler(get_handler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(container, err_to_api.Proc, nil, nil)
	resp := disp.Dispatch(std_engine.AdaptReq(
		httptest.NewRequest("GET", "/a?x=-1", nil),
		httptest.NewRecorder(),
	))
	assert.Equal(t, 500, resp.Status)
	msg := `{"code":"INTERNAL","detail":"custom"}`
	assert.Equal(t, msg, string(resp.Data))
}

func TestGetNotFound(t *testing.T) {
	handler := srv.MakeHandler(get_handler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(container, err_to_api.Proc, nil, nil)
	resp := disp.Dispatch(std_engine.AdaptReq(
		httptest.NewRequest("GET", "/xxx", nil),
		httptest.NewRecorder(),
	))
	assert.Equal(t, 404, resp.Status)
	msg := `{"code":"NOT_FOUND","detail":"Url not found"}`
	assert.Equal(t, msg, string(resp.Data))
}

func TestServePostOk(t *testing.T) {
	handler := srv.MakeHandler(post_handler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(container, err_to_api.Proc, nil, nil)
	buf := bytes.NewBuffer([]byte(`{"x":2}`))
	req := httptest.NewRequest("POST", "/a", buf)
	req.Header.Set("abc", "def")
	resp := disp.Dispatch(std_engine.AdaptReq(
		req, httptest.NewRecorder(),
	))
	assert.Equal(t, 200, resp.Status)
	assert.Equal(t, "1", string(resp.Data))
	assert.Equal(t, "def", req.Header.Get("Abc"))
	assert.Equal(t, "", req.Header.Get("X-My-Header"))
}

func TestSetHeader(t *testing.T) {
	handler := srv.MakeHandler(set_hdr_handler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(container, err_to_api.Proc, nil, nil)
	req := httptest.NewRequest("GET", "/hdr", bytes.NewBuffer([]byte{}))
	req.Header.Set("abc", "def")
	resp := disp.Dispatch(std_engine.AdaptReq(
		req, httptest.NewRecorder(),
	))
	assert.Equal(t, 201, resp.Status)
	assert.Equal(t, "ok", string(resp.Data))
	assert.Equal(t, "def", req.Header.Get("Abc"))
	assert.Equal(t, "1", req.Header.Get("X-My-Header"))
	assert.Equal(t, "", req.Header.Get("Def-Hdr"))
}

func TestDefaultHeader(t *testing.T) {
	handler := srv.MakeHandler(set_hdr_handler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(
		container,
		err_to_api.Proc,
		map[string]string{"def-hdr": "def-val"},
		nil,
	)
	req := httptest.NewRequest("GET", "/hdr", bytes.NewBuffer([]byte{}))
	req.Header.Set("abc", "def")
	resp := disp.Dispatch(std_engine.AdaptReq(
		req, httptest.NewRecorder(),
	))
	assert.Equal(t, 201, resp.Status)
	assert.Equal(t, "ok", string(resp.Data))
	assert.Equal(t, "def", req.Header.Get("Abc"))
	assert.Equal(t, "1", req.Header.Get("X-My-Header"))
	assert.Equal(t, "def-val", req.Header.Get("Def-Hdr"))
}

func TestServePostErr(t *testing.T) {
	handler := srv.MakeHandler(post_handler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(container, err_to_api.Proc, nil, nil)
	buf := bytes.NewBuffer([]byte(`{"x":"abc"}`))
	resp := disp.Dispatch(std_engine.AdaptReq(
		httptest.NewRequest("POST", "/a", buf),
		httptest.NewRecorder(),
	))
	assert.Equal(t, 400, resp.Status)
}

type buffer struct{}

func (buffer) Read([]byte) (int, error) {
	return 0, errors.New("custom")
}

func TestReadErr(t *testing.T) {
	handler := srv.MakeHandler(post_handler{})
	container, err := handler_contaier.New(
		param_maker,
		router.New,
		[]proto.IRoutable{handler},
	)
	assert.NoError(t, err)
	disp := dispatcher.New(container, err_to_api.Proc, nil, nil)
	resp := disp.Dispatch(std_engine.AdaptReq(
		httptest.NewRequest("POST", "/a", buffer{}),
		httptest.NewRecorder(),
	))
	assert.Equal(t, 500, resp.Status)
	msg := `{"code":"INTERNAL","detail":"INTERNAL_ERROR: can not read"}`
	assert.Equal(t, msg, string(resp.Data))
}
