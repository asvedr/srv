package types_test

import (
	"net/textproto"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/srv"
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/types"
)

func TestBadPattern(t *testing.T) {
	_, err := types.PathPttrn{}.Parse("/a/::b/c")
	assert.ErrorAs(t, err, &errs.ErrRestMustBeLast{})
}

func TestStdResponse(t *testing.T) {
	resp := srv.Response[int]{Data: 123}

	internal := resp.IntoResponse()
	assert.Equal(t, 123, internal.Data)
	assert.Equal(t, 200, internal.Status)
	assert.Equal(t, 0, len(internal.HeaderOps))

	resp.SetStatus(403)

	internal = resp.IntoResponse()
	assert.Equal(t, 123, internal.Data)
	assert.Equal(t, 403, internal.Status)
	assert.Equal(t, 0, len(internal.HeaderOps))
}

var modify_header_cases = []struct {
	key  string
	f    func(*srv.Response[int])
	diff []types.HeaderDiff
}{
	{
		"add",
		func(res *srv.Response[int]) {
			res.AddHeader("auth", "xxx")
			res.AddHeader("abc", "123")
		},
		[]types.HeaderDiff{
			{Key: "auth", Type: types.HeaderDiffTypeSet, Val: "xxx"},
			{Key: "abc", Type: types.HeaderDiffTypeSet, Val: "123"},
		},
	},
	{
		"del",
		func(res *srv.Response[int]) {
			res.DelHeader("auth")
			res.DelHeader("abc")
		},
		[]types.HeaderDiff{
			{Key: "auth", Type: types.HeaderDiffTypeDel},
			{Key: "abc", Type: types.HeaderDiffTypeDel},
		},
	},
	{
		"set",
		func(res *srv.Response[int]) {
			res.SetHeaders(map[string]string{
				"auth": "xxx",
				"abc":  "123",
			})
		},
		[]types.HeaderDiff{
			{Type: types.HeaderDiffTypeDel},
			{Key: "abc", Type: types.HeaderDiffTypeSet, Val: "123"},
			{Key: "auth", Type: types.HeaderDiffTypeSet, Val: "xxx"},
		},
	},
}

func TestModifyHeaderOps(t *testing.T) {
	for _, c := range modify_header_cases {
		t.Run(c.key, func(t *testing.T) {
			resp := srv.Response[int]{Data: 123}
			c.f(&resp)

			internal := resp.IntoResponse()
			assert.Equal(t, 123, internal.Data)
			assert.Equal(t, 200, internal.Status)
			assert.Equal(t, c.diff, internal.HeaderOps)
		})
	}
}

func TestResponseType(t *testing.T) {
	assert.Equal(
		t,
		srv.Response[int]{}.ResponseType(),
		reflect.TypeOf(int(0)),
	)
	assert.Equal(
		t,
		srv.Response[float64]{}.ResponseType(),
		reflect.TypeOf(float64(0.0)),
	)
	assert.Equal(
		t,
		srv.Response[[]byte]{}.ResponseType(),
		reflect.TypeOf([]byte{1, 2, 3}),
	)
}

func TestMockFile(t *testing.T) {
	header := textproto.MIMEHeader{}
	header.Add("Content-Type", "text/plain")
	form := srv.FormFile{}.Mock("abc", header, []byte("hi"))
	assert.Equal(t, "abc", form.Header.Filename)
	assert.Equal(t, 2, int(form.Header.Size))
	bts, err := form.ReadAll()
	assert.NoError(t, err)
	assert.Equal(t, []byte("hi"), bts)
}
