package srv

import (
	"errors"
	"sync"

	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/std_engine"
)

type IEngine = proto.IEngine
type IDispatcher = proto.IDispatcher

const std_name = "std"

var engine_registry = map[string]IEngine{
	std_name: std_engine.StdEngine{},
	"fake":   fake_engine{},
}
var engine_registry_mu sync.Mutex

type fake_engine struct{}

func RegisterEngine(name string, engine IEngine) {
	engine_registry_mu.Lock()
	defer engine_registry_mu.Unlock()
	engine_registry[name] = engine
}

func get_engine(name string) IEngine {
	if name == "" {
		name = std_name
	}
	engine_registry_mu.Lock()
	defer engine_registry_mu.Unlock()
	return engine_registry[name]
}

func (fake_engine) Spawn(
	addr string,
	dispatcher IDispatcher,
	tls_cert string,
	tls_key string,
) error {
	return errors.New("the engine is faked")
}
