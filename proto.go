package srv

import (
	"gitlab.com/asvedr/srv/internal/errs"
	"gitlab.com/asvedr/srv/internal/proto"
)

type IToApiError = errs.IToApiError

type IApiHandler[Request, Response any] interface {
	FormParams() any
	Errors() []error
	Method() string
	Path() string
	Description() string
	Handle(Request) (Response, error)
}

type IRoutable = proto.IRoutable
type IQueryParam = proto.IQueryParam
type IHeaderParam = proto.IHeaderParam
type IMiddlewareReqArg = proto.IMiddlewareReqArg
type IMiddlewareRespArg = proto.IMiddlewareRespArg
type IMiddlewareErrArg = proto.IMiddlewareErrArg
type IMiddleware = proto.IMiddleware
