package srv

import (
	"embed"

	"gitlab.com/asvedr/srv/internal/base"
	"gitlab.com/asvedr/srv/internal/std_handlers"
)

type BaseGetHandler = base.BaseGetHandler
type BasePostHandler = base.BasePostHandler
type BasePutHandler = base.BasePutHandler
type BasePatchHandler = base.BasePatchHandler
type BaseDeleteHandler = base.BaseDeleteHandler

type t_static struct{}

var Static = t_static{}

func (t_static) Bts(
	path string,
	data []byte,
	headers map[string]string,
) IRoutable {
	return MakeHandler(
		std_handlers.StaticBts(path, data, headers),
	)
}

func (t_static) EmbedFile(
	path string,
	fs embed.FS,
	key string,
	headers map[string]string,
) IRoutable {
	return MakeHandler(
		std_handlers.StaticEmbedFile(path, fs, key, headers),
	)
}

func (t_static) BtsMap(
	path string,
	data map[string][]byte,
	headers_by_ext map[string]map[string]string,
) IRoutable {
	return MakeHandler(
		std_handlers.StaticBtsMap(path, data, headers_by_ext),
	)
}

func (t_static) EmbedMap(
	path string,
	root embed.FS,
	headers_by_ext map[string]map[string]string,
) IRoutable {
	return MakeHandler(
		std_handlers.StaticEmbedMap(path, root, headers_by_ext),
	)
}
