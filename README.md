# Srv - Api server framework

Srv works over standart `net/http` lib. 
- query param parsing
- body parsing
- response serialization
- error handling
- match method/path
- add "help" method *optional*
- automaticly generate _form params_ method
- routers

# How to use

```go
type handler struct { srv.BaseGetHandler }

type request struct {
    Name string `srv:"query" name:"name"`
}

func (handler) Path() string {
    return "/path/to/handler/"
}

func (handler) Handle(req request) (string, error) {
    return "hello " + req.Name + "!", nil
}

srv.Srv{
	Addr: ":8080",
	HelpPath: "/help",
	Handlers: []srv.IRoutable{
        srv.MakeHandler(handler{})
    }
}.Run()
```

# Documentation
- [How to declare handler](/docs/handler.md)
- [Headers parsing](/docs/headers.md)
- [Query parsing](/docs/query.md)
- [Body parsing](/docs/body.md)
- [Accepting form](/docs/request_form.md)
- [Request inheritance](/docs/request_inheritance.md)
- [Response serialization](/docs/response.md)
- [Default headers](/docs/default_headers.md)
- [Custom headers and response code](/docs/custom_hdr_code.md)
- [Error processing](/docs/error.md)
- [Help handler](/docs/help.md)
- [Params preset](/docs/form_params.md)
- [Routers](/docs/routers.md)
- [Middleware](/docs/middleware.md)

# Tests
[Link to integrations tests](https://gitlab.com/asvedr/srv-tests)
