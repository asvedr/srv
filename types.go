package srv

import (
	"reflect"
	"sort"

	"gitlab.com/asvedr/collections/slices"
	"gitlab.com/asvedr/srv/internal/base"
	"gitlab.com/asvedr/srv/internal/proto"
	"gitlab.com/asvedr/srv/internal/types"
)

type Body[T any] struct {
	Body T `srv:"body"`
}

type Response[T any] struct {
	Data       T
	status     int
	header_ops []types.HeaderDiff
}

type FormFile = types.FormFile

var _ proto.IIntoResponse = Response[string]{}

type BaseMiddleware = base.BaseMiddleware

type RawRequest = proto.IRawRequest
type InternalResponse = types.InternalResponse

func (self *Response[T]) SetStatus(status int) {
	self.status = status
}

func (self *Response[T]) SetHeaders(headers map[string]string) {
	header_ops := make([]types.HeaderDiff, len(headers)+1)
	header_ops[0] = types.HeaderDiff{Type: types.HeaderDiffTypeDel}
	keys := slices.MapKeys(headers)
	sort.Strings(keys)
	tp := types.HeaderDiffTypeSet
	for i, key := range keys {
		val := headers[key]
		header_ops[i+1] = types.HeaderDiff{Key: key, Type: tp, Val: val}
	}
	self.header_ops = header_ops
}

func (self *Response[T]) AddHeader(key string, val string) {
	self.header_ops = append(self.header_ops, types.HeaderDiff{
		Key:  key,
		Type: types.HeaderDiffTypeSet,
		Val:  val,
	})
}

func (self *Response[T]) DelHeader(key string) {
	self.header_ops = append(self.header_ops, types.HeaderDiff{
		Key:  key,
		Type: types.HeaderDiffTypeDel,
	})
}

func (self Response[T]) ResponseType() reflect.Type {
	var t T
	return reflect.TypeOf(t)
}

func (self Response[T]) IntoResponse() types.Response {
	status := self.status
	if status == 0 {
		status = 200
	}
	return types.Response{
		Data:      self.Data,
		Status:    status,
		HeaderOps: self.header_ops,
	}
}
